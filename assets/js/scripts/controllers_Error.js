var LOMCityControllers;

LOMCityControllers = angular.module('LOMCityControllers', []);

LOMCityControllers.controller('headerCtrl', ["$rootScope", "$scope", "$state","$translate","$http","$timeout",'SatellizerShared', function ($rootScope, $scope, $state, $translate, $http,$timeout,shared) {    
    $rootScope.$state = $state;
    $scope.changeLanguage = function(language){
     console.log(language);
     $translate.use(language);
    }
    $scope.logOut = function () {
        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/logOut',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            if (response.status == 1) {
                //localStorage.clear();
                localStorage.removeItem('satellizer_token');
                $state.go('main.login', {});
            } 
        });
    }

    var mr_firstSectionHeight,
    mr_nav,
    mr_navOuterHeight,
    mr_navScrolled = false,
    mr_navFixed = false,
    mr_outOfSight = false,
    mr_floatingProjectSections,
    mr_scrollTop = 0;

    $('.inner-link').each(function () {
        var href = $(this).attr('href');
        if (href)
        if (href.charAt(0) !== "#") {
            $(this).removeClass('inner-link');
        }
    });

    // Update scroll variable for scrolling functions

    addEventListener('scroll', function () {
        mr_scrollTop = window.pageYOffset;
    }, false);

    // Navigation
    $(window).resize(function () {
        $('.mobile-toggle').removeClass('active');
        $('.nav-bar').removeClass('nav-open');
        $('.module.widget-handle').removeClass('toggle-widget-handle');
    });
    if (!$('nav').hasClass('fixed') && !$('nav').hasClass('absolute')) {

        // Make nav container height of nav

        $('.nav-container').css('min-height', $('nav').outerHeight(true));

        $(window).resize(function () {
            $('.nav-container').css('min-height', $('nav').outerHeight(true));
        });

        // Compensate the height of parallax element for inline nav

        if ($(window).width() > 768) {
            $('.parallax:nth-of-type(1) .background-image-holder').css('top', -($('nav').outerHeight(true)));
        }

        // Adjust fullscreen elements

        if ($(window).width() > 768) {
            $('section.fullscreen:nth-of-type(1)').css('height', ($(window).height() - $('nav').outerHeight(true)));
        }

    } else {
        $('body').addClass('nav-is-overlay');
    }

    if ($('nav').hasClass('bg-dark')) {
        $('.nav-container').addClass('bg-dark');
    }

    // Fix nav to top while scrolling

    mr_nav = $('body .nav-container nav:first');
    mr_navOuterHeight = $('body .nav-container nav:first').outerHeight();
    window.addEventListener("scroll", updateNav, false);

    // Menu dropdown positioning

    $('.menu > li > ul').each(function () {
        var menu = $(this).offset();
        var farRight = menu.left + $(this).outerWidth(true);
        if (farRight > $(window).width() && !$(this).hasClass('mega-menu')) {
            $(this).addClass('make-right');
        } else if (farRight > $(window).width() && $(this).hasClass('mega-menu')) {
            var isOnScreen = $(window).width() - menu.left;
            var difference = $(this).outerWidth(true) - isOnScreen;
            $(this).css('margin-left', -(difference));
        }
    });

    // Mobile Menu

    $('.mobile-toggle').click(function () {
        $('.nav-bar').toggleClass('nav-open');
        $(this).toggleClass('active');
    });

    $('.menu li').click(function (e) {
        if (!e) e = window.event;
        e.stopPropagation();
        if ($(this).find('ul').length) {
            $(this).toggleClass('toggle-sub');
        } else {
            $(this).parents('.toggle-sub').removeClass('toggle-sub');
        }
    });

    $('.module.widget-handle').click(function () {
        $(this).toggleClass('toggle-widget-handle');
    });

    $('.search-widget-handle .search-form input').click(function (e) {
        if (!e) e = window.event;
        e.stopPropagation();
    });
}]);

LOMCityControllers.controller('homeCtrl', ["$rootScope", "$state",'$stateParams', 'title',"$cookies", function ($rootScope, $state, $stateParams, title, $cookies) {
    //console.log(title+'  '+$stateParams.param1);
    console.log($stateParams.param1);
    if($stateParams.param1 == ""){
        $cookies.inviter_id= null;
    } else {
        $cookies.inviter_id= $stateParams.param1;
    }

    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });

    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);
    
} ]);

LOMCityControllers.controller('loginCtrl', ["$rootScope", "$scope", "$state", "$http","$auth","$filter", function ($rootScope,$scope, $state, $http, $auth, $filter) {
    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });             
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);
    
    $scope.user = {
        email:'',
        password:''
    };
    //localStorage.clear();
    $scope.rememberMe = false;
    var loginDetail = localStorage.getItem('loginDetail');
    console.log(loginDetail);
    if (loginDetail != "" && loginDetail != null && loginDetail != undefined) {
        loginDetail = JSON.parse(loginDetail);
        if (loginDetail.rememberMe) {
            $scope.rememberMe = true;
            $scope.user.email = loginDetail.email;
            $scope.user.password = loginDetail.password;
        }
    }

	$scope.login = function(user){
        if(user == undefined || user.email == '' || user.password == ''){
            $rootScope.msgDescription = 'Please enter required values.';
            $("#alertModal").modal('show');
        } else {
        	var request = {
                email: user.email,
                password: user.password,
                rememberMe: $scope.rememberMe                
               };
            console.log(JSON.stringify(request));
			$auth.login(request).catch(function(response) {
                console.log(JSON.stringify(response));
                if(response.status == 401){ 
                    $rootScope.msgDescription = $filter('translate')('Invalid_email_pass');
                    $("#alertModal").modal('show');
                    return;
                }                
	        }).then(function(response) {
                //console.log(response.data.email);
                localStorage.setItem('loginDetail', JSON.stringify(request));
                console.log(JSON.stringify(request));
                if(response != undefined && response.status == 200){
                    console.log('*'+response.status);
                    $state.go('main.allEvents', {});
                }
            });
		}
	}

    $scope.verifyEmailId = function (){
        if($scope.user.email == ''){
            $rootScope.msgDescription = $filter('translate')('Error_msg5');
            $("#alertModal").modal('show');
            return;
        }
        var request = {
            email : $scope.user.email
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/checkEmail',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            console.log(JSON.stringify(response));
            if(response.status == 1){
                $state.go('main.forgotPass', { 'email':  $scope.user.email});
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg4');
                $("#alertModal").modal('show');
            }
        });
    }

} ]);

LOMCityControllers.controller('signupCtrl', ["$rootScope", "$scope", "$state",'$http',"$filter","$cookies", function ($rootScope,$scope, $state, $http, $filter, $cookies) {
    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);
   
    var inviter_id = $cookies.inviter_id;

    $scope.signUp = function(user){
    if(inviter_id == null || inviter_id == undefined || inviter_id == ""){
        inviter_id = 0;
    }
	if(user.password == user.rpassword){
	  var request = {
		firstName: user.firstName,
		lastName: user.lastName,
		email: user.email,
		password: user.password,
        inviter_id: parseInt(inviter_id),
        location: user.eventAlertAddress
	  };
      console.log(JSON.stringify(request));
	  if(user.id != undefined){
		request.id = user.id
	  }
	  $http({
		method: 'POST',
		url: $rootScope.baseUrl+'/register',
		data: {data: JSON.stringify(request) }
	  }).success(function(response){
          console.log(JSON.stringify(response));
		  if(response.status == 1){ 
            $cookies.inviter_id= null;
            $rootScope.inviter_id = response.user_id;
            //$rootScope.inviter_id = 122;
			$state.go('main.confirmation', {});
		  } 
	  }).error(function (xhr, req, err) {
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
	} else {
	    $rootScope.msgDescription = $filter('translate')('Error_msg6');
        $("#alertModal").modal('show');
	}
  }

} ]);
  
LOMCityControllers.controller('forgotPassCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','$stateParams', function ($rootScope,$scope, $timeout, $state, $http, $filter,$stateParams) {
    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);
    $scope.user = {
        email: $stateParams.email,
        password: '',
        cpassword: ''
    }

    $scope.forgotPassword = function () {
        if ($scope.user.password == '' || $scope.user.password != $scope.user.cpassword) {
            $rootScope.msgDescription = $filter('translate')('Error_msg8');
            $("#alertModal").modal('show');
            return;
        }
        var request = {
            email: $scope.user.email,
            password: $scope.user.password
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/changePass',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            console.log(JSON.stringify(response));
            if (response.status == 1) {
                $rootScope.msgDescription = 'Change password successfully.';
                $("#alertModal").modal('show').on("shown.bs.modal", function () {
                    $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.login', {});
                    }, 1000);                 
                });
            } 
        }).error(function (xhr, req, err) {
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }
}]);

LOMCityControllers.controller('allEventsCtrl', ["$rootScope","$scope", "$state",'$http','dhm','getDist',"$filter","$sce","$cookies",'$stateParams','isValidToken', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams,isValidToken) {    
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getPreferredLocations();
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = true;
    $scope.events = [];

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    
    $(window).scroll(function() {
      if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        if($scope.load_events == true && ($state.includes('main.allEvents') || $state.includes('main.allEventsFiltered')) ){
            console.log('bottom');
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
      }
    });
    
	$scope.searchModel = {};
    if($stateParams.param2 == "Location" && $stateParams.param1 != ""){
        var SearchVal = localStorage.getItem('SearchVal');
        $scope.SearchVal = JSON.parse(SearchVal);
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchLocationVal = $stateParams.param1;
    } else {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
    }

    $scope.search = function() {
    	//console.log($scope.searchModel.SearchLocationVal);
    	/*if ($scope.searchModel.FilterVal=="Location" && $scope.searchModel.SearchLocationVal>'') {
            var paramObj = {
                param1:$scope.searchModel.SearchLocationVal.formatted_address, 
                param2:$scope.searchModel.FilterVal
            }
            if($stateParams.param1 == $scope.searchModel.SearchLocationVal){
                paramObj.param1 = $scope.searchModel.SearchLocationVal;
                promise = isValidToken.check();
                promise.then(function (token) {
                    $state.go('main.allEventsFiltered', paramObj, {reload: true});
                }, function (response) {
                    $state.go('main.login', {});
                });
            } else {
                localStorage.setItem('SearchVal', JSON.stringify($scope.searchModel.SearchLocationVal));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $state.go('main.allEventsFiltered', paramObj, {reload: true});
                }, function (response) {
                    $state.go('main.login', {});
                });
            }
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $state.go('main.allEvents', {});
            }, function (response) {
                $state.go('main.login', {});
            });
        }*/
        $scope.results_from = 0;
        if ($scope.searchModel.FilterVal == "Location" && $scope.searchModel.SearchLocationVal != undefined && $scope.searchModel.SearchLocationVal != '') {
            promise = isValidToken.check();
            promise.then(function(token) {
                var locations = {
                    lat: $scope.searchModel.SearchLocationVal.geometry.location.lat(),
                    long: $scope.searchModel.SearchLocationVal.geometry.location.lng(),
                    name: $scope.searchModel.SearchLocationVal.formatted_address
                }
                $scope.getContent(locations, $scope.searchModel.FilterVal);
                //$scope.searchModel.SearchVal = "";
                $scope.locationSelected = false;
            }, function(response) {
                $state.go('login', {});
            });
        }else if($scope.searchModel.FilterVal == "Title" || $scope.searchModel.FilterVal == "User")
        {
            promise = isValidToken.check();
            promise.then(function(token) {
                $scope.getContent($scope.searchModel.SearchVal, $scope.searchModel.FilterVal);
            }, function(response) {
                $state.go('login', {});
            });
        } else {
            promise = isValidToken.check();
            promise.then(function(token) {
                $scope.getContent(false, false);
            }, function(response) {
                $state.go('login', {});
            });
        }
    }
   
    $scope.getGeolocation = function() {
        if($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined){
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        } else if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(myLoc);
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        console.log($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent($scope.SearchVal, $scope.FilterVal);
        }, function (response) {
            $state.go('main.login', {});
        });
    }
    
    //console.log($scope.events.length+' '+$scope.results_from+' '+$scope.results_num);
    $scope.getContent = function (SearchVal, FilterVal) {
        geocoder = new google.maps.Geocoder();
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            lat: $rootScope.myLat,
            long: $rootScope.myLong,
            SearchVal: SearchVal,
            FilterVal: FilterVal,
            _token: localStorage.satellizer_token,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getEvents',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if(events.length < 20){
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder= new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {
                        var time = dhm(response.content[i]['created_at']);
                        events[i]['rtime'] = time;
                        events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }
                        $scope.events.push(events[i]);
                    };
                    $scope.noEvents = false;
                    console.log($scope.events.length);
                } else {
                    $scope.load_events = false;
                    $scope.events = events;
                    $scope.noEvents = true;
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getSettings ',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;
            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            console.log(JSON.stringify(req));
        });
    }

    $scope.isLocation = true;
    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
        } else {
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

} ]); 

LOMCityControllers.controller('myEventsCtrl', ["$rootScope","$scope", "$state",'$http','dhm','getDist',"$filter","$sce","$cookies",'$stateParams','isValidToken', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams,isValidToken) {    
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getPreferredLocations();
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = true;
    $scope.events = [];

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    
    $(window).scroll(function() {
      if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        if($scope.load_events == true && ($state.includes('main.allEvents') || $state.includes('main.allEventsFiltered')) ){
            console.log('bottom');
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
      }
    });
    
    $scope.searchModel = {};
    if($stateParams.param2 == "Location" && $stateParams.param1 != ""){
        var SearchVal = localStorage.getItem('SearchVal');
        $scope.SearchVal = JSON.parse(SearchVal);
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchLocationVal = $stateParams.param1;
    } else {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
    }

    $scope.search = function() {
        //console.log($scope.searchModel.SearchLocationVal);
        if ($scope.searchModel.FilterVal=="Location" && $scope.searchModel.SearchLocationVal>'') {
            var paramObj = {
                param1:$scope.searchModel.SearchLocationVal.formatted_address, 
                param2:$scope.searchModel.FilterVal
            }
            if($stateParams.param1 == $scope.searchModel.SearchLocationVal){
                paramObj.param1 = $scope.searchModel.SearchLocationVal;
                promise = isValidToken.check();
                promise.then(function (token) {
                    $state.go('main.allEventsFiltered', paramObj, {reload: true});
                }, function (response) {
                    $state.go('main.login', {});
                });
            } else {
                localStorage.setItem('SearchVal', JSON.stringify($scope.searchModel.SearchLocationVal));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $state.go('main.allEventsFiltered', paramObj, {reload: true});
                }, function (response) {
                    $state.go('main.login', {});
                });
            }
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $state.go('main.allEvents', {});
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }
   
    $scope.getGeolocation = function() {
        if($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined){
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        } else if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(myLoc);
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent($scope.SearchVal, $scope.FilterVal);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        console.log($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent($scope.SearchVal, $scope.FilterVal);
        }, function (response) {
            $state.go('main.login', {});
        });
    }
    
    //console.log($scope.events.length+' '+$scope.results_from+' '+$scope.results_num);
    $scope.getContent = function (SearchVal, FilterVal) {
        geocoder = new google.maps.Geocoder();
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            lat: $rootScope.myLat,
            long: $rootScope.myLong,
            SearchVal: SearchVal,
            FilterVal: "myEvents",
            _token: localStorage.satellizer_token,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getEvents',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if(events.length < 20){
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder= new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {
                        var time = dhm(response.content[i]['created_at']);
                        events[i]['rtime'] = time;
                        events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }
                        $scope.events.push(events[i]);
                    };
                    $scope.noEvents = false;
                    console.log($scope.events.length);
                } else {
                    $scope.load_events = false;
                    $scope.events = events;
                    $scope.noEvents = true;
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getSettings ',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;
            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            console.log(JSON.stringify(req));
        });
    }

    $scope.isLocation = true;
    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
        } else {
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

} ]); 
 
LOMCityControllers.controller('myFavEventsCtrl', ["$rootScope","$scope", "$state",'$http','dhm','getDist',"$filter","$sce","$timeout","isValidToken", function ($rootScope, $scope, $state, $http, dhm, getDist, $filter,$sce,$timeout,isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getGeolocation();
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }
    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = true;
    $scope.events = [];
    $scope.noEvents = false;
    //console.log($scope.events.length+' '+$scope.results_from+' '+$scope.results_num);

    $(window).scroll(function() {
      if ($(window).scrollTop() + $(window).height() == $(document).height()) {
        if($scope.load_events == true && $state.includes('main.myFavEvents')){
            console.log('bottom');
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getFavEvents();
            }, function (response) {
                $state.go('main.login', {});
            });
        }
      }
    });

    var km_checked = localStorage.getItem('km_checked');
    $rootScope.km_checked = km_checked == 'false' ? false : true;

    $scope.getFavEvents = function () {
        var request = {
            _token:localStorage.satellizer_token,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        //console.log(JSON.stringify(request));
        $rootScope.showLoading();
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getFavEvents',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if(events.length < 20){
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder= new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {
                        var time = dhm(response.content[i]['created_at']);
                        events[i]['rtime'] = time;
                        events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        $scope.events.push(events[i]);
                    };
                    $scope.noEvents = false;
                    console.log($scope.events.length);
                } else {
                    $scope.load_events = false;
                    $scope.events = events;
                    $scope.noEvents = true;
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getGeolocation = function() {
        if($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined){
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getFavEvents();
            }, function (response) {
                $state.go('main.login', {});
            });
        } else if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(myLoc);
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getFavEvents();
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }
    function myLoc(position) {
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getFavEvents();
        }, function (response) {
            $state.go('main.login', {});
        });
    }
    
} ]); 

LOMCityControllers.controller('eventCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$stateParams','dhm','getDist',"$filter","$sce","isValidToken", function ($rootScope,$scope, $timeout, $state, $http, $stateParams, dhm, getDist,$filter, $sce,isValidToken) {
    
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.openEvent();
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.getEvent = false;
    $scope.noComments = false;
    $scope.errorMsg = false;
    $scope.stay_annonymous = false;
    $scope.comment = {};
    
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

	$timeout(function () {
        $scope.imageZoom = 1;
	    $scope.$watch("imageZoom", 
			 function changeImageZoom( newValue, oldValue ) {
			 	 var containerWidth = $("#event-img").width();
			 	 var imageWidth = containerWidth * (1+ newValue/100);
				 $("#eventImage").css('max-width', imageWidth);
				 $("#eventImage").css('width', imageWidth);
	        }
	    );
    }, 500);           
    /*document.getElementById("event-img").style.display = 'none';
	document.getElementById("rangeSlider").style.display = 'none';
	document.getElementById("media-con").style.display = 'none';*/         

    $scope.getComments = function (event_id) {
        $scope.comments = '';
        var request = {
            event_id: event_id,
            _token: localStorage.satellizer_token
        }
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getComments',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            console.log(JSON.stringify(response));
            if (response.status == 1) {
                comments = response.content;
                if (comments.length == 0) {
                    $scope.noComments = true;
                } else {
                    for (var i = 0; i < comments.length; i++) {
                        if (comments[i]['avatar'] == '') {
                            comments[i]['avatar'] = 'img/default-avatar.png';
                        }
                            comments[i]['rtime'] = dhm(comments[i]['created_at']);
                            //comments[i]['rtime'] = dhm('2015-12-09 06:20:26');
                    };
                    $scope.comments = comments;
                    $scope.noComments = false;
                }
            } else {
                console.log($filter('translate')('Error_msg7'));
            }
        }).error(function (xhr, req, err) {
            console.log($filter('translate')('Error_msg10'));
        });
    };

    $scope.openEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };
        $rootScope.showLoading();
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getEvent',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response));
            if (response.status == 1) {
                var even = response.content;
                if (even['avatar'] == '') {
                    even['avatar'] = 'img/default-avatar.png';
                }
                var time = dhm(even['created_at']);
                if (time == '') {
                    time = 'Just now';
                }
                /*if (even['mediaType'] != 'image') {
                    document.getElementById("event-img").style.display = 'none';
                    document.getElementById("rangeSlider").style.display = 'none';
                }else{
                    document.getElementById("event-img").style.display = 'block';
                    document.getElementById("rangeSlider").style.display = 'block';
                }
                if(even['mediaType'] == ''){
                    document.getElementById("media-con").style.display = 'none';
                }else{
                    document.getElementById("media-con").style.display = 'block';
                }*/
                even['rtime'] = time;
                even['distance'] = getDist(even['location_lat'], even['location_long'], $rootScope.km_checked);
                if (even.contentUrl == '') {
                    even.content = false;
                } else {
                    even.content = true;
                }
                $scope.op_event = even;
                $rootScope.pageTitle = even['title'];
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getComments(even['id']);
                }, function (response) {
                    $state.go('main.login', {});
                });
                $scope.getEvent = true;
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            } 
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };

    $scope.addComment = function (event_id, comment) {
	    promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                event_id: event_id,
                comment: comment.text,
                _token: localStorage.satellizer_token
            }
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/addComment',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                console.log(JSON.stringify(response));
                if (response.status == 1) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        $scope.getComments(event_id);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                    $scope.comment = {};
                } else {
                     $rootScope.msgDescription = $filter('translate')('Error_msg7');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                   $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.sendMessage = function () {
        if ($scope.data.message == undefined || $scope.data.message == "") {
            $scope.errorMsg = true;
            return;
        }
        $('#sendMessage').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $scope.op_event.user_id,
                message: $scope.data.message,
                event_id: $stateParams.event_id,
                annonymous: $scope.stay_annonymous,
                _token: localStorage.satellizer_token
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/sendMessage',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                console.log(JSON.stringify(response));
                $scope.stay_annonymous = false;
                $scope.data.message = "";
                if (response.status = 1) {
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function(){
       $scope.errorMsg = false;
       $scope.data.message = "";
       $scope.stay_annonymous = false;
    }

    $scope.reportBadContentMsg = function () {
        $('#reportBadContent').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                event_id: $stateParams.event_id,
                subject : $scope.msg_category,
                message: $scope.data.reportMessage
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/reportEvent',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                console.log(JSON.stringify(response));
                $scope.data.reportMessage = "";
                if (response.status = 1) {
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelReportModel = function(){
       $scope.errorMsg = false;
       $scope.data.reportMessage = "";
       $scope.msg_category = "It\'\s spam";
    }
} ]);

LOMCityControllers.controller('createEventsCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','isValidToken', function ($rootScope,$scope, $timeout, $state, $http, $filter,isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.newEvent = {};
    $scope.result1 = '';
    $scope.options1 = null;
    $scope.details1 = '';
    $scope.address = '';
    $scope.selectedOption = 'Stolen';
    $scope.mediaType = '';
    $scope.mediaUrl = '';
    $scope.myFile = '';
    $scope.fileUploading = false;
    
    $('#setTimeExample').timepicker( { 'step': 1 } );
    $timeout(function () {
        $('#setTimeExample').timepicker('setTime', new Date());
    }, 500);

    $scope.opened = false;
    $scope.date = new Date();
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy','dd/MM/yyyy','dd-MM-yyyy', 'shortDate'];
    $scope.format = $scope.formats[4];
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }
    
    $scope.addEvent = function () {
        console.log($scope.mediaType +' '+$scope.mediaUrl);
        var temp = $('#setTimeExample').val();
        var zz = temp.split(":");
        $scope.time =  new Date();
        
        $scope.time.setHours(zz[0]);
        $scope.time.setMinutes(zz[1]);
        $scope.time.getMilliseconds(zz[2]);
        console.log($scope.time);
        
        if (localStorage.satellizer_token == undefined) {
	        $state.go('main.login', {});
            return;
	    }
        var coord = $scope.address.geometry.location;
        var formatted_address = $scope.address.formatted_address;
        var when = $scope.date + ' ' + $scope.time;
        var loc = $.map(coord, function (value, index) {
            return [value];
        });
        if ($scope.newEvent.reward == undefined) {
            $scope.newEvent.reward = '';
        }
        console.log(coord);
        var request = {
            address: formatted_address,
            title: $scope.newEvent.title,
            description: $scope.newEvent.description,
            reward: $scope.newEvent.reward,
            when: when,
            lat: coord.lat(),
            long: coord.lng(),
            _token: localStorage.satellizer_token,
            category: $scope.selectedOption,
            mediaType: $scope.mediaType,
            mediaUrl: $scope.mediaUrl,
            location: $scope.address
        };
        console.log(JSON.stringify(request));
        //return;
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/addEvent',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
        console.log( JSON.stringify(response) );
            $scope.fileUploading = false;
            if (response.status == 1) {
                $scope.newEvent = {};
                $scope.date = new Date();
                $scope.address = '';
                $scope.selectedOption = 'Stolen';
                $scope.result1 = '';
                $scope.mediaType = '';
                $scope.mediaUrl = '';
                $rootScope.msgDescription = response.msg;
                $("#alertModal").modal('show').on("shown.bs.modal", function () {
                    $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.allEvents', {});
                    }, 1000);                 
                });
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
        console.log(xhr);
        console.log(err);
             $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.uploadFile = function(){
        var file = $scope.myFile;
        var tempFile = file.type;
        if(tempFile.indexOf("image") != -1 ){
            fileType = 'image';
        } else if (tempFile.indexOf("video") != -1){
            fileType = 'video';
        } 
        var type = fileType;
        var fileName = file.name;
        console.dir(file);
        
        var uploadUrl = "http://viround.com/public/api/upload";
        //fileUpload.uploadFileToUrl(type, file, fileName, uploadUrl);
        console.log(type + ' ' + file + ' ' + fileName + ' ' + uploadUrl);
        var fd = new FormData();
        fd.append('type', type);
        fd.append('file', file);
        fd.append('fileName', fileName);

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(function (res) {
            console.log(JSON.stringify(res));
            $scope.mediaType = type;
            $scope.mediaUrl = res.content;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.addEvent();
            }, function (response) {
                $state.go('main.login', {});
            });
        })
        .error(function (res) {
            console.log(JSON.stringify(res));
            //$scope.addEvent();
        });
    };

    $scope.uploadFile_addEvent = function() {
        if($scope.myFile != ''){
            $scope.fileUploading = true;
            $scope.uploadFile();
        }else{
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.addEvent();
            }, function (response) {
                $state.go('main.login', {});
            });
            $scope.fileUploading = true;
        }
    }
    
} ]);

LOMCityControllers.controller('settingsCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','$timeout','isValidToken', function ($rootScope,$scope, $timeout, $state, $http, $filter, $timeout,isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getSettingsData();
    }, function (response) {
        $state.go('main.login', {});
    });
    
    $scope.settingResponse = '';
    $scope.result1 = '';
    $scope.result2 = '';
    $scope.result3 = '';
    $scope.result4 = '';
    $scope.result5 = '';
    $rootScope.settingRange = 2.5;
    $scope.km_checked = false;
    $scope.errorMsg = false;
    $scope.newPassword = '';
    $scope.confirmPassword = '';
   
    
    $('.checkbox-option').on("click", function () {
        $(this).toggleClass('checked');
        var checkbox = $(this).find('input');
        if (checkbox.prop('checked') === false) {
            checkbox.prop('checked', true);
            $scope.km_checked = true;
            $scope.$apply();
        } else {
            checkbox.prop('checked', false);
            $scope.km_checked = false;
            $scope.$apply();
        }
    });

    $scope.getSettingsData = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getSettings',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            if (response.status == 1) {
                //console.log(JSON.stringify(response));
                if (response.locations.length != 0) {
                    $scope.settingResponse = response;

                    document.getElementById("l1").value = (response.locations[0].name!= undefined)?response.locations[0].name:'';
                    document.getElementById("l2").value = (response.locations[1].name!= undefined)?response.locations[1].name:'';
                    document.getElementById("l3").value = (response.locations[2].name!= undefined)?response.locations[2].name:'';
                    document.getElementById("l4").value = (response.locations[3].name!= undefined)?response.locations[3].name:'';
                    document.getElementById("l5").value = (response.locations[4].name!= undefined)?response.locations[4].name:'';
                    /*
                    $scope.result1 = response.locations[0].name;
                    $scope.result2 = response.locations[1].name;
                    $scope.result3 = response.locations[2].name;
                    $scope.result4 = response.locations[3].name;
                    $scope.result5 = response.locations[4].name;
                    */
                    $rootScope.settingRange = response.settings[0].notifyRadius;
                    if (response.settings[0].notifyUnits == 'km') {
                        $scope.km_checked = true;
                    } else {
                        $scope.km_checked = false;
                    }
                }
            }
        }).error(function (xhr, req, err) {
            if (req == 500) {
                 $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.changeSettingsData = function (result1, result2, result3, result4, result5, range) {
        var resultArr = [];
        resultArr.push(result1,result2,result3,result4,result5);
        var locationArr = [];
        for(var i = 0; i<5; i++)
        {
            var tempResult = {
                geometry: {
                    location: {
                        G: '',
                        K: ''
                    }
                },
                formatted_address: ''
            };
            if($scope.settingResponse != '')
            {
                if($scope.settingResponse.locations[i] != undefined)
                {
                    if(resultArr[i] != "")
                    {
                        console.log('resultArr');
                        tempResult.geometry.location.G = resultArr[i].geometry.location.lat();
                        tempResult.geometry.location.K = resultArr[i].geometry.location.lng();
                        tempResult.formatted_address =   resultArr[i].formatted_address;
                    }else if(document.getElementById("l"+(i+1)).value !=""){
                        tempResult.geometry.location.G = $scope.settingResponse.locations[i].location_lat;
                        tempResult.geometry.location.K = $scope.settingResponse.locations[i].location_long;
                        tempResult.formatted_address =   $scope.settingResponse.locations[i].name;
                    }else{
                        tempResult.geometry.location.G = "";
                        tempResult.geometry.location.K = "";
                        tempResult.formatted_address =   "";
                    }
                }else{
                    console.log('2'+i)
                }
            }else{
                if(resultArr[i] != "")
                {
                    console.log('Main else'+i);
                    tempResult.geometry.location.G = resultArr[i].geometry.location.lat();
                    tempResult.geometry.location.K = resultArr[i].geometry.location.lng();
                    tempResult.formatted_address =   resultArr[i].formatted_address;
                }else{
                    tempResult.geometry.location.G = "";
                    tempResult.geometry.location.K = "";
                    tempResult.formatted_address =   "";
                }
            }
            locationArr.push(tempResult);
        }
        //console.log(JSON.stringify(result1));
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                settings: {
                    notifyUnits: $scope.km_checked == true ? 'km' : 'mile',
                    notifyRadius: parseFloat(range)
                },
                locations: [{
                    lat: locationArr[0].geometry.location.G,
                    long: locationArr[0].geometry.location.K,
                    name: locationArr[0].formatted_address
                }, {
                    lat: locationArr[1].geometry.location.G,
                    long: locationArr[1].geometry.location.K,
                    name: locationArr[1].formatted_address
                }, {
                    lat: locationArr[2].geometry.location.G,
                    long: locationArr[2].geometry.location.K,
                    name: locationArr[2].formatted_address
                }, {
                    lat: locationArr[3].geometry.location.G,
                    long: locationArr[3].geometry.location.K,
                    name: locationArr[3].formatted_address
                }, {
                    lat: locationArr[4].geometry.location.G,
                    long: locationArr[4].geometry.location.K,
                    name: locationArr[4].formatted_address
                }],
                location1: result1 != '' ? result1: '',
                location2: result2 != '' ? result2: '',
                location3: result3 != '' ? result3: '',
                location4: result4 != '' ? result4: '',
                location5: result5 != '' ? result5: ''
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/changeSettings',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                if (response.status == 1) {
                    $rootScope.msgDescription = $filter('translate')('Success_msg2');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.logOut = function () {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token
            };
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/logOut',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                if (response.status == 1) {
                    //localStorage.clear();
                    localStorage.removeItem('satellizer_token');
                    $state.go('main.login', {});
                } 
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.changePassword = function () {
        if ($scope.newPassword == "" || $scope.confirmPassword == "" || $scope.newPassword != $scope.confirmPassword) {
            $scope.errorMsg = true;
            return;
        }
        $('#changePassword').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                password: $scope.newPassword
            };
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/changePassword',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                if (response.status == 1) {
                    $rootScope.msgDescription = 'Change password successfully.';
                    $("#alertModal").modal('show');
                } 
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function(){
       $scope.errorMsg = false;
       $scope.newPassword = '';
       $scope.confirmPassword = '';
    }
   
    $scope.changeKMM = function (type) {
        if (type == 'km') {
            console.log('km');
            $scope.km_checked = true;
            if (!$scope.$$phase) { // check if digest already in progress
                $scope.$apply();
            }
        } else {
            console.log('mile');
            $scope.km_checked = false;
             if (!$scope.$$phase) { 
                $scope.$apply();
            }
        }
    }
} ]);

LOMCityControllers.controller('messagesCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','isValidToken', function ($rootScope,$scope, $timeout, $state, $http, $filter,isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getMessages();
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.data = {};
    $scope.errorMsg = false;
    $scope.from_user = '';
    $scope.event_id = '';
    $scope.message_id = '';
    $scope.annonymous = '';
    $scope.to_user = '';

    $scope.getMessages = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $rootScope.showLoading();
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getMessages',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            var messages = response.content;
            var current_date = new Date();
            var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;
            for (var i = 0; i < messages.length; i++) {
                if (messages[i]['avatar'] == '' || messages[i]['avatar'] == null) {
                    messages[i]['avatar'] = 'img/default-avatar.png';
                }
                if (messages[i]['firstName'] == null && messages[i]['lastName'] == null) {
                    messages[i]['firstName'] = messages[i]['from_user'];
                }
                var tempDate = messages[i]['created_at'];
                var zz = tempDate.split(" ");
                var zz1 = zz[0].split("-");
                var zz2 = zz[1].split(":");
                var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                var a = zzz3.getTime();
                messages[i]['created_at'] = a - gmt_offset;
            }
            if (messages.length == 0) {
                $scope.noMessages = true;
            } else {
                $scope.noMessages = false;
            }
            $scope.messages = messages;
        }).error(function (xhr, req, err) {
             $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.holdParams = function(from_user, event_id, message_id, annonymous, to_user){
        //alert(annonymous);
        $scope.from_user = from_user;
        $scope.event_id = event_id;
        $scope.message_id = message_id;
        $scope.annonymous = (annonymous == 1)?true:false;
        $scope.to_user = to_user;
    }

    $scope.replyMessage = function () { 
        if($scope.data.message == "" || $scope.data.message == undefined){
            $scope.errorMsg = true;
            return;
        }
        $('#replyMessage').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $scope.from_user,
                message: $scope.data.message,  
                event_id: $scope.event_id,
                message_id: $scope.message_id,
                annonymous: $scope.annonymous,
                _token: localStorage.satellizer_token
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/sendMessage',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                $rootScope.msgDescription = $filter('translate')('Message_send');
                $("#alertModal").modal('show');
                $scope.from_user = '';
                $scope.event_id = '';
                $scope.message_id = '';
                $scope.annonymous = '';
                $scope.to_user = '';
                $scope.data = {};
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                   $rootScope.msgDescription = $filter('translate')('Error_msg10');
                   $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function(){
       $scope.errorMsg = false;
       $scope.data.message = "";
    }

    $scope.deleteMessage = function (message_id) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                message_id: message_id
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl+'/api/deleteMessage',
                data: { data: JSON.stringify(request) }
            }).success(function (response) {
                console.log(JSON.stringify(response));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

}]);

LOMCityControllers.controller('manageAdsCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','isValidToken', function ($rootScope,$scope, $timeout, $state, $http, $filter,isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getUserAds();
    }, function (response) {
        $state.go('main.login', {});
    });
    $scope.noAds = false;

    $scope.getUserAds = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $rootScope.showLoading();
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getAds',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $rootScope.hideLoading();
            if (response.status == 1) {
                var ads = response.content;
                if (ads.length > 0) {
                    for (var i = 0; i < ads.length; i++) {
                        var temp_start_date = response.content[i]['start_date'];
                        var zz = temp_start_date.split(" ");
                        var zz1 = zz[0].split("-");
                        var zz2 = zz[1].split(":");
                        ads[i]['start_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);

                        var temp_end_date = response.content[i]['end_date'];
                        var zz = temp_end_date.split(" ");
                        var zz1 = zz[0].split("-");
                        var zz2 = zz[1].split(":");
                        ads[i]['end_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    };
                    $scope.noAds = false;
                    $scope.ads = ads;
                } else {
                    $scope.ads = ads;
                    $scope.noAds = true;
                }
            }
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            console.log('in error');
            //for testing
            var response = '{"content":[{"id":25,"user_id":4,"title":"The best private investigator in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-21 11:31:24","end_date":"2015-10-28 11:31:24","exposure_center":"New delhi","radius":20,"banner_url":""},{"id":26,"user_id":4,"title":"The best request center in Israel","description":"Click here for more info.","start_date":"2015-10-11 11:31:24","end_date":"2015-10-18 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":5,"banner_url":""},{"id":27,"user_id":4,"title":"The best private help center in Israel","description":"Best prices.","start_date":"2015-10-01 11:31:08","end_date":"2015-10-08 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":10,"banner_url":""},{"id":28,"user_id":4,"title":"The best community in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-22 11:31:30","end_date":"2015-10-29 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":50,"banner_url":""},{"id":29,"user_id":4,"title":"The best private company in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-21 11:31:24","end_date":"2015-10-28 11:31:24","exposure_center":"Nirman Vihar New delhi","radius":20,"banner_url":""}],"status":1}';
            response = JSON.parse(response);
            var ads = response.content;
            if (ads.length > 0) {
                for (var i = 0; i < ads.length; i++) {
                    var temp_start_date = response.content[i]['start_date'];
                    var zz = temp_start_date.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    ads[i]['start_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);

                    var temp_end_date = response.content[i]['end_date'];
                    var zz = temp_end_date.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    ads[i]['end_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                };
                $scope.noAds = false;
                $scope.ads = ads;
            } else {
                $scope.ads = ads;
                $scope.noAds = true;
            }
            //console.log(JSON.stringify($scope.ads));
        });
    }

}]);

LOMCityControllers.controller('createAdsCtrl', ["$rootScope","$scope", "$timeout", "$state",'$http','$filter','isValidToken', function ($rootScope,$scope, $timeout, $state, $http, $filter,isValidToken) {    
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.banner_url = "";
    $scope.amountValue = 0;
    $scope.Exposure_center = "";
    $scope.myFile = "";

    $scope.Start_date = new Date();
    $scope.End_date = new Date();
    var numberOfDaysToAdd = 7;
    $scope.End_date.setDate($scope.End_date.getDate() + numberOfDaysToAdd); 

    $scope.opened = false;
    $scope.opened1 = false;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }
    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    }

    $scope.createAds = function (){
        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var request = {
            title:$scope.Ad_title,
            description:$scope.Ad_description,
            start_date:$scope.Start_date,
            end_date:$scope.End_date,
            exposure_center:$scope.Exposure_center.formatted_address,
            radius:$scope.Exposure_radius,
            banner_url:$scope.banner_url,
            _token: localStorage.satellizer_token,
            location_lat: $scope.Exposure_center.geometry.location.G,
            location_long: $scope.Exposure_center.geometry.location.K,
            link: $scope.link,
            ad_amount: parseFloat($scope.amountValue)
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/createAds',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            console.log(JSON.stringify(response));
            $scope.fileUploading = false;
            if (response.status == 1) {
                $rootScope.msgDescription = response.msg;
                /*$("#alertModal").modal('show').on("shown.bs.modal", function () {
                    $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.manageAds', {});
                    }, 1500);                 
                });*/
                if(response.redirect_url != ''){
                    window.location.href = response.redirect_url;
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };

    $scope.uploadFile = function(){
        var file = $scope.myFile;
        var type = 'image';
        var fileName = file.name;
        console.dir(file);
        
        var uploadUrl = "http://viround.com/public/api/upload";
        console.log(type + ' ' + file + ' ' + fileName + ' ' + uploadUrl);
        var fd = new FormData();
        fd.append('type', type);
        fd.append('file', file);
        fd.append('fileName', fileName);

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(function (res) {
            console.log(JSON.stringify(res));
            $scope.banner_url = res.content;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.createAds();
            }, function (response) {
                $state.go('main.login', {});
            });
        })
        .error(function (res) {
            console.log(JSON.stringify(res));
            //$scope.createAds();
        });
    };

    $scope.createAdsNuploadFile = function() {
        if($scope.myFile != ''){
            $scope.fileUploading = true;
            $scope.uploadFile();
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.createAds();
            }, function (response) {
                $state.go('main.login', {});
            });
            $scope.fileUploading = true;
        }
    }

    $scope.getAdAmount = function (){
        var request = {
            location:$scope.Exposure_center,
            radius:$scope.Exposure_radius,
            startDate:$scope.Start_date,
            endDate:$scope.End_date
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/getAdAmount',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            //console.log(JSON.stringify(response));
             $scope.amountValue = response.amount;
        }).error(function (xhr, req, err) {
            if (req == 500) {
                console.log($filter('translate')('Error_msg10'));
            }
        });
    };

    $scope.testMethod = function (){
        var request = {
            payer_id:'34',
            payment_date:'testdate',
            payment_status: 'Completed',
            txn_id:'443',
            payment_gross:'43',
            item_number:37
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl+'/api/updatePaymentStatus',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            console.log(JSON.stringify(response));
        }).error(function (xhr, req, err) {
            if (req == 500) {
                console.log($filter('translate')('Error_msg10'));
            }
        });
    }

}]);


