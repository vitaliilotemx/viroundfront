var LOMCityControllers;

LOMCityControllers = angular.module('LOMCityControllers', []);

LOMCityControllers.controller('headerCtrl', ["$rootScope", "$scope", "$state", "$translate", "$http", "$timeout", 'SatellizerShared', "$http", "$interval", "$filter", function ($rootScope, $scope, $state, $translate, $http, $timeout, shared, $http, $interval, $filter) {
    $rootScope.$state = $state;

    $scope.stateReload = function () {
        window.location.reload(true);
    }

    $rootScope.language = ($rootScope.language != undefined) ? $rootScope.language : "en";
    $rootScope.termsofUse = "#termsOfUseModel-" + $rootScope.language;
    $rootScope.privacyPolicyModal = "#privacyPolicyModal-" + $rootScope.language;

    //$rootScope.mediaUrlS3 = 'https://d23ethjq7gcnl7.cloudfront.net/uploads/';
    $rootScope.mediaUrlS3 = 'https://dg4x38lr4ktft.cloudfront.net/';

    if (location.protocol != 'https:') {
        //bli  location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }

    $rootScope.monthArray = "Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec";

    var uagent = navigator.userAgent.toLowerCase();

    if (uagent.search("iphone") > -1) {
        $rootScope.deviceType = "iphone";
    } else if (uagent.search("android") > -1) {
        $rootScope.deviceType = "android";
    } else {
        $rootScope.deviceType = "web";
    }
    $scope.selectedLanguage = "en";

    var language_used = localStorage.getItem('language');
    //console.log("language_used" + language_used)
    if (language_used != null) {
        $scope.selectedLanguage = language_used;
    } else {
        $scope.selectedLanguage = "en";
    }
    $scope.changeLanguage = function (language) {
        //console.log(language);
        localStorage.setItem('language', language);

        $rootScope.language = language;
        $translate.use(language);

        $rootScope.termsofUse = "#termsOfUseModel-" + $rootScope.language;
        $rootScope.privacyPolicyModal = "#privacyPolicyModal-" + $rootScope.language;
        $state.reload();
    }

    $scope.changeLanguageAuto = function (lang) {
        //console.log("mylanguage"+lang);
        localStorage.setItem('language', lang);

        $rootScope.language = lang;
        $translate.use(lang);

        $rootScope.termsofUse = "#termsOfUseModel-" + $rootScope.language;
        $rootScope.privacyPolicyModal = "#privacyPolicyModal-" + $rootScope.language;
    }

    if (language_used != "null" && language_used != "") {
        //alert("Here"+language_used);
        $scope.changeLanguageAuto(language_used);
    } else {
        var languages = ['en', 'ru', 'fr', 'es', 'pt', 'ar', 'he'];
        var Device_lang = (navigator.language || navigator.userLanguage).split('-')[0];
        if ($.inArray(Device_lang, languages) != -1) {
            language = Device_lang;
        } else {
            language = 'en';
        }
        $scope.selectedLanguage = language;
        $scope.changeLanguageAuto($scope.selectedLanguage);
    }

    $rootScope.reloadHome = function () {
        //alert($state.current.name);
        //window.location.href = "#/main/allEvents";
        window.location.replace("#/main/allEvents");
        if ($state.current.name == "main.allEvents") {
            window.location.reload(true);
        } else {
            $state.go('main.allEvents', {});
        }
    }

    $scope.logOut = function () {
        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/logOut',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                //localStorage.clear();
                localStorage.removeItem('satellizer_token');
                $state.go('main.login', {});

            }
        });
    }

    var getCount = $interval(function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getUnreadCount',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $rootScope.unreadMessages = response.count;


        }).error(function (xhr, req, err) {
        });
    }, 60000);

    var getNotifications = $interval(function () {
        var request = {
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getCountNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.unreadCommentCount = response.unreadCommentCount;
        }).error(function (xhr, req, err) {
        });
    }, 55000);

    $scope.getNotify = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getGroupNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            var sorted = _.orderBy(response.groupNotify, "created_at");
            sorted.sort((a, b) => (b.created_at > a.created_at) ? 1 : -1);
            $scope.groupNotify = sorted;
            $scope.groupNotifyLen = _.size($scope.groupNotify);
        }).error(function (xhr, req, err) {
        });
    }

    $scope.markAllAsRead = function () {
        var request = {
            _token: localStorage.satellizer_token,
            markComment: 1
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getUnreadCount',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "success");
        });

    }

    $scope.deleteNotify = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/deleteNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "success");
        });

    }

    $scope.readNotify = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            object_id: id
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/readNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    $scope.readAllNotify = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/readAllNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    $scope.readAllNotifyOnEvent = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/readAllNotifyOnEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    var mr_firstSectionHeight,
        mr_nav,
        mr_navOuterHeight,
        mr_navScrolled = false,
        mr_navFixed = false,
        mr_outOfSight = false,
        mr_floatingProjectSections,
        mr_scrollTop = 0;

    $('.inner-link').each(function () {
        var href = $(this).attr('href');
        if (href)
            if (href.charAt(0) !== "#") {
                $(this).removeClass('inner-link');
            }
    });

    // Update scroll variable for scrolling functions

    addEventListener('scroll', function () {
        mr_scrollTop = window.pageYOffset;
    }, false);

    // Navigation
    $(window).resize(function () {
        $('.mobile-toggle').removeClass('active');
        $('.nav-bar').removeClass('nav-open');
        $('.module.widget-handle').removeClass('toggle-widget-handle');
    });
    if (!$('nav').hasClass('fixed') && !$('nav').hasClass('absolute')) {

        // Make nav container height of nav

        $('.nav-container').css('min-height', $('nav').outerHeight(true));

        $(window).resize(function () {
            $('.nav-container').css('min-height', $('nav').outerHeight(true));
        });

        // Compensate the height of parallax element for inline nav

        if ($(window).width() > 768) {
            $('.parallax:nth-of-type(1) .background-image-holder').css('top', -($('nav').outerHeight(true)));
        }

        // Adjust fullscreen elements

        if ($(window).width() > 768) {
            $('section.fullscreen:nth-of-type(1)').css('height', ($(window).height() - $('nav').outerHeight(true)));
        }

    } else {
        $('body').addClass('nav-is-overlay');
    }

    if ($('nav').hasClass('bg-dark')) {
        $('.nav-container').addClass('bg-dark');
    }

    // Fix nav to top while scrolling

    mr_nav = $('body .nav-container nav:first');
    mr_navOuterHeight = $('body .nav-container nav:first').outerHeight();
    window.addEventListener("scroll", updateNav, false);

    // Menu dropdown positioning

    $('.menu > li > ul').each(function () {
        var menu = $(this).offset();
        var farRight = menu.left + $(this).outerWidth(true);
        if (farRight > $(window).width() && !$(this).hasClass('mega-menu')) {
            $(this).addClass('make-right');
        } else if (farRight > $(window).width() && $(this).hasClass('mega-menu')) {
            var isOnScreen = $(window).width() - menu.left;
            var difference = $(this).outerWidth(true) - isOnScreen;
            $(this).css('margin-left', -(difference));
        }
    });

    // Mobile Menu

    $('.mobile-toggle').click(function () {
        $('.nav-bar').toggleClass('nav-open');
        $(this).toggleClass('active');
    });

    $('.menu li').click(function (e) {
        if (!e) e = window.event;
        e.stopPropagation();
        if ($(this).find('ul').length) {
            $(this).toggleClass('toggle-sub');
        } else {
            $(this).parents('.toggle-sub').removeClass('toggle-sub');
        }
    });

    $('.module.widget-handle').click(function () {
        $(this).toggleClass('toggle-widget-handle');
    });

    $('.search-widget-handle .search-form input').click(function (e) {
        if (!e) e = window.event;
        e.stopPropagation();
    });

    $scope.userInformation = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getUserInformation',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                $rootScope.userInfo = response;
                $rootScope.currentUser_id = response.id;
                $rootScope.currentUser_firstName = response.firstName;
                $rootScope.currentUser_lastName = response.lastName;
                $rootScope.currentUser_avatar = response.avatar;
                $rootScope.verificated = response.verificated;
                //$rootScope.notifyRadius = response.notifyRadius;
                if (response.notifyUnits === 'mile') {
                    $rootScope.userInfo.notifyRadius = $rootScope.userInfo.notifyRadius / 0.62137;
                    $rootScope.userInfo.notifyRadius = $rootScope.userInfo.notifyRadius.toFixed(1);
                }
                $rootScope.verificate_message = $filter('translate')('verificate_message');
                //console.log(response);
            }
        });
    }

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            $scope.$apply(function () {
                $rootScope.userPosition = position.coords;
                $rootScope.userPositionLat = position.coords.latitude;
                $rootScope.userPositionLong = position.coords.longitude;
                //console.log($rootScope.userPositionLat,$rootScope.userPosition);
            });
        });
    }

    $scope.showNewCommentsInfo = function () {

        if ($('#commentNotific').is(':visible')) {
            $('#commentNotific').hide();
        } else {
            $('#commentNotific').show();
        }
    }

    $(document).mouseup(function (e) { // событие клика по веб-документу
        var div = $("#commentNotific"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            div.hide(); // скрываем его
        }
    });

}]);

LOMCityControllers.controller('homeCtrl', ["$rootScope", "$state", '$stateParams', 'title', "$cookies", function ($rootScope, $state, $stateParams, title, $cookies) {
    //console.log(title+'  '+$stateParams.param1);
    //console.log($stateParams.param1);
    if ($stateParams.param1 == "") {
        $cookies.inviter_id = null;
    } else {
        $cookies.inviter_id = $stateParams.param1;
    }

    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });

    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);


}]);

LOMCityControllers.controller('loginCtrl', ["$rootScope", "$scope", "$state", "$http", "$auth", "$filter", "OpenFB", 'Facebook', function ($rootScope, $scope, $state, $http, $auth, $filter, OpenFB, Facebook) {
    //Facebook

    $scope.loginStatus = 'disconnected';
    $scope.facebookIsReady = false;

    /*    document.addEventListener("DOMContentLoaded", function () {
              VK.init({
                  //apiId: 5914175
                  apiId: 6838530
              });
          });*/


    $rootScope.getLoginFb = function (fbConnect) {
        FB.api('/me?fields=name,email', function (response) {
            //console.log('Successful login for: ' + response.name);
            // console.log(response);
            $scope.fbLogin(fbConnect, response);
        });
    }
    $rootScope.checkLoginState = function () {
        //console.log("Check login facebook");
        //$rootScope.showLoading();
        FB.getLoginStatus(function (response) {
            //alert(JSON.stringify(response));
            $rootScope.statusChangeCallback(response);
        });
    }
    $scope.fbLogin = function (fbConnect, fbUser) {
        var fb_user = {
            email: fbUser.email,
            password: fbUser.email,
            fb_token: fbConnect.authResponse.accessToken
        }
        $auth.login(fb_user).catch(function (response) {
            // console.log(JSON.stringify(response));
            $rootScope.hideLoading();
            if (response.status == 401) {
                $rootScope.msgDescription = $filter('translate')('Invalid_email_pass');
                $("#alertModal").modal('show');
                return;
            }
        }).then(function (response) {
            //console.log(response.data.email);
            localStorage.setItem('loginDetail', JSON.stringify(fb_user));
            //console.log(JSON.stringify(request));
            $rootScope.hideLoading();
            if (response != undefined && response.status == 200) {
                //console.log('*'+response.status);
                $state.go('main.allEvents', {});
            }
        });
    };

    $scope.removeAuth = function () {
        Facebook.api({
            method: 'Auth.revokeAuthorization'
        }, function (response) {
            Facebook.getLoginStatus(function (response) {
                //$scope.loginStatus = response.status;
                alert("Here " + JSON.stringify(response));
            });
        });
    };

    $scope.api = function () {
        Facebook.api('/me', function (response) {
            //$scope.user = response;
            //alert("Here User"+ JSON.stringify(response));
        });
    };

    $scope.$watch(function () {
            return Facebook.isReady();
        }, function (newVal) {
            if (newVal) {
                $scope.facebookIsReady = true;
            }
        }
    );

    //Facebook

    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);

    $scope.user = {
        email: '',
        password: ''
    };
    //localStorage.clear();
    $scope.rememberMe = false;
    var loginDetail = localStorage.getItem('loginDetail');
    //console.log(loginDetail);
    if (loginDetail != "" && loginDetail != null && loginDetail != undefined) {
        loginDetail = JSON.parse(loginDetail);
        if (loginDetail.rememberMe) {
            $scope.rememberMe = true;
            $scope.user.email = loginDetail.email;
            $scope.user.password = loginDetail.password;
        }
    }

    $scope.login = function (user) {
        if (user == undefined || user.email == '' || user.password == '') {
            $rootScope.msgDescription = 'Please enter required values.';
            $("#alertModal").modal('show');
        } else {
            var request = {
                email: user.email,
                password: user.password,
                rememberMe: $scope.rememberMe
            };

            //console.log(JSON.stringify(request));
            $auth.login(request).then(function (response) {
                //console.log(JSON.stringify(response.data.user));
                $scope.userVerificated = response.data.user.verificated;
                //alert(JSON.stringify(response.data.user.email));
                //alert(JSON.stringify('!!!'));
                if ($scope.userVerificated == 1) {
                    localStorage.setItem('loginDetail', JSON.stringify(request));
                    //console.log(JSON.stringify(request));
                    if (response != undefined && response.status == 200) {
                        //console.log('*'+response.status);
                        $state.go('main.allEvents', {});
                    }
                } else {
                    $scope.sendEmailVerificateRequest = {
                        email: response.data.user.email,
                        firstName: response.data.user.firstName,
                        lastName: response.data.user.lastName,
                        hi: $filter('translate')('email_page_hi'),
                        emailText: $filter('translate')('email_page_text'),
                        emailButton: $filter('translate')('email_page_button'),
                        emailFooter: $filter('translate')('email_page_footer'),
                        emailAfterButton: $filter('translate')('email_after_button')
                    };

                    $scope.sendVerificateSmsRequest = {
                        phone: response.data.user.phone,
                        code: response.data.user.sms_code
                    };

                    $rootScope.msgDescription = "";
                    $("#badVerification").modal('show');

                    $scope.logOut();
                }

            }).catch(function (response, error) {
                //console.log(error);
                //alert(JSON.stringify(response));
                if (response.status == 500 || response.status == 401) {
                    $rootScope.msgDescription = $filter('translate')('Invalid_email_pass');
                    $("#alertModal").modal('show');
                    return;
                }
            });
        }
    }

    $scope.sendEmailVerificate = function () {
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendEmailVerificate',
            data: {data: JSON.stringify($scope.sendEmailVerificateRequest)}
        }).success(function (response) {
            if (response.status == 1) {
                $rootScope.msgDescription = $filter('translate')('verificate_sended_text');

                //alert(JSON.stringify(response.data));
            }
        }).error(function (xhr, req, err) {
            $rootScope.msgDescription = "error: " + req;
        });
    }

    $scope.sendVerificateCodeBySms = function () {
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendSmsVerificate',
            data: {data: JSON.stringify($scope.sendVerificateSmsRequest)}
        }).success(function (response) {
            if (response.status == 1) {
                $rootScope.msgDescription = $filter('translate')('verificate_sended_text');
            }
        }).error(function (xhr, req, err) {
            $rootScope.msgDescription = "error: " + req;
        });
    }

    $scope.verificateByCode = function () {
        if ($scope.smsCode) {

            var request = {
                code: $scope.smsCode
            };

            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/verificateBySmsCode',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                if (response.status == 1) {
                    $.notify('OK', "success");
                }
            }).error(function (xhr, req, err) {
                $rootScope.msgDescription = "error: " + req;
            });

        }
    }

    $scope.logOut = function () {
        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/logOut',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                //localStorage.clear();
                localStorage.removeItem('satellizer_token');
                $state.go('main.login', {});
            }
        });
    }

    function successHandler(response) {
        alert(response);
    };

    function errorHandler(error) {
        alert(error);
    }

    $scope.verifyEmailId = function () {
        if ($scope.user.email == '') {
            $rootScope.msgDescription = $filter('translate')('Error_msg5');
            $("#alertModal").modal('show');
            return;
        }
        var request = {
            email: $scope.user.email
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/SendForgotPasswordEmailToUser',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                //$state.go('main.forgotPass', { 'email':  $scope.user.email});
                //template: $filter('translate')('check_mail')
                $rootScope.msgDescription = $filter('translate')('check_mail');
                $("#alertModal").modal('show');
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg4');
                $("#alertModal").modal('show');
            }
        });
    }


    $rootScope.authenticateInstagram = function () {
        var accessToken = null;
        var instagramClientId = '484be4ea496c4e38bb03a304d1269428';
        var instagramRedirectUri = 'https://viround.com';

        console.log(0);
        // Pop-up window size, change if you want
        var popupWidth = 700,
            popupHeight = 500,
            popupLeft = (window.screen.width - popupWidth) / 2,
            popupTop = (window.screen.height - popupHeight) / 2;
        // Url needs to point to instagram_auth.php
        var popup = window.open('https://instagram.com/oauth/authorize/?client_id=' + instagramClientId + '&redirect_uri=' + instagramRedirectUri + '&response_type=token', '', 'width=' + popupWidth + ',height=' + popupHeight + ',left=' + popupLeft + ',top=' + popupTop + '');
        console.log(1);

        popup.onload = function () {

            if (window.location.hash.length) {
                setTimeout(function () {
                    var searchParams = new URLSearchParams(window.location.hash);
                    console.log(searchParams.toString());
                    var forceAuthentication = window.location.hash.split('force_authentication=', '');
                    console.log(forceAuthentication);
                    var accessToken = window.location.hash.split('access_token=', '');
                    console.log(accessToken);
                }, 5000);
            }

            // Open authorize url in pop-up
            if (window.location.hash.length == 0) {
                popup.open('https://instagram.com/oauth/authorize/?client_id=' + instagramClientId + '&redirect_uri=' + instagramRedirectUri + '&response_type=token', '_self');
                console.log(2);
            }
            // An interval runs to get the access token from the pop-up
            var interval = setInterval(function () {
                console.log(3);
                try {
                    // Check if hash exists
                    if (popup.location.hash.length) {

                        console.log(4);
                        // Hash found, that includes the access token
                        clearInterval(interval);
                        accessToken = popup.location.hash.slice(15); //slice #access_token= from string
                        var getUrl = "https://api.instagram.com/v1/users/self/?access_token=" + accessToken;
                        $.get(getUrl)
                            .done(function (result) {
                                console.log(5);
                                $http({
                                    method: 'POST',
                                    url: $rootScope.baseUrl + '/api/instagramLogin',
                                    data: {data: JSON.stringify(result.data)}
                                }).success(function (response) {
                                    console.log(JSON.stringify(result.data));
                                    console.log(JSON.stringify(response.data));
                                    //console.log(response);
                                    var _token = response.token;
                                    localStorage.satellizer_token = _token;
                                    $state.go('main.allEvents', {});

                                }).error(function (xhr, req, err) {
                                    if (req == 500) {
                                        $rootScope.msgDescription = $filter('translate')('Error_msg10');
                                        $("#alertModal").modal('show');
                                    }
                                });

                            });
                        console.log(7);
                        popup.close();
                    }
                } catch (evt) {
                    // Permission denied
                }
            }, 100);
        };
    };

    $scope.vkAuth = function () {

        $.getScript("https://vkontakte.ru/js/api/openapi.js");

        VK.init({
            //apiId: 5914175
            apiId: 6838530
        });

        VK.Auth.login(function authInfo(response) {
            if (response.session) {
                console.log(JSON.stringify(response));
                // send all info to server
                $.post("https://viround.com/public/api/vk", response)
                    .done(function (data) {
                        console.log(JSON.stringify(data));
                        var _token = data.token;
                        localStorage.satellizer_token = _token;
                        window.location = "#/main/allEvents";
                    });

            } else {
                alert('not auth');
            }
        });
    }
}]);

LOMCityControllers.controller('signupCtrl', ["$rootScope", "$scope", "$state", '$http', "$filter", "$cookies", function ($rootScope, $scope, $state, $http, $filter, $cookies) {
    $('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);

    var inviter_id = $cookies.inviter_id;

    $rootScope.termsofUse = "#termsOfUseModel-" + $rootScope.language;
    $rootScope.privacyPolicyModal = "#privacyPolicyModal-" + $rootScope.language;

    $rootScope.checkLoginState = function () {
        //console.log("Check login facebook");
        //$rootScope.showLoading();
        FB.getLoginStatus(function (response) {
            $rootScope.statusChangeCallback(response);
        });
    }

    $scope.signUp = function (user) {
        if (inviter_id == null || inviter_id == undefined || inviter_id == "") {
            inviter_id = 0;
        }
        if (user.password == user.rpassword) {
            var request = {
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                password: user.password,
                inviter_id: parseInt(inviter_id),
                location: user.eventAlertAddress,
                lat: user.eventAlertAddress.geometry.location.lat(),
                long: user.eventAlertAddress.geometry.location.lng(),
                name: user.eventAlertAddress.formatted_address,
                hi: $filter('translate')('email_page_hi'),
                emailText: $filter('translate')('email_page_text'),
                emailButton: $filter('translate')('email_page_button'),
                emailFooter: $filter('translate')('email_page_footer'),
                emailAfterButton: $filter('translate')('email_after_button')
            };
            //console.log(JSON.stringify(request));
            if (user.id != undefined) {
                request.id = user.id
            }
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/register',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                //alert(JSON.stringify(response));
                if (response.status == 1) {
                    $cookies.inviter_id = null;
                    $rootScope.inviter_id = response.user_id;
                    //$rootScope.inviter_id = 122;
                    //$state.go('main.confirmation', {});
                    $rootScope.msgDescription = $filter('translate')('registred_user_text');
                    $("#registredModal").modal('show');
                    $state.go('main.allEvents', {});
                } else if (response.status == 0) {
                    $rootScope.msgDescription = response.msg;
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                $rootScope.msgDescription = $filter('translate')('Error_msg12');
                $("#alertModal").modal('show');
                // if (req == 500) {
                //     $rootScope.msgDescription = $filter('translate')('Error_msg12');
                //     $("#alertModal").modal('show');
                // }
            });
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg6');
            $("#alertModal").modal('show');
        }
    }

    $scope.signUpByPhone = function (user) {
        if (inviter_id == null || inviter_id == undefined || inviter_id == "") {
            inviter_id = 0;
        }
        if (user.password == user.rpassword) {
            var request = {
                firstName: user.firstName,
                lastName: user.lastName,
                phone: user.phone,
                password: user.password,
                inviter_id: parseInt(inviter_id),
                location: user.eventAlertAddress,
                lat: user.eventAlertAddress.geometry.location.lat(),
                long: user.eventAlertAddress.geometry.location.lng(),
                name: user.eventAlertAddress.formatted_address,
            };
            console.log(JSON.stringify(request));
            if (user.id != undefined) {
                request.id = user.id
            }
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/register',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                console.log(JSON.stringify(response));
                if (response.status == 1) {
                    $cookies.inviter_id = null;
                    $rootScope.inviter_id = response.user_id;
                    $rootScope.msgDescription = $filter('translate')('registred_user_text');
                    $("#registredModal").modal('show');
                    $state.go('main.allEvents', {});
                } else if (response.status == 0) {
                    $rootScope.msgDescription = response.msg;
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg12');
                    $("#alertModal").modal('show');
                }
            });
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg6');
            $("#alertModal").modal('show');
        }
    }

    $scope.vkAuth = function () {
        VK.init({
            //apiId: 5914175
            apiId: 6838530
        });

        VK.Auth.login(function authInfo(response) {
            if (response.session) {
                console.log(JSON.stringify(response));
                // send all info to server
                $.post("https://viround.com/public/api/vk", response)
                    .done(function (data) {
                        console.log(JSON.stringify(data));
                        var _token = data.token;
                        localStorage.satellizer_token = _token;
                        window.location = "#/main/allEvents";
                    });

            } else {
                alert('not auth');
            }
        });
    }

}]);

LOMCityControllers.controller('forgotPassCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', '$stateParams', function ($rootScope, $scope, $timeout, $state, $http, $filter, $stateParams) {
    /*$('.background-image-holder').each(function () {
        var imgSrc = $(this).children('img').attr('src');
        $(this).css('background', 'url("' + imgSrc + '")');
        $(this).children('img').hide();
        $(this).css('background-position', 'initial');
    });
    setTimeout(function () {
        $('.background-image-holder').each(function () {
            $(this).addClass('fadeIn');
        });
    }, 200);
    $scope.user = {
        password: '',
        cpassword: '',
        user_id: $stateParams.user_id,
        user_auth: $stateParams.user_auth
    }
    $scope.forgotPassword = function () {
        if ($scope.user.password == '' || $scope.user.password != $scope.user.cpassword) {
            $rootScope.msgDescription = $filter('translate')('Error_msg8');
            $("#alertModal").modal('show');
            return;
        }
        var request = {
            user_id: $scope.user.user_id,
            user_auth: $scope.user.user_auth,
            password: $scope.user.password
        };
        console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/changePass',
            data: { data: JSON.stringify(request) }
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                $rootScope.msgDescription = 'Change password successfully.';
                $("#alertModal").modal('show').on("shown.bs.modal", function () {
                    $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.login', {});
                    }, 1000);
                });
            }
        }).error(function (xhr, req, err) {
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }*/
    //$state.reload();

    var user_id = $stateParams.user_id;
    var user_auth = $stateParams.user_auth;

    $scope.sendNewPassword = function () {

        var request = {
            user_id: user_id,
            user_auth: user_auth,
            password: $scope.passwordConfirm,
            /*            email: $scope.email, */
        };

        console.log(JSON.stringify(request));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/changePass',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(JSON.stringify(response));
            /*if (response.status == 1) {
                $rootScope.msgDescription = 'Change password successfully.';
                $("#alertModal").modal('show').on("shown.bs.modal", function () {
                   $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.login', {});
                    }, 1000);
                });

            }*/
            $.notify('Change password successfully.', "success");
            $state.go('main.login', {});
        }).error(function (xhr, req, err) {
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });

    }

}]);

LOMCityControllers.controller('allEventsCtrl', ["$rootScope", "$scope", "$state", '$http', 'dhm', 'getDist', "$filter", "$sce", "$cookies", '$stateParams', 'isValidToken', 'dhmFilter', 'getLocationDistance', '$timeout', '$interval', '$location', '$anchorScroll', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams, isValidToken, dhmFilter, getLocationDistance, $timeout, $interval, $location, $anchorScroll) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        //$scope.topMap();
        var isCatOptReady = $interval(function () {
            if ($rootScope.optionsReady) {
                if ($state.current.name == "main.allEvents") {
                    $scope.selectedPOption = $rootScope.selectedPOption;
                    $scope.changePCat();
                }
                $scope.getPreferredLocations();
                $interval.cancel(isCatOptReady);
            }
        }, 10);
    }, function (response) {
        $state.go('main.login', {});
    });
    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = false;
    $scope.events = [];
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.SearchVal = false;
    $scope.FilterVal = false;
    $scope.locationSelected = false;
    $scope.loading = false;
    $scope.favLocLength = 0;
    $scope.isLocationEnabled = false;
    $scope.searchByLocation = false;
    $scope.selectedOption = "";
    $scope.selectedPOption = "";
    $scope.newChildCategories = [];
    $scope.mapLoading = false;
    $scope.mapViewUsing = false;

    if ($rootScope.backToMap) {
        window.location.reload(true);
    }

    console.log($rootScope.backToMap);

    $scope.showEvents = localStorage.getItem('showEvents');
    if ($scope.showEvents == 'list') {
        $scope.showListEvents = true;
        $scope.showMapEvents = false;
    } else if ($scope.showEvents == 'map') {
        $scope.showListEvents = false;
        $scope.showMapEvents = true;
    } else {
        $scope.showListEvents = true;
        $scope.showMapEvents = false;
    }

    $scope.catagorySelected = function (index) {
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $rootScope.chooseOptions[index].name;
            var pCat = $rootScope.chooseOptions[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;

        } else {
            $scope.selectedOption = "";
        }
        $scope.search();

    }
    $scope.catagoryFilteredSelected = function (index) {
        //alert("catagoryFilteredSelected "+index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $scope.newChildCategories[index].name;
            var pCat = $scope.newChildCategories[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;

        } else {
            $scope.selectedOption = "";
        }
        $scope.search();

    }
    $scope.catagoryPSelected = function (index) {
        //console.log("catagoryPSelected " + index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $(".catPBtn").removeClass('button-dark');
        $("#btnp" + index).addClass('button-dark');
        $("#btnall").addClass('button-dark');
        if (index != "all") {
            var cat = $rootScope.parentCategories[index].name;
            var catVal = $filter('translate')(cat);
            $scope.selectedPOption = cat;
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
            for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                if (cat == $rootScope.chooseOptions[j].parentCategory) {
                    $scope.newChildCategories.push($rootScope.chooseOptions[j])
                } else {
                }
            }
        } else {
            $scope.selectedPOption = "";
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
        }
        $scope.search();

        // $timeout(function () {
        //   if(document.querySelector('i[class="fa fa-folder-open fa-lg"]').innerText == ''){
        //     document.querySelector('i[class="fa fa-folder-open fa-lg"]').innerHTML += $filter('translate')('all_sub_categories');
        //   }
        // }, 10);
    }
    $scope.catagoryChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catBtn").removeClass('button-active');
            $("#btn" + index).addClass('button-active');
            if (index != "all") {
                var cat = $rootScope.chooseOptions[index].name;
                var catVal = $filter('translate')(cat);
                $scope.selectedOption = cat;
            } else {
                $scope.selectedOption = "";
            }
        }, 1000);
        //$scope.search();
    }
    $scope.changeCat = function () {
        //alert($rootScope.chooseOptions);
        if ($scope.newChildCategories.length == 0) {
            for (var i = $rootScope.chooseOptions.length - 1; i >= 0; i--) {
                if ($scope.selectedOption == $rootScope.chooseOptions[i].name) {
                    $scope.catagoryChangeSelected(i)
                }
            }
        } else {
            for (var l = $scope.newChildCategories.length - 1; l >= 0; l--) {
                if ($scope.selectedOption == $scope.newChildCategories[l].name) {
                    //alert("l"+l)
                    $scope.catagoryChangeSelected(l)
                }
            }
        }
    }
    $scope.catagoryPChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catPBtn").removeClass('button-dark');
            $("#btnp" + index).addClass('button-dark');
            if (index != "all") {
                var cat = $rootScope.parentCategories[index].name;
                //var catVal = $filter('translate')(cat);
                $scope.selectedPOption = cat;
            } else {
                $scope.selectedPOption = "";
            }
        }, 1000);
    }
    $scope.changePCat = function () {
        //return;
        for (var i = $rootScope.parentCategories.length - 1; i >= 0; i--) {
            //console.log($scope.selectedPOption + "    :     " + $rootScope.chooseOptions[i].name)
            if ($scope.selectedPOption == $rootScope.parentCategories[i].name) {

                //$scope.catagoryPSelected(i);
                //$scope.catagoryPChangeSelected(i)
                $scope.results_from = 0;
                $(".catBtn").removeClass('button-active');
                $(".catPBtn").removeClass('button-dark');
                $("#btnp" + i).addClass('button-dark');
                $("#btnall").addClass('button-dark');
                if (i != "all") {
                    var cat = $rootScope.parentCategories[i].name;
                    var catVal = $filter('translate')(cat);
                    $scope.selectedPOption = cat;
                    //$scope.selectedOption = "";
                    $scope.newChildCategories = [];

                    for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                        if (cat == $rootScope.chooseOptions[j].parentCategory) {
                            $scope.newChildCategories.push($rootScope.chooseOptions[j])
                        } else {
                        }
                    }

                } else {
                    $scope.selectedPOption = "";
                    $scope.selectedOption = "";
                    $scope.newChildCategories = [];
                }
                $scope.catagoryPChangeSelected(i);

            }
        }
    }

    $scope.searchKeyPress = function (e) {
        e = e || window.event;
        if (e.keyCode == 13) {
            if ($scope.locationSelected) {
                $scope.search();
            }
        }
    }

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.switchView = function (event) {
        if (event.target.classList.contains('active')) {
        } else {
            $scope.showListEvents = !$scope.showListEvents;
            $scope.showMapEvents = !$scope.showMapEvents;
            if ($scope.showListEvents) {
                localStorage.setItem('showEvents', 'list');
                $state.reload();
                $scope.mapViewUsing = false;
            } else {
                localStorage.setItem('showEvents', 'map');
                $scope.mapViewUsing = true;
            }
            $scope.getContent($scope.SearchVal, $scope.FilterVal);

        }
    }

    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height()) == ($(document).height())) {
            if (($state.includes('main.allEvents') || $state.includes('main.allEventsFiltered'))) {
                if ($scope.load_events) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        if (!$scope.loading && $scope.load_events) {
                            $scope.loading = true;
                            $scope.load_events = false;
                            $scope.getContent($scope.SearchVal, $scope.FilterVal);
                            //$scope.search();
                        }
                        //$scope.getContent(false,false);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                }
            }
        }
    });

    $scope.searchModel = {};
    $scope.checkLocFromStorage = false;

    //console.log($stateParams.param2+"  "+$stateParams.param1);
    $scope.searchModel.FilterVal = "Location";
    $scope.isLocation = true;

    if ($stateParams.param2 == "Location" && $stateParams.param1 != "") {
        var SearchVal = localStorage.getItem('SearchVal');
        $scope.SearchVal = JSON.parse(SearchVal);
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchLocationVal = $stateParams.param1;
        $scope.selectedOption = $stateParams.param3;
        $scope.selectedPOption = $stateParams.param4;
        $scope.checkLocFromStorage = true;
        $scope.isLocation = true;
        if ($rootScope.optionsReady) {
            $scope.changePCat();
            $scope.changeCat();
        } else {
            var isCatReady = $interval(function () {
                if ($rootScope.optionsReady) {
                    $scope.changePCat();
                    $scope.changeCat();
                    $interval.cancel(isCatReady);
                }
            }, 10);
        }
    } else if ($stateParams.param2 == "Title" || $stateParams.param2 == "User") {
        $scope.SearchVal = $stateParams.param1;
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchVal = $stateParams.param1;
        $scope.searchModel.SearchLocationVal = "";
        $scope.selectedOption = $stateParams.param3;
        $scope.selectedPOption = $stateParams.param4;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = false;
        if ($rootScope.optionsReady) {
            $scope.changePCat();
            $scope.changeCat();
        } else {
            var isCatReady = $interval(function () {
                if ($rootScope.optionsReady) {
                    $scope.changePCat();
                    $scope.changeCat();
                    $interval.cancel(isCatReady);
                }
            }, 10);
        }
    } else if ($stateParams.param2 == undefined && $stateParams.param1 != undefined) {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = true;
        $scope.searchModel.FilterVal = "Location";
    } else {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = true;
        $scope.searchModel.FilterVal = "Location";
    }

    $scope.search = function () {
        $scope.results_from = 0;
        $scope.load_events = false;
        var paramObj = {param1: "", param2: "", param3: "", param4: ""};
        if ($scope.searchModel.FilterVal == "Location" && $scope.searchModel.SearchLocationVal != undefined && $scope.searchModel.SearchLocationVal != '') {
            promise = isValidToken.check();
            promise.then(function (token) {

                var locations = {
                    lat: "",
                    long: "",
                    name: ""
                }
                //alert($scope.searchModel.SearchLocationVal);
                if (!$scope.checkLocFromStorage) {
                    locations.lat = $scope.searchModel.SearchLocationVal.geometry.location.lat();
                    locations.long = $scope.searchModel.SearchLocationVal.geometry.location.lng();
                    locations.name = $scope.searchModel.SearchLocationVal.formatted_address;
                } else {
                    var SearchVal = localStorage.getItem('SearchVal');
                    locations.lat = JSON.parse(SearchVal).lat;
                    locations.long = JSON.parse(SearchVal).long;
                    locations.name = JSON.parse(SearchVal).name;
                }
                paramObj = {
                    param1: locations.name,
                    param2: $scope.searchModel.FilterVal,
                    param3: $scope.selectedOption,
                    param4: $scope.selectedPOption
                }
                localStorage.setItem('SearchVal', JSON.stringify(locations));
                $state.go('main.allEventsFiltered', paramObj, {reload: true});
                $scope.searchByLocation = true;
                $scope.locationSelected = false;

                /*//$scope.searchModel.SearchVal = "";
                $scope.events = [];
                $scope.getContent(locations,$scope.searchModel.FilterVal);
                $scope.locationSelected = false;*/
            }, function (response) {
                $state.go('login', {});
            });
        } else if ($scope.searchModel.FilterVal == "Title" || $scope.searchModel.FilterVal == "User") {
            paramObj = {
                param1: $scope.searchModel.SearchVal,
                param2: $scope.searchModel.FilterVal,
                param3: $scope.selectedOption,
                param4: $scope.selectedPOption
            }
            promise = isValidToken.check();
            promise.then(function (token) {
                localStorage.setItem('SearchVal', $scope.searchModel.SearchVal);
                $state.go('main.allEventsFiltered', paramObj, {reload: true});
                $scope.searchByLocation = false;
                /*
                $scope.events = [];
                $scope.searchByLocation = false;
                $scope.getContent($scope.searchModel.SearchVal,$scope.searchModel.FilterVal);*/
            }, function (response) {
                $state.go('login', {});
            });
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.events = [];
                $scope.searchByLocation = false;
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('login', {});
            });
        }
    }

    $scope.getGeolocation = function () {
        $scope.loading = true;
        $scope.topMap();
        if ($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined) {
            //alert('if');
            $scope.isLocationEnabled = true;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.searchByLocation = false;
                $scope.getContent(false, false);

            }, function (response) {
                $state.go('main.login', {});
            });

        } else if (navigator.geolocation) {
            //alert('navigator');
            var geo_options = {
                timeout: 27000
            };
            navigator.geolocation.getCurrentPosition(myLoc, locationError);
        } else {
            //alert('else');
            $scope.isLocationEnabled = false;
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                //$scope.getContent($scope.SearchVal, $scope.FilterVal);
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $scope.isLocationEnabled = true;
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        //console.log($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent(false, false);
            $scope.topMap();
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    function locationError(error) {
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        $scope.getContent(false, false);
        $scope.isLocationEnabled = false;
    }

    $scope.getNumberEvents = function () {
        $rootScope.numberOfIvents = document.querySelectorAll('[ng-repeat="event in events"]').length;
        //$rootScope.numberOfIvents = $scope.events.length;
    }

    if ($rootScope.backToPost) {
        if ($rootScope.numberOfIvents > 20) {
            $scope.results_num = $rootScope.numberOfIvents;
        }

        if ($rootScope.numberOfIvents > 100) {
            $scope.results_from = 100;
        }
        //alert($scope.results_num);
    } else {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
    }

    $scope.scrollTo = function (postId) {
        $timeout(function () {
            if ($rootScope.backToPost) {
                $location.hash(postId);
                $anchorScroll();
            }
            $rootScope.backToPost = null;
            $scope.results_num = 20;
        }, 100);
    };

    $scope.getContent = function (SearchVal, FilterVal) {
        $scope.loading = true;

        $scope.locSearchVal = $scope.SearchVal;
        geocoder = new google.maps.Geocoder();

        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }

        $rootScope.showLoading();

        var request = {
            range: $rootScope.range,
            lat: $rootScope.myLat,
            long: $rootScope.myLong,
            SearchVal: ($scope.SearchVal == undefined) ? "" : $scope.SearchVal,
            FilterVal: ($scope.FilterVal == undefined) ? "" : $scope.FilterVal,
            _token: localStorage.satellizer_token,
            category: ($stateParams.param3 != undefined) ? $stateParams.param3 : $scope.selectedOption,
            parentCategory: ($stateParams.param4 != undefined) ? $stateParams.param4 : $scope.selectedPOption,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };

        if ($rootScope.myLat == 0 || $rootScope.myLong == 0) {
            $scope.isLocationEnabled = false;
        }

        //console.log(JSON.stringify(request));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvents',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(response.content[0].mediaTypeIs.type);

            $rootScope.hideLoading();
            $scope.loading = false;
            //console.log(response);
            $scope.flat = response.flat;
            $scope.flong = response.flong;

            if (document.querySelector('.parCat.active-cat')) {
                document.querySelector('.parCat.active-cat').classList.remove('active-cat');
            }

            if (document.querySelector('.chilCat.active-cat')) {
                document.querySelector('.chilCat.active-cat').classList.remove('active-cat');
            }

            if (request.parentCategory) {
                //document.querySelector('.'+request.parentCategory).style.backgroundColor = 'red';
                document.querySelector('.parCat.' + request.parentCategory).classList.add('active-cat');
            }

            if (request.category) {
                //document.querySelector('.'+request.category).style.backgroundColor = 'red';
                document.querySelector('.chilCat.' + request.category).classList.add('active-cat');
            }

            if (response.status == 1) {
                var events = response.content;
                var newEvents = [];

                events.sort((a, b) => (b.created_at > a.created_at) ? 1 : -1);

                console.log(events);

                if (events.length > 0) {
                    if (events.length < 20) {
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder = new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {

                        if (events[i]['mediaThumb']) {
                            var mediaName = events[i]['mediaThumb'].split('/').pop();
                            events[i]['mediaThumb'] = $rootScope.mediaUrlS3 + mediaName;
                        }

                        if (events[i]['avatar'] == '') {
                            events[i]['profile_avatar'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
                        } else {
                            events[i]['profile_avatar'] = events[i]['avatar'];
                        }

                        var created_at_full = events[i]['created_at'].split(' ');
                        var created_at_date = created_at_full[0].split('-');
                        var created_at_new_date = created_at_date[2] + '-' + created_at_date[1] + '-' + created_at_date[0];
                        events[i]['created_at'] = created_at_new_date + ' ' + created_at_full[1];

                        //var time = dhmFilter(response.content[i]['created_at']);
                        // events[i]['rtime'] = time[0];
                        // $scope.isNow = time[1];
                        // $scope.showtime = time[2];
                        //
                        // events[i]['isNow'] = $scope.isNow;
                        // events[i]['showtime'] = $scope.showtime;
                        //
                        // var timeArr = String(events[i]['created_at']).split(" ");
                        // timeArr = String(timeArr[1]).split(":");
                        // events[i]['timeString'] = timeArr[0] + ":" + timeArr[1];

                        if ($scope.locSearchVal) {
                            events[i]['distance'] = getLocationDistance(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked, null, $scope.locSearchVal.lat, $scope.locSearchVal.long);
                        } else {
                            events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        }

                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }

                        events[i]['type'] = 0;

                        if (events[i]['mediaTypeIs']) {
                            events[i]['type'] = events[i]['mediaTypeIs'].type;
                        }

                        $scope.events.push(events[i]);
                        //console.log('$scope.events',$scope.events);

                    }
                    ;

                    $rootScope.latForMap = response.content[0]['location_lat'];
                    $rootScope.longForMap = response.content[0]['location_long'];
                    $scope.noEvents = false;
                    $scope.loading = false;

                } else {
                    if ($scope.events.length == 0) {
                        $scope.load_events = false;
                        $scope.events = events;
                        $scope.noEvents = true;
                    }
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            $scope.loading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getMapEvents = function (events) {
        var map = document.getElementById("allEventsMap");
        if (map) {
            $scope.Markers = events;
            $scope.MapOptions = {
                center: new google.maps.LatLng($rootScope.myLat, $rootScope.myLong),
                /* zoom: 15 - $rootScope.userInfo.notifyRadius,*/
                zoom: 16 - $rootScope.userInfo.notifyRadius,
                //zoom: 27 - $rootScope.userInfo.notifyRadius,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            $scope.Latlngbounds = new google.maps.LatLngBounds();
            $scope.Map = new google.maps.Map(document.getElementById("allEventsMap"), $scope.MapOptions);

            //Looping through the Array and adding Markers.
            for (var i = 0; i < $scope.Markers.length; i++) {
                var data = $scope.Markers[i];
                var myLatlng = new google.maps.LatLng(data.location_lat, data.location_long);

                //Initializing the Marker object.
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: $scope.Map,
                    icon: "img/map_icon.png",
                    title: data.title
                });

                //Adding InfoWindow to the Marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent('<div style = "max-width:300px;min-height:50px; text-align: center;"><a href="/#/main/event/' + data.id + '" target="_blank"><b>' + data.title + '</b></a><p>' + data.address + '</p><p><img src="' + data.mediaThumb + '" style="width:50%; height: 150px;"></p></div>');
                        $scope.InfoWindow.open($scope.Map, marker);
                    });
                })(marker, data);

                //Plotting the Marker on the Map.
                $scope.Latlngbounds.extend(marker.position);
            }

            //Adjusting the Map for best display.
            $scope.Map.setCenter($scope.Latlngbounds.getCenter());
            $scope.Map.fitBounds($scope.Latlngbounds);
        }

    }

    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getSettings ',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;

            $scope.favLocLength = 0;
            for (var i = $scope.preferredLocations.length - 1; i >= 0; i--) {
                if ($scope.preferredLocations[i].name != "") {
                    $scope.favLocLength++;
                }
            }

            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            //console.log(JSON.stringify(req));
        });
    }

    $scope.changeLocationState = function () {
        if ($scope.favLocLength > 0) {
            $state.go('main.myFavEvents', {});
        } else {
            $state.go('main.settings', {});
        }
    }

    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
            $scope.locationSelected = false;
        } else {
            $scope.locationSelected = true;
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

    $scope.newInputAttribute = function (val) {
        $scope.searchModel.FilterVal = val;
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
            $scope.locationSelected = false;
        } else {
            $scope.locationSelected = true;
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

    $scope.$on('g-places-autocomplete:select', function (event, param) {
        if ($scope.isLocation) {
            $scope.locationSelected = true;
            $scope.checkLocFromStorage = false;
        }
        ;
    });

    $scope.getAroundEvents = function (lat, lng) {

        $scope.mapLoading = true;


        var reqt = {
            lat: lat,
            lng: lng,
            category: ($stateParams.param3 != undefined) ? $stateParams.param3 : $scope.selectedOption,
            parentCategory: ($stateParams.param4 != undefined) ? $stateParams.param4 : $scope.selectedPOption,
            radiusKm: $rootScope.userInfo.notifyRadius,
            _token: localStorage.satellizer_token,
        };

        //console.log(reqt);

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/aroundEvents',
            data: {data: JSON.stringify(reqt)}
        }).success(function (response) {
            $scope.mapLoading = true;

            console.log(response);

            $scope.aEvents = response.aroundEvents;

            let respon = $scope.aEvents;
            let positions = ['0-0']
            let multipleEvents = []
            let oneEventPoints = []

            respon.map(item => {
                positions.map(pos => {
                    if (pos === item.location_lat + "-" + item.location_long)
                        item.unique = false
                    else item.unique = true
                })
                positions.push(item.location_lat + "-" + item.location_long)
            })

            respon.map(item => {
                if (item.unique) {
                    let count = 0
                    positions.map(pos => {
                        if (pos === item.location_lat + "-" + item.location_long)
                            count++
                    })
                    if (count > 1)
                        item.unique = false
                }
            })


            respon.map(item => {
                if (item.unique) {
                    oneEventPoints.push(item)
                } else {
                    let pushed = false
                    multipleEvents.map(mult => {
                        if (mult.lat + "-" + mult.long === item.location_lat + "-" + item.location_long) {
                            mult.count++
                            mult.events.push(item)
                            pushed = true
                        }
                    })
                    if (!pushed)
                        multipleEvents.push({
                            lat: item.location_lat,
                            long: item.location_long,
                            count: 1,
                            events: [item]
                        })
                }
            })

            //work many markers on map
            var map, infoWindow;
            var centerLatLng = new google.maps.LatLng(lat, lng);
            $scope.centerMarker = centerLatLng;
            var mapOptions = {
                center: centerLatLng,
                zoom: 16 - $rootScope.userInfo.notifyRadius
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);

            infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(map, "click", function () {
                infoWindow.close();
            });

            oneEventPoints.forEach(function (item, i, arr) {

                var latLng = new google.maps.LatLng(item.location_lat, item.location_long);
                var iconUrl = "img/map_icon.png";
                if (lng === item.location_long) {
                    iconUrl = "img/map_icon_current.png";
                }
                var name = item.title;
                var address = item.address;
                var id = item.id;
                var parentCategory = $filter('translate')(item.parentCategory);
                var category = $filter('translate')(item.category);
                var mediaThumb;
                //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                if (!item.mediaThumb) {
                    mediaThumb = "img/blog-single-4.jpg";
                } else if (item.mediaThumb.indexOf('http')) {
                    mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                } else {
                    mediaThumb = item.mediaThumb;
                }
                // Добавляем маркер с информационным окном
                addMarker(latLng, name, address, mediaThumb, id, iconUrl, parentCategory, category);
            });

            multipleEvents.forEach(function (item, i, arr) {

                var latLng = new google.maps.LatLng(item.events[0].location_lat, item.events[0].location_long);
                var iconUrl = "img/map_icon_many.png";
                if (lng === item.events[0].location_long) {
                    //iconUrl = "img/map_icon_current_many.png";
                    iconUrl = "img/map_icon_current.png";
                }
                addMarkers(item.events, latLng, iconUrl);
            });


            google.maps.event.addDomListener(window, "load");

            // Функция добавления маркера с информационным окном


            function addMarker(latLng, name, address, mediaThumb, id, iconUrl, parentCategory, category) {
                // var iconUrl = "img/map_icon.png";

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: name
                });

                // Отслеживаем клик по нашему маркеру
                google.maps.event.addListener(marker, "click", function () {

                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="infowindow">' +
                        '<h4> <a href="#/main/event/' + id + '?from=map">' + name + '</a></h4>' +
                        '<p>' + address + '</p>' +
                        '<p>' + parentCategory + ' / ' + category + '</p>' +
                        '<a href="#/main/event/' + id + '?from=map"><img src="' + mediaThumb + '">' + '</a></div>';
                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, marker);
                });
            }

            //
            // много маркеров на одном пине
            function addMarkers(array, latLng, iconUrl) {
                // var iconUrl = "img/map_icon_many.png";
                var address = array[0].address;
                var firstThumb = array[0].mediaThumb;
                var eventMarkerItems = array.length.toString() + " Events";

                var markers = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: eventMarkerItems
                });

                google.maps.event.addListener(markers, "click", function () {
                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="manyItemsWindow"><h4>' + eventMarkerItems + '</h4><p>' + address + '</p>';
                    contentString += '<div style="width: 80%;"><ul>';
                    array.forEach(function (item, i, arr) {
                        var mediaThumb;
                        //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                        if (!item.mediaThumb) {
                            mediaThumb = "img/blog-single-4.jpg";
                        } else if (item.mediaThumb.indexOf('http')) {
                            mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                        } else {
                            mediaThumb = item.mediaThumb;
                        }

                        contentString +=
                            '<li>' +
                            '<i class="fas fa-burn"></i> ' +
                            '<a href="#/main/event/' + item.id + '?from=map" title="' + item.title + '">' + item.title.substr(0, 55) + '</a>' +
                            '<div class="manyItWinHyde"><img src="' + mediaThumb + '"></div>' +
                            '</li>';
                    });
                    contentString += '</ul></div>';
                    contentString += '<div style="width: 20%;float: right;position: absolute;right: -10px; top: 20px; margin: 10px;"></div>';
                    contentString += '</div>';

                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, markers);
                });

            }

            $timeout(function () {
                $scope.mapLoading = false;
            }, 1000);


        }).error(function (xhr, req, err) {
            //console.log(err);
        });

    }

    $scope.initGetAroundEvents = function () {
        $timeout(function () {
            //console.log($rootScope.latForMap,$rootScope.longForMap );
            $scope.getAroundEvents($scope.flat, $scope.flong);
        }, 1000);
    }

    $scope.addLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.isLiked = function (id) {

        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isLiked',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            document.querySelector('.like_' + id + ' span.addLike').hidden = false;
            document.querySelector('.like_' + id + ' span.disLike').hidden = false;
            var liked = response.content;
            if (liked) {
                document.querySelector('.like_' + id + ' span.addLike').hidden = true;
            }

            if (!liked) {
                document.querySelector('.like_' + id + ' span.disLike').hidden = true;
            }

            document.querySelector('.like_' + id + ' span.likeCount').innerText = response.count;

        }).error(function (xhr, req, err) {
            //$.notify("error: "+req, "error");
        });
    }

    $scope.disLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/disLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.topMap = function () {
        var map, infoWindow;
        var lat = $rootScope.userPositionLat;
        var long = $rootScope.userPositionLong;

        var centerLatLng = new google.maps.LatLng(lat, long);

        var mapOptions = {
            center: centerLatLng,
            zoom: 14
        };

        map = new google.maps.Map(document.getElementById("top-map"), mapOptions);

        var iconUrl = "img/map_icon.png";

        var marker = new google.maps.Marker({
            position: centerLatLng,
            map: map,
            icon: iconUrl,
            title: name
        });


        if ($rootScope.userPositionLat == undefined && $rootScope.userPositionLong == undefined) {
            $.notify("You are not allowed to use your location.", "error");
            $('#top-map-hide').show();
            $('#top-map').hide();
            console.log('userPositionLat', lat, long);
        }
    }

    $('.sidebarBtn').click(function(){
        $('.sidebar-mobile-toggle').toggleClass('active');
        $('.sidebarBtn').toggleClass('toggle');
    });

    $('.sidebarBtnSearch').click(function(){
        $('.mob-search-location-toggle').toggleClass('active');
        $('.sidebarBtnSearch').toggleClass('toggle');
    });


    /*$('body,html').animate({
        scrollTop: 0
    }, 600);*/
}]);

LOMCityControllers.controller('myFavEventsCtrl', ["$rootScope", "$scope", "$state", '$http', 'dhm', 'getDist', "$filter", "$sce", "$cookies", '$stateParams', 'isValidToken', 'dhmFilter', 'getLocationDistance', '$timeout', '$interval', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams, isValidToken, dhmFilter, getLocationDistance, $timeout, $interval) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        var isCatOptReady = $interval(function () {
            if ($rootScope.optionsReady) {
                if ($state.current.name == "main.myFavEvents") {
                    $scope.selectedPOption = $rootScope.selectedPOption;
                    $scope.changePCat();
                }
                $scope.getPreferredLocations();
                $interval.cancel(isCatOptReady);
            }
        }, 10);
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = false;
    $scope.events = [];
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.SearchVal = false;
    $scope.FilterVal = false;
    $scope.locationSelected = false;
    $scope.loading = false;
    $scope.favLocLength = 0;
    $scope.isLocationEnabled = false;
    $scope.searchByLocation = false;
    $scope.selectedOption = "";
    $scope.selectedPOption = "";
    $scope.newChildCategories = [];

    $scope.showEventsFav = localStorage.getItem('showEventsFav');

    if ($scope.showEventsFav == 'list') {
        $scope.showListEventsFav = true;
        $scope.showMapEventsFav = false;
    } else if ($scope.showEventsFav == 'map') {
        $scope.showListEventsFav = false;
        $scope.showMapEventsFav = true;
    } else {
        $scope.showListEventsFav = true;
        $scope.showMapEventsFav = false;
    }


    $scope.catagorySelected = function (index) {
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $rootScope.chooseOptions[index].name;
            var pCat = $rootScope.chooseOptions[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;
        } else {
            $scope.selectedOption = "";
        }
        $scope.search();
    }
    $scope.catagoryFilteredSelected = function (index) {
        //alert("catagoryFilteredSelected "+index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $scope.newChildCategories[index].name;
            var pCat = $scope.newChildCategories[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;
        } else {
            $scope.selectedOption = "";
        }
        $scope.search();
    }
    $scope.catagoryPSelected = function (index) {
        //console.log("catagoryPSelected " + index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $(".catPBtn").removeClass('button-dark');
        $("#btnp" + index).addClass('button-dark');
        $("#btnall").addClass('button-dark');
        if (index != "all") {
            var cat = $rootScope.parentCategories[index].name;
            var catVal = $filter('translate')(cat);
            $scope.selectedPOption = cat;
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
            for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                if (cat == $rootScope.chooseOptions[j].parentCategory) {
                    $scope.newChildCategories.push($rootScope.chooseOptions[j])
                } else {
                }
            }
        } else {
            $scope.selectedPOption = "";
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
        }
        $scope.search();
    }
    $scope.catagoryChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catBtn").removeClass('button-active');
            $("#btn" + index).addClass('button-active');
            if (index != "all") {
                var cat = $rootScope.chooseOptions[index].name;
                var catVal = $filter('translate')(cat);
                $scope.selectedOption = cat;
            } else {
                $scope.selectedOption = "";
            }
        }, 1000);
        //$scope.search();
    }
    $scope.changeCat = function () {
        //alert($rootScope.chooseOptions);
        if ($scope.newChildCategories.length == 0) {
            for (var i = $rootScope.chooseOptions.length - 1; i >= 0; i--) {
                if ($scope.selectedOption == $rootScope.chooseOptions[i].name) {
                    $scope.catagoryChangeSelected(i)
                }
            }
        } else {
            for (var l = $scope.newChildCategories.length - 1; l >= 0; l--) {
                if ($scope.selectedOption == $scope.newChildCategories[l].name) {
                    //alert("l"+l)
                    $scope.catagoryChangeSelected(l)
                }
            }
        }
    }
    $scope.catagoryPChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catPBtn").removeClass('button-dark');
            $("#btnp" + index).addClass('button-dark');
            if (index != "all") {
                var cat = $rootScope.parentCategories[index].name;
                //var catVal = $filter('translate')(cat);
                $scope.selectedPOption = cat;
            } else {
                $scope.selectedPOption = "";
            }
        }, 1000);
    }
    $scope.changePCat = function () {
        //return;
        for (var i = $rootScope.parentCategories.length - 1; i >= 0; i--) {
            //console.log($scope.selectedPOption + "    :     " + $rootScope.chooseOptions[i].name)
            if ($scope.selectedPOption == $rootScope.parentCategories[i].name) {

                //$scope.catagoryPSelected(i);
                //$scope.catagoryPChangeSelected(i)
                $scope.results_from = 0;
                $(".catBtn").removeClass('button-active');
                $(".catPBtn").removeClass('button-dark');
                $("#btnp" + i).addClass('button-dark');
                $("#btnall").addClass('button-dark');
                if (i != "all") {
                    var cat = $rootScope.parentCategories[i].name;
                    var catVal = $filter('translate')(cat);
                    $scope.selectedPOption = cat;
                    //$scope.selectedOption = "";
                    $scope.newChildCategories = [];
                    for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                        if (cat == $rootScope.chooseOptions[j].parentCategory) {
                            $scope.newChildCategories.push($rootScope.chooseOptions[j])
                        } else {
                        }
                    }
                } else {
                    $scope.selectedPOption = "";
                    $scope.selectedOption = "";
                    $scope.newChildCategories = [];
                }
                $scope.catagoryPChangeSelected(i);
            }
        }
    }

    $scope.searchKeyPress = function (e) {
        e = e || window.event;
        if (e.keyCode == 13) {
            if ($scope.locationSelected) {
                $scope.search();
            }
        }
    }

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.switchViewFav = function (event) {
        if (event.target.classList.contains('active')) {
        } else {
            $scope.showListEventsFav = !$scope.showListEventsFav;
            $scope.showMapEventsFav = !$scope.showMapEventsFav;
            if ($scope.showListEventsFav) {
                localStorage.setItem('showEventsFav', 'list');
            } else {
                localStorage.setItem('showEventsFav', 'map');
            }
            $scope.getContent($scope.SearchVal, $scope.FilterVal);
        }
    }

    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height()) == ($(document).height())) {
            if (($state.includes('main.myFavEvents') || $state.includes('main.myFavEventsFiltered'))) {
                if ($scope.load_events) {
                    //console.log($scope.load_events);
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        if (!$scope.loading && $scope.load_events) {
                            $scope.loading = true;
                            $scope.load_events = false;
                            $scope.getContent($scope.SearchVal, $scope.FilterVal);
                            //$scope.search();
                        }
                        //$scope.getContent(false,false);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                }
            }
        }
    });

    $scope.searchModel = {};
    $scope.checkLocFromStorage = false;

    //console.log($stateParams.param2+"  "+$stateParams.param1);
    $scope.searchModel.FilterVal = "Location";
    $scope.isLocation = true;
    //console.log($stateParams);
    if ($stateParams.param2 == "Location" && $stateParams.param1 != "") {
        var SearchVal = localStorage.getItem('SearchVal');
        $scope.SearchVal = JSON.parse(SearchVal);
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchLocationVal = $stateParams.param1;
        $scope.selectedOption = $stateParams.param3;
        $scope.selectedPOption = $stateParams.param4;
        $scope.checkLocFromStorage = true;
        $scope.isLocation = true;
        if ($rootScope.optionsReady) {
            //console.log("if called");
            $scope.changePCat();
            $scope.changeCat();
        } else {
            //console.log("else called");
            var isCatReady = $interval(function () {
                if ($rootScope.optionsReady) {
                    $scope.changePCat();
                    $scope.changeCat();
                    $interval.cancel(isCatReady);
                }
            }, 10);
        }
    } else if ($stateParams.param2 == "Title" || $stateParams.param2 == "User") {
        $scope.SearchVal = $stateParams.param1;
        $scope.FilterVal = $stateParams.param2;
        $scope.searchModel.FilterVal = $stateParams.param2;
        $scope.searchModel.SearchVal = $stateParams.param1;
        $scope.searchModel.SearchLocationVal = "";
        $scope.selectedOption = $stateParams.param3;
        $scope.selectedPOption = $stateParams.param4;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = false;
        if ($rootScope.optionsReady) {
            $scope.changePCat();
            $scope.changeCat();
        } else {
            var isCatReady = $interval(function () {
                if ($rootScope.optionsReady) {
                    $scope.changePCat();
                    $scope.changeCat();
                    $interval.cancel(isCatReady);
                }
            }, 10);
        }
    } else if ($stateParams.param2 == undefined && $stateParams.param1 != undefined) {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = true;
        $scope.searchModel.FilterVal = "Location";
    } else {
        $scope.SearchVal = false;
        $scope.FilterVal = false;
        $scope.checkLocFromStorage = false;
        $scope.isLocation = true;
        $scope.searchModel.FilterVal = "Location";
    }

    $scope.search = function () {
        $scope.results_from = 0;
        $scope.load_events = false;
        var paramObj = {param1: "", param2: "", param3: "", param4: ""};
        if ($scope.searchModel.FilterVal == "Location" && $scope.searchModel.SearchLocationVal != undefined && $scope.searchModel.SearchLocationVal != '') {
            promise = isValidToken.check();
            promise.then(function (token) {

                var locations = {
                    lat: "",
                    long: "",
                    name: ""
                }
                //alert($scope.searchModel.SearchLocationVal);
                if (!$scope.checkLocFromStorage) {
                    locations.lat = $scope.searchModel.SearchLocationVal.geometry.location.lat();
                    locations.long = $scope.searchModel.SearchLocationVal.geometry.location.lng();
                    locations.name = $scope.searchModel.SearchLocationVal.formatted_address;
                } else {
                    var SearchVal = localStorage.getItem('SearchVal');
                    locations.lat = JSON.parse(SearchVal).lat;
                    locations.long = JSON.parse(SearchVal).long;
                    locations.name = JSON.parse(SearchVal).name;
                }
                paramObj = {
                    param1: locations.name,
                    param2: $scope.searchModel.FilterVal,
                    param3: $scope.selectedOption,
                    param4: $scope.selectedPOption
                }
                localStorage.setItem('SearchVal', JSON.stringify(locations));
                $state.go('main.myFavEventsFiltered', paramObj, {reload: true});
                $scope.searchByLocation = true;
                $scope.locationSelected = false;

                /*//$scope.searchModel.SearchVal = "";
                $scope.events = [];
                $scope.getContent(locations,$scope.searchModel.FilterVal);
                $scope.locationSelected = false;*/
            }, function (response) {
                $state.go('login', {});
            });
        } else if ($scope.searchModel.FilterVal == "Title" || $scope.searchModel.FilterVal == "User") {
            paramObj = {
                param1: $scope.searchModel.SearchVal,
                param2: $scope.searchModel.FilterVal,
                param3: $scope.selectedOption,
                param4: $scope.selectedPOption
            }
            promise = isValidToken.check();
            promise.then(function (token) {
                localStorage.setItem('SearchVal', $scope.searchModel.SearchVal);
                $state.go('main.myFavEventsFiltered', paramObj, {reload: true});
                $scope.searchByLocation = false;

                /*
                $scope.events = [];
                $scope.searchByLocation = false;
                $scope.getContent($scope.searchModel.SearchVal,$scope.searchModel.FilterVal);*/
            }, function (response) {
                $state.go('login', {});
            });
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.events = [];
                $scope.searchByLocation = false;
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('login', {});
            });
        }
    }

    $scope.getGeolocation = function () {
        $scope.loading = true;
        if ($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined) {
            //alert('if');
            $scope.isLocationEnabled = true;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.searchByLocation = false;
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });

        } else if (navigator.geolocation) {
            //alert('navigator');
            var geo_options = {
                timeout: 27000
            };
            navigator.geolocation.getCurrentPosition(myLoc, locationError);
        } else {
            //alert('else');
            $scope.isLocationEnabled = false;
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                //$scope.getContent($scope.SearchVal, $scope.FilterVal);
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $scope.isLocationEnabled = true;
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        //alert($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent(false, false);
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    function locationError(error) {
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        $scope.getContent(false, false);
        $scope.isLocationEnabled = false;
    }

    //console.log($scope.events.length+' '+$scope.results_from+' '+$scope.results_num);
    $scope.getContent = function (SearchVal, FilterVal) {
        $scope.loading = true;
        /*SearchVal = (SearchVal == undefined)?"":SearchVal;
        //(SearchVal = undefined)?"":SearchVal;
        //alert(SearchVal+"  "+FilterVal);
        $scope.SearchVal = SearchVal;
        $scope.FilterVal = FilterVal;*/
        //$scope.events = [];
        //console.log(SearchVal,FilterVal);
        //console.log($scope.selectedOption,$scope.selectedPOption);
        $scope.locSearchVal = $scope.SearchVal;
        geocoder = new google.maps.Geocoder();
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            lat: $rootScope.myLat,
            long: $rootScope.myLong,
            SearchVal: ($scope.SearchVal == undefined) ? "" : $scope.SearchVal,
            FilterVal: ($scope.FilterVal == undefined) ? "" : $scope.FilterVal,
            _token: localStorage.satellizer_token,
            category: ($stateParams.param3 != undefined) ? $stateParams.param3 : $scope.selectedOption,
            parentCategory: ($stateParams.param4 != undefined) ? $stateParams.param4 : $scope.selectedPOption,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        if ($rootScope.myLat == 0 || $rootScope.myLong == 0) {
            $scope.isLocationEnabled = false;
        }
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getFavEvents',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            $scope.loading = false;
            //console.log($scope.events);
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if (events.length < 20) {
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder = new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {
                        var time = dhmFilter(response.content[i]['created_at']);

                        //events[i]['rtime'] = time;
                        //$scope.isNow = ((time.indexOf("Yesterday") > -1) || (time.indexOf("days") > -1) ||(time.indexOf("-") > -1))?false:true;
                        //events[i]['isNow'] = $scope.isNow;


                        events[i]['rtime'] = time[0];
                        $scope.isNow = time[1];
                        $scope.showtime = time[2];

                        events[i]['isNow'] = $scope.isNow;
                        events[i]['showtime'] = $scope.showtime;


                        var timeArr = String(events[i]['created_at']).split(" ");
                        timeArr = String(timeArr[1]).split(":");
                        events[i]['timeString'] = timeArr[0] + ":" + timeArr[1];

                        if ($scope.locSearchVal) {
                            //console.log(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked,$scope.locSearchVal.lat,$scope.locSearchVal.long);
                            events[i]['distance'] = getLocationDistance(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked, null, $scope.locSearchVal.lat, $scope.locSearchVal.long);
                        } else {
                            events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        }
                        //events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }
                        $scope.events.push(events[i]);
                        $scope.getMapEvents($scope.events);


                    }
                    ;
                    $scope.noEvents = false;
                    $scope.loading = false;
                    //console.log($scope.events.length);
                } else {
                    //console.log($scope.events.length);
                    if ($scope.events.length == 0) {
                        $scope.load_events = false;
                        $scope.events = events;
                        $scope.noEvents = true;
                    }
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            $scope.loading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getMapEvents = function (events) {
        $scope.Markers = events;

        $scope.MapOptions = {
            center: new google.maps.LatLng($rootScope.myLat, $rootScope.myLong),
            /*zoom: 15 / $rootScope.userInfo.notifyRadius,*/
            zoom: 15 - $rootScope.userInfo.notifyRadius,
            //zoom: 27 - $rootScope.userInfo.notifyRadius,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //Initializing the InfoWindow, Map and LatLngBounds objects.
        $scope.InfoWindow = new google.maps.InfoWindow();
        $scope.Latlngbounds = new google.maps.LatLngBounds();
        $scope.Map = new google.maps.Map(document.getElementById("favEventsMap"), $scope.MapOptions);

        //Looping through the Array and adding Markers.
        for (var i = 0; i < $scope.Markers.length; i++) {
            var data = $scope.Markers[i];
            var myLatlng = new google.maps.LatLng(data.location_lat, data.location_long);

            //Initializing the Marker object.
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: $scope.Map,
                title: data.title
            });

            //Adding InfoWindow to the Marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    $scope.InfoWindow.setContent('<div style = "max-width:300px;min-height:50px"><a href="/#/main/event/' + data.id + '"><b>' + data.title + '</b></a><p>' + data.description + '</p></div>');
                    $scope.InfoWindow.open($scope.Map, marker);
                });
            })(marker, data);

            //Plotting the Marker on the Map.
            $scope.Latlngbounds.extend(marker.position);
        }

        //Adjusting the Map for best display.
        $scope.Map.setCenter($scope.Latlngbounds.getCenter());
        $scope.Map.fitBounds($scope.Latlngbounds);
    }
    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getSettings ',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;

            $scope.favLocLength = 0;
            for (var i = $scope.preferredLocations.length - 1; i >= 0; i--) {
                if ($scope.preferredLocations[i].name != "") {
                    $scope.favLocLength++;
                }
            }

            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            //console.log(JSON.stringify(req));
        });
    }
//
    $scope.changeLocationState = function () {
        if ($scope.favLocLength > 0) {
            $state.go('main.myFavEvents', {});
        } else {
            $state.go('main.settings', {});
        }
    }
    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
            $scope.locationSelected = false;
        } else {
            $scope.locationSelected = true;
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }
    $scope.$on('g-places-autocomplete:select', function (event, param) {
        if ($scope.isLocation) {
            $scope.locationSelected = true;
            $scope.checkLocFromStorage = false;
        }
        ;
    });

}]);

LOMCityControllers.controller('myEventsCtrl', ["$rootScope", "$scope", "$state", '$http', 'dhm', 'getDist', "$filter", "$sce", "$cookies", '$stateParams', 'isValidToken', 'dhmFilter', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams, isValidToken, dhmFilter) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getContent(false, false);
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = false;
    $scope.events = [];
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.loading = false;
    $scope.user_id = $stateParams.user_id;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            if ($scope.load_events == true && ($state.includes('main.allEvents') || $state.includes('main.allEventsFiltered') || $state.includes('main.myEvents'))) {
                //console.log('bottom');
                promise = isValidToken.check();
                promise.then(function (token) {
                    //$scope.getContent($scope.SearchVal, $scope.FilterVal);
                    if (!$scope.loading) {
                        $scope.loading = true;
                        $scope.getContent(false, false);
                    }
                }, function (response) {
                    $state.go('main.login', {});
                });
            }
        }
    });

    $scope.searchModel = {};

    $scope.getGeolocation = function () {
        //alert('here');
        if ($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined) {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });
        } else if (navigator.geolocation) {
            var geo_options = {
                enableHighAccuracy: true,
                maximumAge: 30000,
                timeout: 27000
            };
            navigator.geolocation.getCurrentPosition(myLoc, onErrorLoc, geo_options);
        } else {
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        //console.log($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent(false, false);
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    function onErrorLoc(error) {
        promise.then(function (token) {
            $scope.getContent(false, false);
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    //console.log($scope.events.length+' '+$scope.results_from+' '+$scope.results_num);
    $scope.getContent = function (SearchVal, FilterVal) {
        $scope.loading = true;
        //$scope.events = [];
        geocoder = new google.maps.Geocoder();
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            lat: $rootScope.myLat,
            long: $rootScope.myLong,
            SearchVal: SearchVal,
            FilterVal: "myEvents",
            _token: localStorage.satellizer_token,
            user_id: $scope.user_id,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvents',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $scope.loading = false;
            $rootScope.hideLoading();
            //console.log(response);
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if (events.length < 20) {
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }
                    var geocoder = new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {
                        var time = dhmFilter(response.content[i]['created_at']);
                        events[i]['rtime'] = time[0];

                        events[i]['rtime'] = time[0];

                        //$scope.isNow = ((time.indexOf("Yesterday") > -1) || (time.indexOf("days") > -1) ||(time.indexOf("-") > -1))?false:true;
                        $scope.isNow = time[1];
                        $scope.showtime = time[2];
                        events[i]['isNow'] = $scope.isNow;
                        events[i]['showtime'] = $scope.showtime;

                        var timeArr = String(events[i]['created_at']).split(" ");
                        timeArr = String(timeArr[1]).split(":");
                        events[i]['timeString'] = timeArr[0] + ":" + timeArr[1];


                        events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }
                        $scope.events.push(events[i]);
                    }
                    ;
                    $scope.noEvents = false;
                    $scope.loading = false;
                    //console.log($scope.events.length);
                } else {
                    if ($scope.events.length == 0) {
                        $scope.load_events = false;
                        $scope.events = events;
                        $scope.noEvents = true;
                    }
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            $scope.loading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getSettings ',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;
            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            //console.log(JSON.stringify(req));
        });
    }

    $scope.isLocation = true;
    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
        } else {
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

}]);

LOMCityControllers.controller('eventCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$stateParams', 'dhm', 'getDist', "$filter", "$sce", "isValidToken", 'dhmFilter', '$location', '$anchorScroll', function ($rootScope, $scope, $timeout, $state, $http, $stateParams, dhm, getDist, $filter, $sce, isValidToken, dhmFilter, $location, $anchorScroll) {

    //$scope.deviceDetector=deviceDetector;
    if (localStorage.satellizer_token) {
        var promise = isValidToken.check();
        promise.then(function (token) {
            $scope.openEvent();
        }, function (response) {
            //$state.go('main.login', {});
            $rootScope.loggedIn = false;
            $scope.openEvent();
        });
    }

    $scope.getEvent = false;
    $scope.noComments = false;
    $scope.errorMsg = false;
    $scope.stay_annonymous = false;
    $scope.comment = {};
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.eIsNow = false;

    $scope.galleryFiles = [];
    $scope.isLoggedIn = $rootScope.loggedIn;
    $scope.shareLink = "https://myedge.lotemx.com/#/main/event/" + $stateParams.event_id;
    // $rootScope.shareLink = "https://viround.com/public/event/" + $stateParams.event_id;

    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.backToPost = fromParams.event_id;
        //alert(JSON.stringify(fromParams));

        var loc = location.href;
        if (loc.indexOf('map')) {
            $rootScope.backToMap = 1;
            console.log($rootScope.backToMap);
        }
    });

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    if (!localStorage.satellizer_token) {
        localStorage.satellizer_token = 1;
        var request = {
            id: $stateParams.event_id,
            publicEvent: true
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(response);

            if (response.status == 1) {
                $scope.guestBlocked = response.guestBlocked;
                var even = response.content;
                if (even.gallery_files != null) {
                    even.gallery = JSON.parse(even.gallery_files);
                    angular.forEach(even.gallery, function (el) {
                        el.filename = $rootScope.mediaUrlS3 + el.filename;
                        el.thumb = $rootScope.mediaUrlS3 + el.thumb;
                        //console.log(el.thumb);
                    });
                } else {
                    even.gallery = [];
                }
                console.log(even);
                if (even['avatar'] == '') {
                    even['avatar'] = 'img/default-avatar.png';
                }
                var time = dhmFilter(even['created_at']);
                if (time[0] == '') {
                    time[0] = 'Just now';
                }

                if (response.content.gallery_files) {
                    $scope.mediaTypeIs = JSON.parse(response.content.gallery_files)[0].type;
                } else {
                    $scope.mediaTypeIs = response.content.mediaType;
                }


                var timeArr = String(even['created_at']).split(" ");
                timeArr = String(timeArr[1]).split(":");

                $rootScope.mediaThumb = even['mediaThumb'];
                $rootScope.description = even['description'];
                $scope.galleryFiles = even['gallery_files'];

                //console.log(even['gallery_files']);
                $scope.isNow = time[1];
                even['rtime'] = time[0];
                $scope.showtime = time[2];

                even['timeString'] = timeArr[0] + ":" + timeArr[1];

                var tempDate = even['when'];

                var t = tempDate.split(/[- :]/);
                // Apply each element to the Date function
                var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                var eventDate = d.toString();
                var eventDateString = eventDate.split(" ");

                var _date = $filter('date')(d.getTime(), $filter('translate')('dateFormat'));
                var _mn = (d.getMonth());
                var _month = $rootScope.monthArray.split(",");
                var pattern = new RegExp(_month[_mn].trim(), "ig");

                var _lanM = $filter('translate')('monthArray').split(",");
                var _dateF = _date.replace(pattern, _lanM[_mn]);
                even['eTime'] = _dateF;

                function timeMorph(tDate) {
                    var et = tDate.split(/[- :]/);
                    // Apply each element to the Date function
                    var ed = new Date(et[0], et[1] - 1, et[2], et[3], et[4], et[5]);
                    var eDate = ed.toString();
                    var eDateString = eDate.split(" ");

                    var _edate = $filter('date')(ed.getTime(), $filter('translate')('dateFormat'));
                    var _emn = (ed.getMonth());
                    var _emonth = $rootScope.monthArray.split(",");
                    var epattern = new RegExp(_emonth[_emn].trim(), "ig");

                    var _elanM = $filter('translate')('monthArray').split(",");
                    var _edateF = _edate.replace(epattern, _elanM[_emn]);
                    return _edateF;
                }

                if (even['end_event_time'] != null) {
                    var endDate = even['end_event_time'];
                    $scope.endDate = timeMorph(endDate);
                }

                if (even['end_discount_time'] != null) {
                    var endDiscountDate = even['end_discount_time'];
                    // $scope.nowTime = Date.parse(endDiscountDate)+" "+ Date.now();
                    // $scope.endDiscountDate = timeMorph(endDiscountDate);
                    if (Date.parse(endDiscountDate) > Date.now()) {
                        $scope.endDiscountDate = timeMorph(endDiscountDate);
                    }
                }

                var eventTimeString = eventDateString[4].split(":");
                even['eTimeString'] = eventTimeString[0] + ":" + eventTimeString[1] + ":" + eventTimeString[2];


                even['distance'] = getDist(even['location_lat'], even['location_long'], $rootScope.km_checked);
                if (even.contentUrl == '') {
                    even.content = false;
                } else {
                    even.content = true;
                }
                $scope.op_event = even;
                $rootScope.pageTitle = even['title'];

                if (even['mediaThumb']) {
                    $rootScope.pageImg = even['mediaThumb'];
                } else {
                    $rootScope.pageImg = even.gallery.thumb;
                }

                //console.log("!!!!!!!!!!!!", even['mediaUrl']) ;

                var mediaName = even['mediaThumb'].split('/').pop();
                even['mediaThumb'] = $rootScope.mediaUrlS3 + mediaName;

                var mediaUrlName = even['mediaUrl'].split('/').pop();
                even['mediaUrl'] = $rootScope.mediaUrlS3 + mediaUrlName;

                $scope.getEvent = true;
                var tokenFalse = true;
                $scope.getAroundEvents(even['location_lat'], even['location_long'], even['parentCategory'], even['category'], tokenFalse);
            } else {
                if (response.status == 0 && response.content == "") {
                    window.history.back();
                    return;
                }
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }

        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                //$rootScope.msgDescription = $filter('translate')('Error_msg10');
                $rootScope.msgDescription = "alarm";
                $("#alertModal").modal('show');
            }
        });

        localStorage.satellizer_token = "";

    }

    /*$timeout(function () {
        $scope.imageZoom = 1;
        $scope.$watch("imageZoom",
             function changeImageZoom( newValue, oldValue ) {
             	 var containerWidth = $("#event-img").width();
             	 var imageWidth = containerWidth * (1+ newValue/100);
                 $("#eventImage").css('max-width', imageWidth);
                 $("#eventImage").css('width', imageWidth);
            }
        );
    }, 500);*/
    /*document.getElementById("event-img").style.display = 'none';
    document.getElementById("rangeSlider").style.display = 'none';
    document.getElementById("media-con").style.display = 'none';*/

    $scope.getComments = function (event_id) {
        $scope.comments = {};
        var request = {
            event_id: $stateParams.event_id,
            _token: localStorage.satellizer_token
        }
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getComments',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(response.content);
            if (response.status == 1) {
                comments = response.content;
                if (comments.length == 0) {
                    $scope.noComments = true;
                } else {
                    for (var i = 0; i < comments.length; i++) {
                        if (comments[i]['avatar'] == '') {
                            comments[i]['avatar'] = 'img/default-avatar.png';
                        }
                        comments[i]['comment'] = comments[i]['comment'].replace(/\n/g, '<br>');
                        var r_time = dhmFilter(comments[i]['created_at']);
                        comments[i]['rtime'] = r_time[0];
                        //comments[i]['rtime'] = dhm('2015-12-09 06:20:26');
                    }

                    $scope.comments = comments;
                    console.log($scope.comments);
                    $scope.noComments = false;
                }
            } else {
                //console.log($filter('translate')('Error_msg7'));
            }
            $scope.followedEventUser();
            $scope.scrollTo();
        }).error(function (xhr, req, err) {
            //console.log($filter('translate')('Error_msg10'));
        });
    };

    $scope.openEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };
        $rootScope.showLoading();
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(response);
            $rootScope.hideLoading();
            if (window.scrollY) {
                //alert('ss');
                window.scroll(0, 0);  // reset the scroll position to the top left of the document.
            }
            if (response.status == 1) {
                $scope.guestBlocked = response.guestBlocked;
                var even = response.content;
                if (even.gallery_files != null) {
                    even.gallery = JSON.parse(even.gallery_files);
                    angular.forEach(even.gallery, function (el) {
                        el.filename = $rootScope.mediaUrlS3 + el.filename;
                        el.thumb = $rootScope.mediaUrlS3 + el.thumb;
                    });
                } else {
                    even.gallery = [];
                }
                //console.log(even);
                if (even['avatar'] == '') {
                    even['avatar'] = 'img/default-avatar.png';
                }

                var charsHalfLen = Math.ceil(even['cleanDescription'].length / 2);
                var fChar = checkRTL(even['cleanDescription'].substring(0,1));
                var midChar = checkRTL(even['cleanDescription'].substring(charsHalfLen,charsHalfLen+1));
                var lChar = checkRTL(even['cleanDescription'].slice(-2));

                even['haveAr'] = null;

                if(fChar || midChar || lChar){
                    even['haveAr'] = true;
                }

                /*else{
                  var avatarName = even['avatar'].split('/').pop();
                  even['avatar'] = $rootScope.mediaUrlS3+'avatar/'+avatarName;
                }*/
                var time = dhmFilter(even['created_at']);
                if (time[0] == '') {
                    time[0] = 'Just now';
                }

                if (response.content.gallery_files) {
                    $scope.mediaTypeIs = JSON.parse(response.content.gallery_files)[0].type;
                } else {
                    $scope.mediaTypeIs = response.content.mediaType;
                }

                var timeArr = String(even['created_at']).split(" ");
                timeArr = String(timeArr[1]).split(":");

                $rootScope.mediaThumb = even['mediaThumb'];
                $rootScope.description = even['description'];
                $scope.galleryFiles = even['gallery_files'];

                //console.log(even['gallery_files']);
                $scope.isNow = time[1];
                even['rtime'] = time[0];
                $scope.showtime = time[2];

                even['timeString'] = timeArr[0] + ":" + timeArr[1];

                var tempDate = even['when'];

                var t = tempDate.split(/[- :]/);
                // Apply each element to the Date function
                var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                var eventDate = d.toString();
                var eventDateString = eventDate.split(" ");

                var _date = $filter('date')(d.getTime(), $filter('translate')('dateFormat'));
                var _mn = (d.getMonth());
                var _month = $rootScope.monthArray.split(",");
                var pattern = new RegExp(_month[_mn].trim(), "ig");

                var _lanM = $filter('translate')('monthArray').split(",");
                var _dateF = _date.replace(pattern, _lanM[_mn]);
                even['eTime'] = _dateF;

                function timeMorph(tDate) {
                    var et = tDate.split(/[- :]/);
                    // Apply each element to the Date function
                    var ed = new Date(et[0], et[1] - 1, et[2], et[3], et[4], et[5]);
                    var eDate = ed.toString();
                    var eDateString = eDate.split(" ");

                    var _edate = $filter('date')(ed.getTime(), $filter('translate')('dateFormat'));
                    var _emn = (ed.getMonth());
                    var _emonth = $rootScope.monthArray.split(",");
                    var epattern = new RegExp(_emonth[_emn].trim(), "ig");

                    var _elanM = $filter('translate')('monthArray').split(",");
                    var _edateF = _edate.replace(epattern, _elanM[_emn]);
                    return _edateF;
                }

                if (even['end_event_time'] != null) {
                    var endDate = even['end_event_time'];
                    $scope.endDate = timeMorph(endDate);
                }

                if (even['end_discount_time'] != null) {
                    var endDiscountDate = even['end_discount_time'];
                    // $scope.nowTime = Date.parse(endDiscountDate)+" "+ Date.now();
                    // $scope.endDiscountDate = timeMorph(endDiscountDate);
                    if (Date.parse(endDiscountDate) > Date.now()) {
                        $scope.endDiscountDate = timeMorph(endDiscountDate);
                    }
                }

                var eventTimeString = eventDateString[4].split(":");
                even['eTimeString'] = eventTimeString[0] + ":" + eventTimeString[1] + ":" + eventTimeString[2];


                even['distance'] = getDist(even['location_lat'], even['location_long'], $rootScope.km_checked);
                if (even.contentUrl == '') {
                    even.content = false;
                } else {
                    even.content = true;
                }

                var mediaName = even['mediaThumb'].split('/').pop();
                even['mediaThumb'] = $rootScope.mediaUrlS3 + mediaName;

                var mediaUrlName = even['mediaUrl'].split('/').pop();
                even['mediaUrl'] = $rootScope.mediaUrlS3 + mediaUrlName;

                $scope.op_event = even;

                $scope.thisPageShareUrl = $scope.op_event.id + "_" + $scope.op_event.title.replace(/ /g, '~') + "_" + $scope.op_event.mediaThumb.split('/').pop().split('_').shift();
                //console.log($scope.thisPageShareUrl);

                $scope.socialIcons($scope.thisPageShareUrl);

                $rootScope.pageTitle = even['title'];


                if (even['mediaThumb']) {
                    $rootScope.pageImg = even['mediaThumb'];
                } else {
                    $rootScope.pageImg = even.gallery.thumb;
                }

                //console.log("!!!!!!!!!!!!", $rootScope.pageImg) ;

                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getComments(even['id']);
                }, function (response) {
                    //$state.go('main.login', {});
                });
                $scope.getEvent = true;
                $scope.getAroundEvents(even['location_lat'], even['location_long'], even['parentCategory'], even['category']);
                $scope.followedEventUser();

            } else {
                if (response.status == 0 && response.content == "") {
                    window.history.back();
                    return;
                }
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                //$("#alertModal").modal('show');
                console.error($rootScope.msgDescription);
            }
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                //$("#alertModal").modal('show');
                console.error($rootScope.msgDescription);
            }
        });
    };

    $scope.addComment = function (event_id, comment) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var commentTxt = comment.text;
            commentTxt = commentTxt.replace(/(\n)+/g, "<br>");
            var request = {
                event_id: $stateParams.event_id,
                comment: commentTxt,
                to_user_id: $scope.op_event.user_id,
                _token: localStorage.satellizer_token
            }
            console.log(JSON.stringify(request));
            //alert(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/addComment',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                console.log(JSON.stringify(response.dd));
                if (response.status == 1) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        $scope.getComments(event_id);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                    $scope.comment = {};
                } else {
                    $rootScope.msgDescription = $filter('translate')('Error_msg7');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.deleteEventMethod = function () {
        //alert('called')
        $('#deleteEvent').modal('hide');
        $rootScope.showLoading();
        //$scope.canceDeletelModel();
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                event_id: $stateParams.event_id,
                _token: localStorage.satellizer_token
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/deleteEvent',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //$state.go('main.allEvents', {});
                $location.path("/allEvents");
                console.log('response', JSON.stringify(response));
                $rootScope.hideLoading();
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.canceDeletelModel = function () {
        $scope.errorMsg = false;
    }

    $scope.deleteComment = function (event, id) {
        $scope.deletedComment = event.target.parentNode;
        //$stateParams.comment_id = event.target.parentNode.getAttribute('data-id');
        $scope.del_comment_id = id;
        console.log(id);
    }

    $scope.deleteCommentMethod = function (event) {
        var comment = $scope.deletedComment;
        var parent = comment.parentNode;
        console.log(event.target.value);
        if (event.target.value == 'Yes') {
            $('#deleteComment').modal('hide');
            $rootScope.showLoading();
            $scope.canceDeletelModel();
            promise = isValidToken.check();
            promise.then(function (token) {
                var request = {
                    //comment_id: $stateParams.comment_id,
                    comment_id: $scope.del_comment_id,
                    _token: localStorage.satellizer_token
                };
                console.log(JSON.stringify(request));
                $http({
                    method: 'POST',
                    url: $rootScope.baseUrl + '/api/deleteComment',
                    data: {data: JSON.stringify(request)}
                }).success(function (response) {
                    //console.log(JSON.stringify(response));
                    $scope.deletedComment.parentNode.removeChild($scope.deletedComment);
                    $scope.deletedComment = false;
                    if (response.status = 1) {
                        $rootScope.msgDescription = $filter('translate')('Comment_deleted');
                        $.notify($rootScope.msgDescription, "success");
                        //$("#alertModal").modal('show');
                        $scope.getComments($stateParams.event_id);
                    }
                }).error(function (xhr, req, err) {
                    if (req == 500) {
                        $rootScope.msgDescription = $filter('translate')('Error_msg10');
                        $("#alertModal").modal('show');
                    }
                });
            }, function (response) {
                $state.go('main.login', {});
            });


        }
    }

    $scope.editComment = function (id) {
        var newComment = $('#' + id + '_edit').val();
        var request = {
            comment_id: id,
            _token: localStorage.satellizer_token,
            text: newComment,
            event: 'save'
        };

        console.log('save', JSON.stringify(request));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/editComment',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(JSON.stringify(response.content));
            $scope.getComments($stateParams.event_id);
        }).error(function (xhr, req, err) {
            $.notify(xhr, req, err, "err");
        });
    }

    $scope.getOneComment = function (id) {
        if ($('.' + id + '_edit').is(':hidden')) {
            $('.' + id + '_edit').show();
        } else {
            $('.' + id + '_edit').hide();
        }

        var request = {
            comment_id: id,
            _token: localStorage.satellizer_token,
        };

        console.log('get', JSON.stringify(request));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/editComment',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(JSON.stringify(response.content));
            $('#' + id + '_edit').val(response.content.comment);
        }).error(function (xhr, req, err) {
            $.notify(xhr, req, err, "err");
        });
    }

    $scope.sendMessage = function () {
        if ($scope.data.message == undefined || $scope.data.message == "") {
            $scope.errorMsg = true;
            return;
        }
        $('#sendMessage').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $scope.op_event.user_id,
                message: $scope.data.message,
                event_id: $stateParams.event_id,
                annonymous: $scope.stay_annonymous,
                _token: localStorage.satellizer_token
            };

            //alert(JSON.stringify(request));
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/sendMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                $scope.stay_annonymous = false;
                $scope.agree_message = false;
                $scope.data.message = "";
                if (response.status = 1) {
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $.notify($rootScope.msgDescription, "success");
                    //$("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                //alert(req + " / "+ err);
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function () {
        $scope.errorMsg = false;
        $scope.data.message = "";
        $scope.stay_annonymous = false;
        $scope.agree_message = false;
    }

    $scope.reportBadContentMsg = function () {
        $('#reportBadContent').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                event_id: $stateParams.event_id,
                subject: $scope.msg_category,
                message: $scope.data.reportMessage
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/reportEvent',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                $scope.data.reportMessage = "";
                if (response.status = 1) {
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelReportModel = function () {
        $scope.errorMsg = false;
        $scope.data.reportMessage = "";
        $scope.msg_category = "It\'\s spam";
    }

    $scope.getAroundEvents = function (lat, lng, parentCategory, category, tokenFalse) {
        var getToken = localStorage.satellizer_token;
        if (tokenFalse) {
            getToken = false;
        }
        var reqt = {
            lat: lat,
            lng: lng,
            parentCategory: parentCategory,
            category: category,
            radiusKm: $rootScope.userInfo.notifyRadius,
            _token: getToken
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/aroundEvents',
            data: {data: JSON.stringify(reqt)}
        }).success(function (response) {
            $scope.aEvents = response.aroundEvents;
            // var markersData = $scope.aEvents;
            let respon = $scope.aEvents;

            let positions = ['0-0']
            let multipleEvents = []
            let oneEventPoints = []

            respon.map(item => {
                positions.map(pos => {
                    if (pos === item.location_lat + "-" + item.location_long)
                        item.unique = false
                    else item.unique = true
                })
                positions.push(item.location_lat + "-" + item.location_long)
            })

            respon.map(item => {
                if (item.unique) {
                    let count = 0
                    positions.map(pos => {
                        if (pos === item.location_lat + "-" + item.location_long)
                            count++
                    })
                    if (count > 1)
                        item.unique = false
                }
            })

            respon.map(item => {
                if (item.unique) {
                    oneEventPoints.push(item)
                } else {
                    let pushed = false
                    multipleEvents.map(mult => {
                        if (mult.lat + "-" + mult.long === item.location_lat + "-" + item.location_long) {
                            mult.count++
                            mult.events.push(item)
                            pushed = true
                        }
                    })
                    if (!pushed)
                        multipleEvents.push({
                            lat: item.location_lat,
                            long: item.location_long,
                            count: 1,
                            events: [item]
                        })
                }
            })


            //work many markers on map
            var map, infoWindow;
            var centerLatLng = new google.maps.LatLng(lat, lng);
            $scope.centerMarker = centerLatLng;
            var mapOptions = {
                center: centerLatLng,
                zoom: 16 - $rootScope.userInfo.notifyRadius
                //zoom: 27 - $rootScope.userInfo.notifyRadius,
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);

            infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(map, "click", function () {
                infoWindow.close();
            });

            oneEventPoints.forEach(function (item, i, arr) {
                var latLng = new google.maps.LatLng(item.location_lat, item.location_long);
                var iconUrl = "img/map_icon.png";
                if (lng === item.location_long) {
                    iconUrl = "img/map_icon_current.png";
                }
                var name = item.title;
                var address = item.address;
                var id = item.id;
                var mediaThumb;
                //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                if (!item.mediaThumb) {
                    mediaThumb = "img/blog-single-4.jpg";
                } else if (item.mediaThumb.indexOf('http')) {
                    mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                } else {
                    mediaThumb = item.mediaThumb;
                }
                // Добавляем маркер с информационным окном
                addMarker(latLng, name, address, mediaThumb, id, iconUrl);
            });

            multipleEvents.forEach(function (item, i, arr) {
                var latLng = new google.maps.LatLng(item.events[0].location_lat, item.events[0].location_long);
                var iconUrl = "img/map_icon_many.png";
                if (lng === item.events[0].location_long) {
                    //iconUrl = "img/map_icon_current_many.png";
                    iconUrl = "img/map_icon_current.png";
                }
                addMarkers(item.events, latLng, iconUrl);
            });


            google.maps.event.addDomListener(window, "load");

            // Функция добавления маркера с информационным окном

            function addMarker(latLng, name, address, mediaThumb, id, iconUrl) {
                // var iconUrl = "img/map_icon.png";

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: name
                });

                // Отслеживаем клик по нашему маркеру
                google.maps.event.addListener(marker, "click", function () {
                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="infowindow">' +
                        '<h4> <a href="#/main/event/' + id + '" target="_blank">' + name + '</a></h4>' +
                        '<p>' + address + '</p>' +
                        '<a href="#/main/event/' + id + '" target="_blank"><img src="' + mediaThumb + '">' + '</a></div>';
                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, marker);
                });
            }

            //
            // много маркеров на одном пине
            function addMarkers(array, latLng, iconUrl) {
                // var iconUrl = "img/map_icon_many.png";
                var address = array[0].address;
                var firstThumb = array[0].mediaThumb;
                var eventMarkerItems = array.length.toString() + " Events";

                var markers = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: eventMarkerItems
                });

                google.maps.event.addListener(markers, "click", function () {
                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="manyItemsWindow"><h4>' + eventMarkerItems + '</h4><p>' + address + '</p>';
                    contentString += '<div style="width: 80%;"><ul>';
                    array.forEach(function (item, i, arr) {
                        var mediaThumb;
                        //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                        if (!item.mediaThumb || item.mediaThumb.indexOf('/_thumb')) {
                            mediaThumb = "img/blog-single-4.jpg";
                        } else if (item.mediaThumb.indexOf('http://vi')) {
                            mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                        } else {
                            mediaThumb = item.mediaThumb;
                        }

                        contentString +=
                            '<li>' +
                            '<i class="fas fa-burn"></i> ' +
                            '<a href="#/main/event/' + item.id + '" title="' + item.title + '" target="_blank">' + item.title.substr(0, 55) + '</a>' +
                            '<div class="manyItWinHyde"><img src="' + mediaThumb + '" ng-src="img/blog-single-4.jpg"></div>' +
                            '</li>';
                    });
                    contentString += '</ul></div>';
                    contentString += '<div style="width: 20%;float: right;position: absolute;right: -10px; top: 20px; margin: 10px;"></div>';
                    contentString += '</div>';

                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, markers);
                });

            }

        }).error(function (xhr, req, err) {
            console.log(err);
        });


    }

    /* LIKE */

    $scope.isLiked = function () {

        var request = {
            _token: localStorage.satellizer_token,
            event_id: $stateParams.event_id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isLiked',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            var liked = response.content;
            if (liked) {
                document.querySelector('.liked .addLiked').hidden = true;
                document.querySelector('.liked .disLiked').hidden = false;
            }

            if (!liked) {
                document.querySelector('.liked .addLiked').hidden = false;
                document.querySelector('.liked .disLiked').hidden = true;
            }

            $scope.Liked = response.content;
            $scope.likeCount = response.count;

        }).error(function (xhr, req, err) {
            //$.notify("error: "+req, "error");
        });
    }

    $scope.addLike = function () {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: $stateParams.event_id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
                $scope.isLiked();
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.disLike = function () {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: $stateParams.event_id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/disLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
                $scope.isLiked();
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.socialShare = function (shareOn) {
        alert(shareOn);
        //$scope.thisPageShareUrl = $scope.op_event.id+"_"+$scope.op_event.title+"_"+$scope.op_event.mediaThumb.split('/').pop().split('_').shift();
        switch (shareOn) {
            case "fb":
                //alert("Here");
                window.open("https://www.facebook.com/sharer/sharer.php?src=sp&u=" + encodeURIComponent($scope.thisPageShareUrl));
                break;
            case "twitter":
                window.open("https://twitter.com/share?url=" + encodeURIComponent($scope.shareLink));
                break;
            case "whatsapp":
                window.open("whatsapp://send?text=" + $scope.shareLink);
                break;
            case "messenger":
                window.open("https://www.addtoany.com/add_to/facebook_messenger?linkurl=" + $scope.shareLink);
                break;
            default:

        }
    }

    $scope.socialIcons = function (str) {
        var linkToSharePhp = "https://viround.com/share.php?z=" + encodeURIComponent(str);
        var vkLink = "https://vk.com/share.php?url=" + encodeURIComponent(document.URL) + "&title=" + encodeURIComponent($scope.op_event.title) + "&image=" + encodeURIComponent($scope.op_event.mediaThumb.split());
        var twitLink = "https://twitter.com/intent/tweet?url=" + encodeURIComponent(linkToSharePhp) + "&text=" + encodeURIComponent($scope.op_event.title);
        var telega = "https://telegram.me/share/url?url=" + encodeURIComponent(linkToSharePhp) + "&text=" + encodeURIComponent($scope.op_event.title);
        var watsapp = "https://wa.me/?text=" + encodeURIComponent(linkToSharePhp + " ") + encodeURIComponent($scope.op_event.title);
        //var viber = "https://3p3x.adj.st/?adjust_t=u783g1_kw9yml&adjust_fallback=https%3A%2F%2Fwww.viber.com%2F%3Futm_source%3DPartner%26utm_medium%3DSharebutton%26utm_campaign%3DDefualt&adjust_campaign=Sharebutton&adjust_deeplink="+encodeURIComponent("viber://forward?text=" + encodeURIComponent($scope.op_event.title + " " + window.location.href));
        $scope.socShareLink = [
            {
                id: "fb",
                img: "img/social/fb.png",
                url: "https://www.facebook.com/sharer/sharer.php?src=sp&u=" + linkToSharePhp
            },
            {id: "vk", img: "img/social/vk.png", url: vkLink},
            {id: "tw", img: "img/social/tw.png", url: twitLink},
            {id: "ws", img: "img/social/ws.png", url: watsapp},
            {id: "tg", img: "img/social/tg.png", url: telega},
            {id: "vb", img: "img/social/vb.png", url: ""},
        ];
    }

    $scope.viberPush = function () {
        //window.open("viber://forward?text=" + encodeURIComponent($scope.op_event.title + " " + "https://viround.com/share.php?z="+$scope.thisPageShareUrl));
        window.open("viber://forward?text=" + "https://viround.com/share.php?z=" + $scope.thisPageShareUrl);
        console.log("viber://forward?text=" + "https://viround.com/share.php?z=" + $scope.thisPageShareUrl);
    }

    $scope.copyToClipboard = function () {

        var urlField = document.querySelector('#url_field');
        urlField.select();

        console.log(urlField);

        var success = false;

        try {
            // copy text
            success = document.execCommand('copy');
        } catch (err) {
            success = false;
        }

        if (success) {
            //urlField.blur();
            $('#copied').modal('show');
        } else {
            window.prompt("Copy to clipboard: Ctrl/Cmd+C. ", "text");
        }
    }

    $scope.openReply = function (commentId) {
        if ($('#' + commentId).is(':visible')) {
            $('#' + commentId).hide();
        } else {
            $('#' + commentId).show();
        }
    }

    $scope.replyComment = function (commentId, replyText, toUserId) {
        console.log(commentId);
        promise = isValidToken.check();
        promise.then(function (token) {
            var commentTxt = replyText;
            commentTxt = commentTxt.replace(/(\n)+/g, "<br>");
            var request = {
                event_id: $stateParams.event_id,
                comment: commentTxt,
                comment_id: commentId,
                to_user_id: toUserId,
                _token: localStorage.satellizer_token
            }
            console.log(JSON.stringify(request));
            //alert(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/addComment',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                if (response.status == 1) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        $scope.getComments($stateParams.event_id);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                    $scope.comment = {};
                } else {
                    $rootScope.msgDescription = $filter('translate')('Error_msg7');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $timeout(function () {
        $scope.urlCopy = "https://viround.com/share.php?z=" + encodeURIComponent($scope.thisPageShareUrl);

        var cutTextareaBtn = document.querySelector('.js-textareacutbtn');

        cutTextareaBtn.addEventListener('click', function (event) {
            var cutTextarea = document.querySelector('.js-cuttextarea');
            cutTextarea.select();

            try {

                var сutIsEnabled = document.queryCommandEnabled('copy');
                console.log(сutIsEnabled, 'copyIsEnabled');
                var successful = document.execCommand('copy');
                var msg = successful ? 'OK' : 'NO';
                //console.log('Cutting text command was ' + msg);
                сutIsEnabled = document.queryCommandEnabled('copy');
                //console.log(сutIsEnabled, 'Realy success!');
                $.notify(msg, "success");
            } catch (err) {
                //console.error(err);
                $.notify(msg, "error");
            }
        });

        //$scope.thisPageShareUrl = $scope.op_event.id+"_"+$scope.op_event.title+"_"+$scope.op_event.mediaThumb.split('/').pop().split('_').shift();
        //document.querySelector('meta[property="og:title"]').attributes.content.value = $rootScope.pageTitle;
        //document.querySelector('meta[property="og:image"]').attributes.content.value = $rootScope.pageImg;
        $('.carousel').carousel('pause');
    }, 2000);

    $scope.followedEventUser = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/followedEventUser',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $scope.followedEvent = response.followed;
            //console.log('followedEvent',$scope.followedEvent);
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.unSubscribeEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/unSubscribeEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            console.log(response);
            $scope.followedEventUser();
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.subscribeToEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/subscribeToEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            console.log(response);
            $scope.followedEventUser();
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.scrollTo = function () {
        var url = location.href.split('#').pop();
        if (url > 0) {
            var commentId = 'c-' + url;
            console.log(url);
            $timeout(function () {
                $location.hash(commentId);
                $anchorScroll();
            }, 100);
        }
    };

    function checkRTL(s) {
        var ltrChars = 'A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8\u0300-\u0590\u0800-\u1FFF' + '\u2C00-\uFB1C\uFDFE-\uFE6F\uFEFD-\uFFFF',
            rtlChars = '\u0591-\u07FF\uFB1D-\uFDFD\uFE70-\uFEFC',
            rtlDirCheck = new RegExp('^[^' + ltrChars + ']*[' + rtlChars + ']');

        return rtlDirCheck.test(s);
    };

}]);

LOMCityControllers.controller('createEventsCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', '$interval', '$location', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken, $interval, $location) {
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.newEvent = {};
    $scope.result1 = '';
    $scope.options1 = null;
    $scope.details1 = '';
    $scope.address = '';
    //$scope.selectedOption = 'Stolen';
    $scope.mediaType = '';
    $scope.mediaUrl = '';
    $scope.files = [];
    $scope.galleryFiles = null;
    $scope.fileUploading = false;
    $scope.maxFileSizeMB = 8e+7;
    $scope.required = false;
    $scope.pCategory = null;
    $scope.sCategory = null;

    var isCatReady = $interval(function () {
        if ($rootScope.optionsReady) {
            $scope.setCat();
            $interval.cancel(isCatReady);
            //$scope.$apply();
        }
        //alert("Here");
    }, 10);

    $scope.setCat = function () {
        $timeout(function () {
            //alert($rootScope.chooseOptions[0].name);
            // $scope.selectedPOption = $rootScope.parentCategories[0].name;
            $scope.selectedPOption = "";
            // $scope.selectedOption = $rootScope.chooseOptions[0].name;
            $scope.selectedOption = "";
            $scope.updateSubCat($scope.selectedPOption);
            $scope.$apply();
        }, 100);
    }
    $('#setTimeExample').timepicker({'timeFormat': 'H:i:s', 'step': 30});
    $('#setDiscountEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
    $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});

    $timeout(function () {
        $('#setTimeExample').timepicker('setTime', new Date());
    }, 500);

    $scope.opened = false;
    $scope.nowSartDate = new Date();
    $scope.date = new Date();
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'dd/MM/yyyy', 'dd-MM-yyyy', 'shortDate'];
    $scope.format = $scope.formats[4];

//calendar позволяет делать много датапикеров в одной форме
    $scope.calendar = {
        opened: {},
        dateFormat: $scope.formats[4],
        dateOptions: {},
        open: function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calendar.opened[which] = true;
        }
    };

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }

    $scope.addEvent = function () {
        //console.log($scope.mediaType +' '+$scope.mediaUrl);
        var temp = $('#setTimeExample').val();
        var zz = temp.split(":");
        $scope.time = new Date();
        $scope.time.setHours(zz[0]);
        $scope.time.setMinutes(zz[1]);
        $scope.time.getMilliseconds(zz[2]);
        //console.log($scope.time);

        var whenDate = $scope.date;
        var whenTime = $scope.time;

        var dateString = whenDate.getFullYear() + '-' +
            ('00' + (whenDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + whenDate.getDate()).slice(-2) + ' ' +
            ('00' + whenTime.getHours()).slice(-2) + ':' +
            ('00' + whenTime.getMinutes()).slice(-2) + ':' +
            ('00' + whenTime.getSeconds()).slice(-2);
        // alert(dateString);

        var whenStr = whenDate.getFullYear() + "-" + whenDate.getMonth() + 1 + "-" + whenDate.getDate();
        var timeStr = whenTime.getHours() + ":" + whenTime.getMinutes() + ":" + whenTime.getSeconds();
        var when = whenStr + '~' + timeStr;

        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var coord = $scope.address.geometry.location;
        var formatted_address = $scope.address.formatted_address;
        //var when = $scope.date + ' ' + $scope.time;

// --------------------------------------------------------
        function dateStrings(dateItem, timeItem) {
            var dd = timeItem.split(":");
            var dtime = new Date();
            dtime.setHours(dd[0]);
            dtime.setMinutes(dd[1]);
            dtime.getMilliseconds(dd[2]);

            var dDateInput = dateItem.split('-');
            var dDateString = dDateInput[2] + "-" + dDateInput[1] + "-" + dDateInput[0];
            var dDate = new Date(dDateString);
            var dTime = dtime;

            var dString = dDate.getFullYear() + '-' +
                ('00' + (dDate.getMonth() + 1)).slice(-2) + '-' +
                ('00' + dDate.getDate()).slice(-2) + ' ' +
                ('00' + dTime.getHours()).slice(-2) + ':' +
                ('00' + dTime.getMinutes()).slice(-2) + ':' +
                ('00' + dTime.getSeconds()).slice(-2);

            return dString;
        }

        var endDate = $('#endDate').val();
        var endTemp = $('#setEndTime').val();
        var endDateString = null;
        if (endDate != null & endTemp != null) {
            var endDateString = dateStrings(endDate, endTemp);
        }

// --------------------------------------------------------

        var discountEndDate = $('#endDiscountDate').val();
        var disEndTemp = $('#setDiscountEndTime').val();
        var discountEndDateString = null;
        if (discountEndDate != null & disEndTemp != null) {
            var discountEndDateString = dateStrings(discountEndDate, disEndTemp);
        }

        var loc = $.map(coord, function (value, index) {
            return [value];
        });
        if ($scope.newEvent.reward == undefined) {
            $scope.newEvent.reward = '';
        }
        //console.log(coord);
        var request = {
            address: formatted_address,
            title: $scope.newEvent.title,
            description: $scope.newEvent.description,
            reward: $scope.newEvent.reward,
            when: dateString,
            endEventTime: endDateString,
            endDiscountTime: discountEndDateString,
            discount: $('#discount').val(),
            lat: coord.lat(),
            long: coord.lng(),
            _token: localStorage.satellizer_token,
            category: $scope.selectedOption,
            parentCategory: $scope.selectedPOption,
            mediaType: 'image',
            mediaUrl: $scope.mediaUrl,
            location: $scope.address,
            galleryFiles: $scope.galleryFiles,
            email: $scope.newEvent.email,
            phone: $scope.newEvent.phone,
        };
        console.log('request', JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(JSON.stringify(response));
            $scope.fileUploading = false;
            if (response.status == 1) {
                $scope.newEvent = {};
                $scope.date = new Date();
                $scope.address = '';
                $scope.selectedOption = 'Stolen';
                $scope.result1 = '';
                $scope.mediaType = '';
                $scope.mediaUrl = '';
                $rootScope.msgDescription = response.msg;
                if (response.msg == 'Event was published successfully.') {
                    $rootScope.msgDescription = $filter('translate')('event_published_successfully');
                }
                if (response.msg == 'You are not authorized to publish in this category.') {
                    $rootScope.msgDescription = $filter('translate')('not_authorized_to_publish_event');
                }
                //alert(response.msg);
                $.notify($filter('translate')('nfy_event_added'), "success");
                $timeout(function () {
                    $location.path("/allEvents");
                    //$state.go('main.allEvents', {});
                }, 1000);
                // $("#alertModal").modal('show').on("shown.bs.modal", function () {
                //     $timeout(function () {
                //         $("#alertModal").modal("hide");
                //         $state.go('main.allEvents', {});
                //     }, 1000);
                // });
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            //console.log(xhr);
            //console.log(err);
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    // $scope.remove = function(){
    //   $scope.files.splice(this.$index, 1);
    // }

    $scope.inputFiles = [];

    $scope.bigFilesName = [];

    // Second upload
    var secondUpload = new FileUploadWithPreview('mySecondImage', {showDeleteButtonOnImages: true})
    var secondUploadInfoButton = document.querySelector('.upload-info-button--second');

    // Image selected event listener
    window.addEventListener('fileUploadWithPreview:imageSelected', function (e) {
        $scope.bigFilesName = [];
        if (e.detail.uploadId === 'mySecondImage') {
            //console.log(e.detail.selectedFilesCount)
            //console.log(e.detail.cachedFileArray)
            var list = e.detail.cachedFileArray;
            list.forEach(function (item, index, object) {
                if (item.size > $scope.maxFileSizeMB) {
                    //$scope.bigFilesName.push({'name': item.name, 'size':$scope.formatBytes(item.size, 3)});
                }
            });

            $scope.inputFiles = e.detail.cachedFileArray;
            //console.log('!!!!!!!!!!!!!',$scope.bigFilesName, list);
        }
    })

    // Image deleted event listener
    window.addEventListener('fileUploadWithPreview:imageDeleted', function (e) {
        $scope.bigFilesName = [];
        if (e.detail.uploadId === 'mySecondImage') {
            //console.log(e.detail.selectedFilesCount);
            //console.log(e.detail.cachedFileArray);
            //console.log(typeof(e.detail.cachedFileArray));
            $scope.files.splice(e.detail.selectedFilesCount, 1);
            //console.log($scope.files);

            var list = e.detail.cachedFileArray;
            list.forEach(function (item, index, object) {
                if (item.size > $scope.maxFileSizeMB) {
                    //$scope.bigFilesName.push({'name': item.name, 'size':$scope.formatBytes(item.size, 3)});
                }
            });

            $scope.inputFiles = e.detail.cachedFileArray;
            //$scope.inputFiles = list;
            //console.log($scope.bigFilesName, list);
        }
    });

    $scope.uploadFile = function () {

        var files = $scope.inputFiles;
        // files.forEach(function(item, index, object) {
        //     if (item.size > $scope.maxFileSizeMB) {
        //         //object.splice(index, 1);
        //     }
        // });

        files.splice(6);

        var fd = new FormData();

        angular.forEach(files, function (el, key) {
            //console.log(el);
            var tempFile = el.type;
            if (tempFile.indexOf("image") != -1) {
                fileType = 'image';
            } else if (tempFile.indexOf("video") != -1) {
                fileType = 'video';
            }
            fd.append('files[]', el);
            console.log('el', el);
        });

        var uploadUrl = $rootScope.baseUrl + "/api/uploads3";


        const config = {
            onUploadProgress: function (progressEvent) {
                $scope.percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                var procent = $scope.percentCompleted + '%';
                $('.progress-bar').css('width', procent)
                $('.procent').text(procent);
                console.log(procent)
            }
        }

        console.log('fd', fd);

        axios.post(uploadUrl, fd, config)
            .then(function (res) {
                console.log('res', res);
                $scope.mediaUrl = res.data.content.mediaUrl;
                $scope.mediaType = res.data.content.mediaType;
                $scope.galleryFiles = res.data.content.gallery;
                console.log(res.data.content.gallery);
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.addEvent();
                }, function (response) {
                    $state.go('main.login', {});
                });
            })
            .catch(function (err) {
                $.notify("error: " + err, "error");
                console.log(err);
            });
    };

    $scope.uploadFile_addEvent = function () {
        if ($scope.files.length > 0) {
            $scope.fileUploading = true;
            $scope.uploadFile();
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.addEvent();
            }, function (response) {
                $state.go('main.login', {});
            });
            $scope.fileUploading = true;
        }
    }
    $scope.updateSubCat = function (filter) {
        var catid = $('#parent-cat option:selected')[0].id - 1;
        $scope.pCategory = $rootScope.parentCategories[catid];

        $scope.selectedPOption = filter;
        //alert($scope.selectedPOption);
        for (var i = 0; i < $rootScope.chooseOptions.length; i++) {
            if ($rootScope.chooseOptions[i].parentCategory == filter) {
                $scope.selectedOption = $rootScope.chooseOptions[i].name;
                $scope.sCategory = $rootScope.chooseOptions[i];
                break;
            }
        }
        //console.log("Here")
        //$scope.$apply();
        $timeout(function () {
            $('#setDiscountEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
            $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
            //document.querySelectorAll('#sub-cat option[value="? string: ?"]')[0].remove();
        }, 500);
    }

    $scope.updateSubCatdata = function () {
        var scatid = $('#sub-cat option:selected')[0].id - 9;
        $scope.sCategory = $rootScope.chooseOptions[scatid];
    }

    $scope.formatBytes = function (bytes, decimals) {
        if (bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals <= 0 ? 0 : decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    $scope.timeToTime = function (goodClass, badClass) {
        var goodTime = $('#' + goodClass).val();
        $('#' + badClass).val(goodTime);
    }

    $scope.getDateData = function (inputData) {
        $scope.endEvData = inputData;
    }

    $scope.toNowDate = function () {
        $scope.date = new Date();
    }

    $scope.setEndTime = function () {
        $timeout(function () {
            $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
        }, 500);
    }
}]);

LOMCityControllers.controller('settingsCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', '$timeout', 'isValidToken', function ($rootScope, $scope, $timeout, $state, $http, $filter, $timeout, isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getSettingsData();
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.settingResponse = '';
    $scope.result1 = '';
    $scope.result2 = '';
    $scope.result3 = '';
    $scope.result4 = '';
    $scope.result5 = '';
    $scope.l1 = '';
    $scope.l2 = '';
    $scope.l3 = '';
    $scope.l4 = '';
    $scope.l5 = '';
    $rootScope.settingRange = 10;
    $scope.km_checked = false;
    $scope.errorMsg = false;
    $scope.newPassword = '';
    $scope.confirmPassword = '';

    $("#iconSpan1").click(function () {
        $scope.result1 = '';
        $scope.l1 = '';
        $("#l1").val('');
        $scope.$apply();
        //$("#iconSpan1").hide();
    });
    $("#iconSpan2").click(function () {
        $scope.result2 = '';
        $scope.l2 = '';
        $("#l2").val('');
        $scope.$apply();
        //$("#iconSpan2").hide();
    });
    $("#iconSpan3").click(function () {
        $scope.result3 = '';
        $scope.l3 = '';
        $("#l3").val('');
        $scope.$apply();
        //$("#iconSpan3").hide();
    });
    $("#iconSpan4").click(function () {
        $scope.result4 = '';
        $scope.l4 = '';
        $("#l4").val('');
        $scope.$apply();
        //$("#iconSpan4").hide();
    });
    $("#iconSpan5").click(function () {
        $scope.result5 = '';
        $scope.l5 = '';
        $("#l5").val('');
        $scope.$apply();
        //$("#iconSpan5").hide();
    });
    $('.checkbox-option').on("click", function () {
        $(this).toggleClass('checked');
        var checkbox = $(this).find('input');
        if (checkbox.prop('checked') === false) {
            checkbox.prop('checked', true);
            $scope.km_checked = true;
            $scope.$apply();
        } else {
            checkbox.prop('checked', false);
            $scope.km_checked = false;
            $scope.$apply();
        }
    });

    $scope.getSettingsData = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getSettings',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                console.log(JSON.stringify(response));
                if (response.locations.length != 0) {
                    $scope.settingResponse = response;

                    console.log($scope.settingResponse);

                    // for (var i = 0; i < 3; i++) {
                    //     alert(i); // 0, 1, 2
                    // }
                    // document.getElementById("l1").value = (response.locations[0].name != undefined) ? response.locations[0].name : '';
                    // document.getElementById("l2").value = (response.locations[1].name != undefined) ? response.locations[1].name : '';
                    // document.getElementById("l3").value = (response.locations[2].name != undefined) ? response.locations[2].name : '';
                    // document.getElementById("l4").value = (response.locations[3].name != undefined) ? response.locations[3].name : '';
                    // document.getElementById("l5").value = (response.locations[4].name != undefined) ? response.locations[4].name : '';

                    document.getElementById("l1").value = (response.locations[0] != undefined) ? response.locations[0].name : '';
                    document.getElementById("l2").value = (response.locations[1] != undefined) ? response.locations[1].name : '';
                    document.getElementById("l3").value = (response.locations[2] != undefined) ? response.locations[2].name : '';
                    document.getElementById("l4").value = (response.locations[3] != undefined) ? response.locations[3].name : '';
                    document.getElementById("l5").value = (response.locations[4] != undefined) ? response.locations[4].name : '';

                    $scope.l1 = document.getElementById("l1").value;
                    $scope.l2 = document.getElementById("l2").value;
                    $scope.l3 = document.getElementById("l3").value;
                    $scope.l4 = document.getElementById("l4").value;
                    $scope.l5 = document.getElementById("l5").value;
                    /*
                    $scope.result1 = response.locations[0].name;
                    $scope.result2 = response.locations[1].name;
                    $scope.result3 = response.locations[2].name;
                    $scope.result4 = response.locations[3].name;
                    $scope.result5 = response.locations[4].name;
                    */

                    document.querySelector('#notification' + response.settings[0].notifications).checked = true;

                }
                //var nRange = parseFloat(response.settings[0].notifyRadius);
                //console.log((nRange<=1));
                //$rootScope.settingRange = (nRange <= 1) ? nRange * 10 : nRange;
                $rootScope.settingRange = response.settings[0].notifyRadius * 1000;

                //console.log($rootScope.settingRange);
                if (response.settings[0].notifyUnits == 'km') {
                    $scope.km_checked = true;
                } else {
                    $scope.km_checked = false;
                }
            }
        }).error(function (xhr, req, err) {
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.changeSettingsData = function (result1, result2, result3, result4, result5, range) {
        var resultArr = [];
        resultArr.push(result1, result2, result3, result4, result5);

        var locationArr = [];
        for (var i = 0; i < 5; i++) {
            var tempResult = {
                geometry: {
                    location: {
                        G: '',
                        K: ''
                    }
                },
                formatted_address: ''
            };

            if ($scope.settingResponse) {

                if (resultArr[i] != "") {

                    tempResult.geometry.location.G = resultArr[i].geometry.location.lat();
                    tempResult.geometry.location.K = resultArr[i].geometry.location.lng();
                    tempResult.formatted_address = resultArr[i].formatted_address;

                } else if (document.querySelector('#l' + (i + 1)).value != "" && $scope.settingResponse.locations[i]) {

                    console.log(document.getElementById("l" + (i + 1)).value);
                    tempResult.geometry.location.G = $scope.settingResponse.locations[i].location_lat;
                    tempResult.geometry.location.K = $scope.settingResponse.locations[i].location_long;
                    tempResult.formatted_address = $scope.settingResponse.locations[i].name;

                } else {

                    tempResult.geometry.location.G = "";
                    tempResult.geometry.location.K = "";
                    tempResult.formatted_address = "";
                }

            } else {

                if (resultArr[i] != "") {
                    tempResult.geometry.location.G = resultArr[i].geometry.location.lat();
                    tempResult.geometry.location.K = resultArr[i].geometry.location.lng();
                    tempResult.formatted_address = resultArr[i].formatted_address;
                } else {
                    tempResult.geometry.location.G = "";
                    tempResult.geometry.location.K = "";
                    tempResult.formatted_address = "";
                }

            }


            locationArr.push(tempResult);

        }

        console.log('locationArr',locationArr);
        //console.log(JSON.stringify(result1));

        $scope.checkField = function (id) {
            return document.getElementById(id).value;
        }

        var notification = document.querySelector('#notification1').value;
        if (document.querySelector('#notification0').checked) {
            notification = document.querySelector('#notification0').value;
        }


        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                settings: {
                    notifyUnits: $scope.km_checked == true ? 'km' : 'mile',
                    notifyRadius: range / 1000
                    //notifyRadius: parseFloat(range)
                },

                locations: [{
                    lat: locationArr[0].geometry.location.G,
                    long: locationArr[0].geometry.location.K,
                    name: locationArr[0].formatted_address
                }, {
                    lat: locationArr[1].geometry.location.G,
                    long: locationArr[1].geometry.location.K,
                    name: locationArr[1].formatted_address
                }, {
                    lat: locationArr[2].geometry.location.G,
                    long: locationArr[2].geometry.location.K,
                    name: locationArr[2].formatted_address
                }, {
                    lat: locationArr[3].geometry.location.G,
                    long: locationArr[3].geometry.location.K,
                    name: locationArr[3].formatted_address
                }, {
                    lat: locationArr[4].geometry.location.G,
                    long: locationArr[4].geometry.location.K,
                    name: locationArr[4].formatted_address
                }],

                // location1: result1 != '' ? result1 : '',
                // location2: result2 != '' ? result2 : '',
                // location3: result3 != '' ? result3 : '',
                // location4: result4 != '' ? result4 : '',
                // location5: result5 != '' ? result5 : '',

                notification: notification
            };
            console.log('request',request);
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/changeSettings',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                console.log('response', response);
                if (response.status == 1) {
                    $rootScope.msgDescription = $filter('translate')('Success_msg2');
                    //$("#alertModal").modal('show');
                    $.notify($rootScope.msgDescription, "success");
                }
                $state.reload();
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.logOut = function () {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token
            };
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/logOut',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                if (response.status == 1) {
                    //localStorage.clear();
                    localStorage.removeItem('satellizer_token');
                    $state.go('main.login', {});
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.changePassword = function () {
        if ($scope.newPassword == "" || $scope.confirmPassword == "" || $scope.newPassword != $scope.confirmPassword) {
            $scope.errorMsg = true;
            return;
        }
        //$('#changePassword').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                password: $scope.newPassword,
                currPassword: $scope.currPassword,
            };
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/changePassword',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                if (response.content == 'saved') {
                    $('#changePassword').modal('hide');
                    $scope.cancelModel();
                    $.notify("Change password successfully.", "success");
                }

                if (response.content == 'Wrong password') {
                    $.notify($filter('translate')('Enter_correct_pass'), "error");
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function () {
        $scope.errorMsg = false;
        $scope.newPassword = '';
        $scope.confirmPassword = '';
        $scope.newEmail = '';
        $scope.currentPassword = '';
        $scope.currPassword = '';
    }

    $scope.changeKMM = function (type) {
        if (type == 'km') {
            //console.log('km');
            $scope.km_checked = true;
            if (!$scope.$$phase) { // check if digest already in progress
                $scope.$apply();
            }
        } else {
            //console.log('mile');
            $scope.km_checked = false;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        }
    }

    $scope.changeEmail = function () {
        $scope.msgErrEmail = '';
        var request = {
            _token: localStorage.satellizer_token,
            email: $scope.newEmail,
            password: $scope.currentPassword,
        };

        //console.log(JSON.stringify(request));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/changeEmail',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(response);

            if (response.content == 'Wrong password') {
                $scope.msgErrEmail = $filter('translate')('Enter_correct_pass');
                $.notify($scope.msgErrEmail, "error");
            }
            if (response.content == 'saved') {
                $('#changeEmail').modal('hide');
                $scope.cancelModel();
                $.notify("Change email successfully.", "success");
            }
            $state.go('main.settings', {});
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.reportOnBugs = function () {
        $('#reportOnBugs').modal('hide');

        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                message: $scope.reportBugsText
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/reportOnBugs',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                console.log(JSON.stringify(response));
                $scope.data.reportMessage = "";
                if (response.status = 1) {
                    //$rootScope.msgDescription = $filter('translate')('Message_send');
                    //$("#alertModal").modal('show');
                    $.notify("OK", "success");
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.sendTestSms = function () {
        var request = {
            _token: localStorage.satellizer_token,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendTestSms',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            console.log(response);
            $.notify("Change email successfully.", "success");
            //$state.go('main.settings', {});
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

}]);

LOMCityControllers.controller('messagesCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getMessages();
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.data = {};
    $scope.errorMsg = false;
    $scope.from_user = '';
    $scope.event_id = '';
    $scope.message_id = '';
    $scope.annonymous = '';
    $scope.to_user = '';

    $scope.getMessages = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $rootScope.showLoading();
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getMessages',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            var messages = response.content;
            var current_date = new Date();
            var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;
            for (var i = 0; i < messages.length; i++) {
                if (messages[i]['avatar'] == '' || messages[i]['avatar'] == null) {
                    messages[i]['avatar'] = 'img/default-avatar.png';
                }
                if (messages[i]['firstName'] == null && messages[i]['lastName'] == null) {
                    messages[i]['firstName'] = messages[i]['from_user'];
                }
                var tempDate = messages[i]['created_at'];
                var zz = tempDate.split(" ");
                var zz1 = zz[0].split("-");
                var zz2 = zz[1].split(":");
                var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                var a = zzz3.getTime();
                messages[i]['created_at'] = a - gmt_offset;
            }
            if (messages.length == 0) {
                $scope.noMessages = true;
            } else {
                $scope.noMessages = false;
            }
            $scope.messages = messages;
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.holdParams = function (from_user, event_id, message_id, annonymous, to_user) {
        //alert(annonymous);
        $scope.from_user = from_user;
        $scope.event_id = event_id;
        $scope.message_id = message_id;
        $scope.annonymous = (annonymous == 1) ? true : false;
        $scope.to_user = to_user;
    }

    $scope.replyMessage = function () {
        if ($scope.data.message == "" || $scope.data.message == undefined) {
            $scope.errorMsg = true;
            return;
        }
        $('#replyMessage').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $scope.from_user,
                message: $scope.data.message,
                event_id: $scope.event_id,
                message_id: $scope.message_id,
                annonymous: $scope.annonymous,
                _token: localStorage.satellizer_token
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/sendMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                $rootScope.msgDescription = $filter('translate')('Message_send');
                $("#alertModal").modal('show');
                $scope.from_user = '';
                $scope.event_id = '';
                $scope.message_id = '';
                $scope.annonymous = '';
                $scope.to_user = '';
                $scope.data = {};
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function () {
        $scope.errorMsg = false;
        $scope.data.message = "";
    }

    $scope.deleteMessage = function (message_id) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                message_id: message_id
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/deleteMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

}]);

LOMCityControllers.controller('conversationsCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', '$interval', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken, $interval) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        //$scope.getMessages();
        $scope.getUsersChatList();
        /*        if(!$rootScope.inChat){
            $scope.getChatMessages($scope.userList[0].chat_id, $scope.userList[0].id)
        }*/
    }, function (response) {
        $state.go('main.login', {});
    });
    // window.scroll(0, 0);
    $scope.data = {};
    $scope.errorMsg = false;
    $scope.from_user = '';
    $scope.event_id = '';
    $scope.message_id = '';
    $scope.annonymous = '';
    $scope.to_user = '';
    $scope.intervalRefresh = false;
    $rootScope.inChat = false;

    // var getMessages = $interval(function () {
    //     //console.log($state.current);
    //     if ($state.current.name == "main.conversations") {
    //         $scope.intervalRefresh = true;
    //         //$scope.getMessages();
    //         $scope.getUsersChatList();
    //     }
    // }, 10000);

    // var getMessagesFast = $interval(function () {
    //     //console.log($state.current);
    //     if ($state.current.name == "main.conversations") {
    //         $scope.intervalRefresh = true;
    //         if ($rootScope.inChat && $rootScope.chatMessages) {
    //             $scope.getChatMessages($rootScope.chatMessages[0].chat_id, $rootScope.oppInfo.id, $rootScope.iBlockedForThis);
    //         }
    //     }
    // }, 7000);

    $scope.scrollDown = function () {
        var block = document.getElementById("chat-window");
        block.scrollTop = block.scrollHeight;
    }

    $scope.getMessages = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        if (!$scope.intervalRefresh) {
            $rootScope.showLoading();
        } else {
            $rootScope.intervalRefresh = false;
        }
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getConversations',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            var messages = response.content;
            var current_date = new Date();
            var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;
            for (var i = 0; i < messages.length; i++) {
                if (messages[i]['avatar'] == '' || messages[i]['avatar'] == null) {
                    messages[i]['avatar'] = 'img/default-avatar.png';
                }
                if (messages[i]['firstName'] == null && messages[i]['lastName'] == null) {
                    messages[i]['firstName'] = messages[i]['from_user'];
                }
                var tempDate = messages[i]['created_at'];
                var zz = tempDate.split(" ");
                var zz1 = zz[0].split("-");
                var zz2 = zz[1].split(":");
                var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                var a = zzz3.getTime();
                messages[i]['created_at'] = a - gmt_offset;
            }
            if (messages.length == 0) {
                $scope.noMessages = true;
            } else {
                $scope.noMessages = false;
            }
            $scope.messages = messages;
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.holdParams = function (from_user, event_id, message_id, annonymous, to_user) {
        //alert(annonymous);
        $scope.from_user = from_user;
        $scope.event_id = event_id;
        $scope.message_id = message_id;
        $scope.annonymous = (annonymous == 1) ? true : false;
        $scope.to_user = to_user;
    }

    $scope.replyMessage = function () {
        if ($scope.data.message == "" || $scope.data.message == undefined) {
            $scope.errorMsg = true;
            return;
        }
        $('#replyMessage').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $scope.from_user,
                message: $scope.data.message,
                event_id: $scope.event_id,
                message_id: $scope.message_id,
                annonymous: $scope.annonymous,
                _token: localStorage.satellizer_token
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/sendMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                $rootScope.msgDescription = $filter('translate')('Message_send');
                $("#alertModal").modal('show');
                $scope.from_user = '';
                $scope.event_id = '';
                $scope.message_id = '';
                $scope.annonymous = '';
                $scope.to_user = '';
                $scope.data = {};
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function () {
        $scope.errorMsg = false;
        $scope.data.message = "";
    }

    $scope.deleteMessage = function (message_id) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                message_id: message_id
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/deleteMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getMessages();
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.getUsersChatList = function () {
        var request = {
            _token: localStorage.satellizer_token
        };

        if (!$scope.intervalRefresh) {
            $rootScope.showLoading();
        } else {
            $rootScope.intervalRefresh = false;
        }

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getUsersChatList',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response.users));
            var messages = response.users;
            var current_date = new Date();
            var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;
            for (var i = 0; i < messages.length; i++) {
                if (messages[i]['avatar'] == '' || messages[i]['avatar'] == null) {
                    messages[i]['avatar'] = 'img/default-avatar.png';
                } else {
                    var avatar_user = messages[i]['avatar'];
                    if (avatar_user.indexOf('viround.com/public/uploads/avatar') > 0) {
                        avatar_user = avatar_user.split('/').pop();
                        messages[i]['avatar'] = $rootScope.mediaUrlS3 + 'avatar/' + avatar_user;
                    } else {
                        messages[i]['avatar'] = avatar_user;
                    }
                }
                if (messages[i]['firstName'] == null && messages[i]['lastName'] == null) {
                    messages[i]['firstName'] = messages[i]['from_user'];
                }

                if (messages[i]['message']) {
                    var tempDate = messages[i]['message'].created_at;
                    var zz = tempDate.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    var a = zzz3.getTime();
                    messages[i]['message'].created_at = a - gmt_offset;
                }

            }
            if (messages.length == 0) {
                $scope.noMessages = true;
            } else {
                $scope.noMessages = false;
            }
            $scope.userList = messages;
            //console.log(JSON.stringify($scope.userList[0]));
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });

    }

    $scope.getChatMessages = function (chat_id, user_id, iBlocked) {
        //console.log(chat_id);

        //document.getElementById(opponentId).classList.add('active_chat');

        $rootScope.iBlockedForThis = iBlocked;

        var request = {
            _token: localStorage.satellizer_token,
            chatId: chat_id,
            opponent: user_id
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getChatMessages',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response));

            $rootScope.oppInfo = response.opponent;

            if ($rootScope.oppInfo.avatar == '') {
                $rootScope.oppInfo.avatar = 'img/default-avatar.png';
            } else {
                if ($rootScope.oppInfo.avatar.indexOf('viround.com/public/uploads/avatar') > 0) {
                    var avatar_user = $rootScope.oppInfo.avatar.split('/').pop();
                    $rootScope.oppInfo.avatar = $rootScope.mediaUrlS3 + 'avatar/' + avatar_user;
                }
            }

            var messages = response.list.reverse();

            var current_date = new Date();
            var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;

            console.log('offset',gmt_offset);

            for (var i = 0; i < messages.length; i++) {

                if (messages[i]['message']) {
                    var tempDate = messages[i].created_at;
                    var zz = tempDate.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    var a = zzz3.getTime();
                    messages[i].created_at = a - gmt_offset;
                }

            }

            $rootScope.chatMessages = messages;

            $rootScope.inChat = true;

            $scope.scrollDown();

        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });

    }

    $scope.sendChatMessage = function (chat_id, opponentId, text) {
        var request = {
            _token: localStorage.satellizer_token,
            chatId: chat_id,
            opponent: opponentId,
            message: text
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendChatMessage',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response));
            $('#chatMessageArea').val('');
            $scope.getChatMessages(chat_id, opponentId);
            $scope.scrollDown();
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });


    }

    $('.mobile-chat-list').click(function(){
        $('.mobile-chat-list-toggle').toggleClass('active');
        $('.mobile-chat-list').toggleClass('toggle');
    });

}]);

LOMCityControllers.controller('conversationCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$stateParams', 'dhm', 'getDist', "$filter", "$sce", "isValidToken", "$interval", "dhmFilter", function ($rootScope, $scope, $timeout, $state, $http, $stateParams, dhm, getDist, $filter, $sce, isValidToken, $interval, dhmFilter) {

    var promise = isValidToken.check();
    promise.then(function (token) {
        //$scope.conversationName = $stateParams.user_name;
        $scope.getmessages($stateParams.event_id);
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.getEvent = false;
    $scope.noMessages = false;
    $scope.errorMsg = false;
    $scope.stay_annonymous = false;
    $scope.comment = {};
    $scope.isNow = false;
    $scope.eIsNow = false;
    $scope.conversationName = "";
    $scope.other_user_id = $stateParams.from_user;
    $scope.annonymous = false;

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    /*$timeout(function () {
        $scope.imageZoom = 1;
        $scope.$watch("imageZoom",
             function changeImageZoom( newValue, oldValue ) {
                 var containerWidth = $("#event-img").width();
                 var imageWidth = containerWidth * (1+ newValue/100);
                 $("#eventImage").css('max-width', imageWidth);
                 $("#eventImage").css('width', imageWidth);
            }
        );
    }, 500);*/
    /*document.getElementById("event-img").style.display = 'none';
    document.getElementById("rangeSlider").style.display = 'none';
    document.getElementById("media-con").style.display = 'none';*/

    var getMessages = $interval(function () {
        //console.log($state.current);
        if ($state.current.name == "main.conversation") {
            $scope.getmessages($stateParams.event_id);
        }
    }, 10000);

    $scope.getmessages = function (event_id) {
        //alert('eeee'+event_id);
        //$scope.comments = '';
        var request = {
            event_id: event_id,
            other_user_id: $stateParams.from_user,
            _token: localStorage.satellizer_token
        }
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getConversation',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                //alert(document.body.scrollHeight);
                $scope.iBlocked = response.iBlocked;
                if ($scope.comments != undefined) {
                    if (response.content.length > $scope.comments.length) {
                        window.setTimeout(function () {
                            window.scrollTo(0, document.body.scrollHeight);
                        }, 500);
                    }
                } else {
                    window.setTimeout(function () {
                        window.scrollTo(0, document.body.scrollHeight - 850);
                    }, 500);
                }
                comments = response.content;
                if (comments.length == 0) {
                    $scope.noMessages = true;
                } else {
                    for (var i = 0; i < comments.length; i++) {
                        if (comments[i]['avatar'] == '') {
                            comments[i]['avatar'] = 'img/default-avatar.png';
                        }

                        var r_time = dhmFilter(comments[i]['created_at']);
                        comments[i]['rtime'] = r_time[0];
                        //comments[i]['rtime'] = dhm('2015-12-09 06:20:26');
                        $scope.annonymous = comments[i]['annonymous'];
                    }
                    ;
                    $scope.comments = comments;
                    $scope.noMessages = false;
                }
            } else {
                //console.log($filter('translate')('Error_msg7'));
            }
        }).error(function (xhr, req, err) {
            //console.log($filter('translate')('Error_msg10'));
        });
    };

    $scope.deleteMessage = function (message_id, event_id) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                message_id: message_id
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/deleteMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //alert(JSON.stringify(response.msg));
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getmessages(event_id);
                }, function (response) {
                    $state.go('main.login', {});
                });
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.openEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token

        };
        $rootScope.showLoading();
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            //console.log(JSON.stringify(response));
            if (response.status == 1) {
                var even = response.content;
                if (even['avatar'] == '') {
                    even['avatar'] = 'img/default-avatar.png';
                }
                var time = dhmFilter(even['created_at']);
                if (time == '') {
                    time = 'Just now';
                }
                /*if (even['mediaType'] != 'image') {
                    document.getElementById("event-img").style.display = 'none';
                    document.getElementById("rangeSlider").style.display = 'none';
                }else{
                    document.getElementById("event-img").style.display = 'block';
                    document.getElementById("rangeSlider").style.display = 'block';
                }
                if(even['mediaType'] == ''){
                    document.getElementById("media-con").style.display = 'none';
                }else{
                    document.getElementById("media-con").style.display = 'block';
                }*/
                $scope.isNow = ((time.indexOf("Yesterday") > -1) || (time.indexOf("days") > -1) || (time.indexOf("-") > -1)) ? false : true;
                even['rtime'] = time;
                var timeArr = String(even['created_at']).split(" ");
                timeArr = String(timeArr[1]).split(":");
                even['timeString'] = timeArr[0] + ":" + timeArr[1];

                var tempDate = even['when'];

                var date = new Date();

                tempDate = tempDate.toString();
                var zz = tempDate.split(" ");
                var zz1 = zz[0].split("-");
                var zz2 = zz[1].split(":");
                var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                var b = zzz3.getTime();


                var whenDateTimeStamp = b;
                var offset = date.getTimezoneOffset();
                offset = offset * 60 * 1000;
                var wNewDate = new Date(whenDateTimeStamp - offset);
                //alert(wNewDate);
                var dateString = wNewDate.getFullYear() + "-" + (wNewDate.getMonth() + 1) + "-" + wNewDate.getDate() + " " + wNewDate.getHours() + ":" + wNewDate.getMinutes() + ":" + wNewDate.getSeconds();
                //alert(dateString);
                var eTime = dhmFilter(dateString);

                if (eTime == '') {
                    eTime = 'Just now';
                }
                //alert(eTime);
                $scope.eIsNow = ((eTime.indexOf("Yesterday") > -1) || (eTime.indexOf("days") > -1) || (eTime.indexOf("-") > -1)) ? false : true;
                even['eTime'] = eTime;
                var eTimeArr = String(even['when']).split(" ");
                eTimeArr = String(eTimeArr[1]).split(":");
                even['eTimeString'] = eTimeArr[0] + ":" + eTimeArr[1];


                even['distance'] = getDist(even['location_lat'], even['location_long'], $rootScope.km_checked);
                if (even.contentUrl == '') {
                    even.content = false;
                } else {
                    even.content = true;
                }
                $scope.op_event = even;
                $rootScope.pageTitle = even['title'];
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.getComments(even['id']);
                }, function (response) {
                    $state.go('main.login', {});
                });
                $scope.getEvent = true;
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };

    $scope.addComment = function (event_id, comment) {
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                event_id: event_id,
                comment: comment.text,
                _token: localStorage.satellizer_token
            }
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/addComment',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                if (response.status == 1) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        $scope.getComments(event_id);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                    $scope.comment = {};
                } else {
                    $rootScope.msgDescription = $filter('translate')('Error_msg7');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.sendMessage = function (message) {
        if (message.text == undefined || message.text == "") {
            //$scope.errorMsg = true;
            return;
        }
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                user_to: $stateParams.from_user,
                message: message.text,
                event_id: $stateParams.event_id,
                annonymous: ($scope.annonymous == 1) ? true : false,
                _token: localStorage.satellizer_token
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/sendMessage',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                $scope.stay_annonymous = false;
                $scope.comment.text = "";
                if (response.status = 1) {
                    $scope.getmessages($stateParams.event_id);
                    return;
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelModel = function () {
        $scope.errorMsg = false;
        $scope.data.message = "";
        $scope.stay_annonymous = false;
    }

    $scope.reportBadContentMsg = function () {
        $('#reportBadContent').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                event_id: $stateParams.event_id,
                subject: $scope.msg_category,
                message: $scope.data.reportMessage
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/reportEvent',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                $scope.data.reportMessage = "";
                if (response.status = 1) {
                    $rootScope.msgDescription = $filter('translate')('Message_send');
                    $("#alertModal").modal('show');
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.cancelReportModel = function () {
        $scope.errorMsg = false;
        $scope.data.reportMessage = "";
        $scope.msg_category = "It\'\s spam";
    }
}]);

LOMCityControllers.controller('manageAdsCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getUserAds();
    }, function (response) {
        $state.go('main.login', {});
    });
    $scope.noAds = false;

    $scope.getUserAds = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        $rootScope.showLoading();
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getAds',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $rootScope.hideLoading();
            if (response.status == 1) {
                var ads = response.content;
                if (ads.length > 0) {
                    for (var i = 0; i < ads.length; i++) {
                        var temp_start_date = response.content[i]['start_date'];
                        var zz = temp_start_date.split(" ");
                        var zz1 = zz[0].split("-");
                        var zz2 = zz[1].split(":");
                        ads[i]['start_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);

                        var temp_end_date = response.content[i]['end_date'];
                        var zz = temp_end_date.split(" ");
                        var zz1 = zz[0].split("-");
                        var zz2 = zz[1].split(":");
                        ads[i]['end_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    }
                    ;
                    $scope.noAds = false;
                    $scope.ads = ads;
                } else {
                    $scope.ads = ads;
                    $scope.noAds = true;
                }
            }
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            //console.log('in error');
            //for testing
            var response = '{"content":[{"id":25,"user_id":4,"title":"The best private investigator in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-21 11:31:24","end_date":"2015-10-28 11:31:24","exposure_center":"New delhi","radius":20,"banner_url":""},{"id":26,"user_id":4,"title":"The best request center in Israel","description":"Click here for more info.","start_date":"2015-10-11 11:31:24","end_date":"2015-10-18 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":5,"banner_url":""},{"id":27,"user_id":4,"title":"The best private help center in Israel","description":"Best prices.","start_date":"2015-10-01 11:31:08","end_date":"2015-10-08 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":10,"banner_url":""},{"id":28,"user_id":4,"title":"The best community in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-22 11:31:30","end_date":"2015-10-29 11:31:24","exposure_center":"Laxmi Nagar New delhi","radius":50,"banner_url":""},{"id":29,"user_id":4,"title":"The best private company in Israel","description":"Best prices! click here for more info.","start_date":"2015-10-21 11:31:24","end_date":"2015-10-28 11:31:24","exposure_center":"Nirman Vihar New delhi","radius":20,"banner_url":""}],"status":1}';
            response = JSON.parse(response);
            var ads = response.content;
            if (ads.length > 0) {
                for (var i = 0; i < ads.length; i++) {
                    var temp_start_date = response.content[i]['start_date'];
                    var zz = temp_start_date.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    ads[i]['start_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);

                    var temp_end_date = response.content[i]['end_date'];
                    var zz = temp_end_date.split(" ");
                    var zz1 = zz[0].split("-");
                    var zz2 = zz[1].split(":");
                    ads[i]['end_date'] = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                }
                ;
                $scope.noAds = false;
                $scope.ads = ads;
            } else {
                $scope.ads = ads;
                $scope.noAds = true;
            }
            //console.log(JSON.stringify($scope.ads));
        });
    }

}]);

LOMCityControllers.controller('createAdsCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken) {
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.banner_url = "";
    $scope.amountValue = 0;
    $scope.Exposure_center = "";
    $scope.myFile = "";

    $scope.Start_date = new Date();
    $scope.End_date = new Date();
    var numberOfDaysToAdd = 7;
    $scope.End_date.setDate($scope.End_date.getDate() + numberOfDaysToAdd);

    $scope.opened = false;
    $scope.opened1 = false;
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }
    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    }

    $scope.createAds = function () {
        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }
        var request = {
            title: $scope.Ad_title,
            description: $scope.Ad_description,
            start_date: $scope.Start_date,
            end_date: $scope.End_date,
            exposure_center: $scope.Exposure_center.formatted_address,
            radius: $scope.Exposure_radius,
            banner_url: $scope.banner_url,
            _token: localStorage.satellizer_token,
            location_lat: $scope.Exposure_center.geometry.location.G,
            location_long: $scope.Exposure_center.geometry.location.K,
            link: $scope.link,
            ad_amount: parseFloat($scope.amountValue)
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/createAds',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.fileUploading = false;
            if (response.status == 1) {
                $rootScope.msgDescription = response.msg;
                /*$("#alertModal").modal('show').on("shown.bs.modal", function () {
                    $timeout(function () {
                        $("#alertModal").modal("hide");
                        $state.go('main.manageAds', {});
                    }, 1500);
                });*/
                if (response.redirect_url != '') {
                    window.location.href = response.redirect_url;
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };

    $scope.uploadFile = function () {
        var file = $scope.myFile;
        var type = 'image';
        var fileName = file.name;
        //console.dir(file);

        var uploadUrl = "https://viround.com/public/api/uploads3";
        //var uploadUrl = $rootScope.baseUrl + "/api/upload";
        //console.log(type + ' ' + file + ' ' + fileName + ' ' + uploadUrl);
        var fd = new FormData();
        fd.append('type', type);
        fd.append('file', file);
        fd.append('fileName', fileName);

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function (res) {
                //console.log(JSON.stringify(res));
                $scope.banner_url = res.content;
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.createAds();
                }, function (response) {
                    $state.go('main.login', {});
                });
            })
            .error(function (res) {
                //console.log(JSON.stringify(res));
                //$scope.createAds();
            });
    };

    $scope.createAdsNuploadFile = function () {
        if ($scope.myFile != '') {
            $scope.fileUploading = true;
            $scope.uploadFile();
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.createAds();
            }, function (response) {
                $state.go('main.login', {});
            });
            $scope.fileUploading = true;
        }
    }

    $scope.getAdAmount = function () {
        var request = {
            location: $scope.Exposure_center,
            radius: $scope.Exposure_radius,
            startDate: $scope.Start_date,
            endDate: $scope.End_date
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getAdAmount',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.amountValue = response.amount;
        }).error(function (xhr, req, err) {
            if (req == 500) {
                //console.log($filter('translate')('Error_msg10'));
            }
        });
    };

    $scope.testMethod = function () {
        var request = {
            payer_id: '34',
            payment_date: 'testdate',
            payment_status: 'Completed',
            txn_id: '443',
            payment_gross: '43',
            item_number: 37
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/updatePaymentStatus',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
        }).error(function (xhr, req, err) {
            if (req == 500) {
                //console.log($filter('translate')('Error_msg10'));
            }
        });
    }

}]);

LOMCityControllers.controller('downloadCtrl', ["$rootScope", "$scope", function ($rootScope, $scope) {
    if ($rootScope.deviceType == "iphone") {
        window.location = "https://itunes.apple.com/in/app/viround/id1105886064?mt=8";

    } else if ($rootScope.deviceType == "android") {
        window.location = "market://details?id=com.viround";
    } else {
        window.location = "https://viround.com";
    }
}]);

LOMCityControllers.controller('profileCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$filter', 'isValidToken', '$interval', function ($rootScope, $scope, $timeout, $state, $http, $filter, isValidToken, $interval) {
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.files = [];
    $scope.fileUploading = false;
    $scope.avatar = '';
    $scope.profile_avatar = '';
    $scope.firstName = '';
    $scope.lastName = '';
    $scope.message = "";
    $scope.user_id = 'none';
    $scope.userEventCount = 0;
    $scope.iFollow = [];
    $scope.ifollowCount = 0;
    $scope.followers = [];
    $scope.followersCount = 0;
    $scope.root = $state;


    var request = {
        _token: localStorage.satellizer_token
    };
    //console.log(JSON.stringify(request));
    $http({
        method: 'POST',
        url: $rootScope.baseUrl + '/api/getProfileInfo',
        data: {data: JSON.stringify(request)}
    }).success(function (response) {
        if (response.avatar == '') {
            $scope.profile_avatar = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
        } else {
            $scope.profile_avatar = response.avatar;
        }
        $scope.user_id = response.id;
        $scope.firstName = response.firstName;
        $scope.lastName = response.lastName;
        $scope.userEventCount = response.userEventCount;
        $scope.iFollow = response.ifolow;
        $scope.ifollowCount = response.ifolow.length;
        $scope.followMe = response.followMe;
        $scope.followMeCount = response.followMe.length;
    }).error(function (xhr, req, err) {
        $scope.message = "error";
    });

    // $scope.getUserInfo = function(){
    //   var request = {
    //       _token: localStorage.satellizer_token
    //   };
    //  //console.log(JSON.stringify(request));
    //   $http({
    //       method: 'POST',
    //       url: $rootScope.baseUrl + '/api/getProfileInfo',
    //       data: { data: JSON.stringify(request) }
    //   }).success(function (response) {
    //     if(response.avatar == ''){
    //       $scope.avatar = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
    //     }else{
    //       $scope.avatar = response.avatar;
    //     }
    //     $scope.firstName = response.firstName;
    //     $scope.lastName = response.lastName;
    //   }).error(function (xhr, req, err) {
    //     $scope.message = "error";
    //   });
    // };

    $scope.uploadAvatar = function () {
        $scope.fileUploading = true;
        var avatar = $scope.files[0];
        var fd = new FormData();
        fd.append('avatar', avatar.file);

        var uploadUrl = $rootScope.baseUrl + "/api/uploadAvatar";

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function (res) {
                $scope.avatar = $rootScope.baseUrl + '/uploads/avatar/' + res.avatar;
                //console.log($scope.avatar);
                $state.go('main.profile', {});
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.changeAvatar();
                }, function (response) {
                    $state.go('main.login', {});
                });
            })
            .error(function (res) {
                //console.log(JSON.stringify(res));
                //$scope.addEvent();
            });
    };

    $scope.changeAvatar = function () {
        var request = {
            avatar: $scope.avatar,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/changeAvatar',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            //console.log( JSON.stringify(response) );
            $scope.fileUploading = false;
            if (response.status == 1) {
                $rootScope.msgDescription = response.msg;
                if (response.msg == 'Avatar was changed successfully.') {
                    $rootScope.msgDescription = $filter('translate')('avatar_changed_successfully');
                }
                //alert(response.msg);
                // $("#alertModal").modal('show').on("shown.bs.modal", function () {
                //     $timeout(function () {
                //         $("#alertModal").modal("hide");
                //     }, 1000);
                // });
                $scope.message = response.msg;
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            //console.log(xhr);
            //console.log(err);
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };
}]);

LOMCityControllers.controller('userCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$stateParams', '$filter', 'isValidToken', '$interval', '$location', '$anchorScroll', function ($rootScope, $scope, $timeout, $state, $http, $stateParams, $filter, isValidToken, $interval, $location, $anchorScroll) {
    $(document.body).removeClass("modal-open");
    $(".modal-backdrop.show").hide()
    var promise = isValidToken.check();
    promise.then(function (token) {
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.profile_avatar = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
    $scope.firstName = '';
    $scope.lastName = '';
    $scope.message = "";
    $scope.user_id = $stateParams.user_id;
    $scope.userEventCount = 0;
    $scope.iFollow = [];
    $scope.ifollowCount = 0;
    $scope.followMe = [];
    $scope.followMeCount = 0;
    $scope.followed = 0;
    $scope.unfollowMessage = '';
    $scope.resp = [];
    $scope.root = $state;
    $scope.userEvents = [];
    //for get content

    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.loading = false;
    $scope.load_events = true;
    $scope.events = [];
    $scope.backToEvent = false;

    $(document).ready(function () {
        $('.profile-avatar').hover(function () {
            $('.upload-avatar').show();
        });

        $('upload-avatar').hover(function () {
            $('.upload-avatar').show();
        });

        $('.upload-avatar').mouseleave(function () {
            $('.upload-avatar').hide();
        });


    });

    $scope.stateReaload = function () {
        $state.reload();
    }

    var request = {
        id: $stateParams.user_id
    };
    $rootScope.showLoading();
    $http({
        method: 'POST',
        url: $rootScope.baseUrl + '/api/getProfileInfo',
        data: {data: JSON.stringify(request)}
    }).success(function (response) {
        //console.log(JSON.stringify(response));
        $scope.resp = response;
        if (response.avatar == '') {
            $scope.profile_avatar = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
        } else {
            var avatar_user = response.avatar;
            if (response.avatar.indexOf('viround.com/public/uploads/avatar') > 0) {
                avatar_user = response.avatar.split('/').pop();
                $scope.profile_avatar = $rootScope.mediaUrlS3 + 'avatar/' + avatar_user;
            } else {
                $scope.profile_avatar = avatar_user;
            }

        }
        $scope.firstName = response.firstName;
        $scope.lastName = response.lastName;
        $scope.userEventCount = response.userEventCount;
        $scope.iFollow = response.ifolow;
        $scope.ifollowCount = response.ifolow.length;
        $scope.followMe = response.followMe;
        $scope.followMeCount = response.followMe.length;
        $scope.statusText = response.status_text;
        $scope.phpv = response.phpv;
    }).error(function (xhr, req, err) {
        $scope.message = "error: " + req;
    });

    // -------------------------------------------------
    //follow USER
    // followed or not
    $scope.isFollowUser = function () {
        var req = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        }

        // console.log(localStorage.satellizer_token);

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isFollow',
            data: {data: JSON.stringify(req)}
        }).success(function (response) {
            $scope.followed = response.status;

        }).error(function (xhr, req, err) {
            $scope.message = "error0: " + req;
        });
    }


    //to follow

    $scope.follow = function () {
        var request = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/follow',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                //$scope.message = 'FOLLOWING!';
                $.notify("FOLLOWING!", "success");
                $state.reload();
            } else {
                //$scope.message = 'you not folowed!';
                $.notify("FOLLOWING!", "warn");
            }
        }).error(function (xhr, req, err) {
            $scope.message = "error1: " + req;
        });
    };

    $scope.unfollow = function () {
        var request = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/unFollow',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                $scope.unfollowMessage = 'Unfolowed!';
            } else {
                $scope.unfollowMessage = 'Not unfolowed!';
            }
        }).error(function (xhr, req, err) {
            $scope.unfollowMessage = "error: " + req;
        });
    };

    var url_to_user = window.location.toString().indexOf('touser');

    if (url_to_user > 0) {
        $rootScope.backToPost = null;
        window.location = window.location.toString().substring(0, url_to_user - 1)
    }

    if ($rootScope.backToPost) {
        $scope.backToEvent = true;
        if ($rootScope.numberOfIvents > 20) {
            $scope.results_num = $rootScope.numberOfIvents;

        }

        if ($rootScope.numberOfIvents > 100) {
            $scope.results_from = 100;
        }
        //alert($scope.results_num);
    } else {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
    }


    $scope.scrollTo = function (postId) {
        // if(eventIsset(postId)){

        $timeout(function () {
            $location.hash(postId);
            $anchorScroll();
        }, 100);
        $rootScope.backToPost = false;
        $scope.backToEvent = false;
        $scope.results_num = 20;
        // }
    };

    $scope.getNumberEvents = function () {
        $rootScope.numberOfIvents = document.querySelectorAll('[ng-repeat="event in events"]').length;
    }

    function eventIsset(id) {
        return document.getElementById(id);
    }


    $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
        if ($rootScope.backToPost) {
            $scope.scrollTo($rootScope.backToPost);
        }
        // $scope.stop = true;
    });

    /*$scope.results_from = 0;
    $scope.results_num = 20;
    $scope.loading = false;
    $scope.load_events = true;
    $scope.events = [];
    $scope.backToEvent = false;*/
    // $scope.stop = false;

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            if ($scope.load_events == true) {
                //console.log('bottom');
                promise = isValidToken.check();
                promise.then(function (token) {
                    //$scope.getContent($scope.SearchVal, $scope.FilterVal);
                    if (!$scope.loading) {
                        $scope.loading = true;
                        // $scope.load_events = false;
                        // $scope.stop = false;
                        $scope.getContent();
                    }
                }, function (response) {
                    $state.go('main.login', {});
                });
            }
        }
    });

    // -------------------------------------------------

    $scope.getContent = function () {
        $scope.loading = true;
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            FilterVal: "myEvents",
            searchTitle: $scope.userEventSearch,
            user_id: $scope.user_id,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };
        //alert(JSON.stringify(request));
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvents',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            //$scope.loading = false;
            $rootScope.hideLoading();
            console.log(response.content);
            if (response.status == 1) {
                var events = response.content;
                if (events.length > 0) {
                    if (events.length < 20) {
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                    }

                    for (var i = 0; i < events.length; i++) {

                        var mediaName = events[i]['mediaThumb'].split('/').pop();
                        events[i]['mediaThumb'] = $rootScope.mediaUrlS3 + mediaName;

                        events[i]['type'] = 0;

                        if (events[i]['mediaTypeIs']) {
                            events[i]['type'] = events[i]['mediaTypeIs'].type;
                        }

                        var created_at_full = events[i]['created_at'].split(' ');
                        var created_at_date = created_at_full[0].split('-');
                        var created_at_new_date = created_at_date[2] + '-' + created_at_date[1] + '-' + created_at_date[0];
                        events[i]['created_at'] = created_at_new_date + ' ' + created_at_full[1];

                        $scope.events.push(events[i]);
                    }
                    ;
                    // alert(JSON.stringify($scope.events))
                    $scope.noEvents = false;
                    $scope.loading = false;
                    //console.log($scope.events.length);
                } else {
                    if ($scope.events.length == 0) {
                        $scope.load_events = false;
                        $scope.events = events;
                        $scope.noEvents = true;
                    }
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            $scope.loading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    // -------------------------------------------------
    //BLOCK USER

    $scope.blockedMessage = "";
    $scope.userIsBlocked = false;
    $scope.guest_id = 0;

    $scope.blockUser = function () {
        var request = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/userBlock',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                $scope.blockedMessage = 'BLOCKED!';
                $.notify($filter('translate')('nfy_blocked'), "success");
            } else {
                $scope.blockedMessage = 'you not folowed!';
                $.notify("BLOCKED!", "warn");
            }
        }).error(function (xhr, req, err) {
            $scope.blockedMessage = "error1: " + req + " - " + err;
        });
    };

    $scope.unBlockUser = function () {
        var request = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/userUnBlock',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            if (response.status == 1) {
                $scope.blockedMessage = '';
                $.notify("UNBLOCKED!", "success");
                $state.reload();
            } else {
                $scope.blockedMessage = '';
                $.notify("NOT UNBLOCKED!", "warn");
            }
        }).error(function (xhr, req, err) {
            $scope.blockedMessage = "";
            $.notify("error: " + req, "error");
        });
    };

    $scope.isBlocked = function () {
        var req = {
            id: $stateParams.user_id,
            _token: localStorage.satellizer_token
        }

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isBlocked',
            data: {data: JSON.stringify(req)}
        }).success(function (response) {
            $scope.guest_id = response.guest_id;
            $scope.guestBlocked = response.guestBlocked;
            $scope.userIsBlocked = response.status;
            //alert($scope.guest_id + " " + $scope.userIsBlocked);
        }).error(function (xhr, req, err) {
            $scope.message = "error: " + req + " | " + err + " | " + xhr;
        });
    };

    // -------------------------------------------------

    $scope.uploadAvatar = function () {
        $scope.fileUploading = true;
        var avatar = $scope.files[0];
        var fd = new FormData();
        fd.append('avatar', avatar.file);

        var uploadUrl = $rootScope.baseUrl + "/api/uploadAvatar";

        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
            .success(function (res) {
                $scope.avatar = $rootScope.mediaUrlS3 + 'avatar/' + res.avatar;
                $state.go('main.user', {});
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.changeAvatar();
                }, function (response) {
                    $state.go('main.login', {});
                });
            })
            .error(function (res) {
                //console.log(JSON.stringify(res));
                //$scope.addEvent();
            });
    };

    $scope.changeAvatar = function () {
        var request = {
            avatar: $scope.avatar,
            _token: localStorage.satellizer_token
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/changeAvatar',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            //console.log( JSON.stringify(response) );
            $scope.fileUploading = false;
            if (response.status == 1) {
                $rootScope.msgDescription = response.msg;
                if (response.msg == 'Avatar was changed successfully.') {
                    $rootScope.msgDescription = $filter('translate')('avatar_changed_successfully');
                    $.notify($rootScope.msgDescription, "success");
                }
                $scope.message = response.msg;
                $('#changeAvatarModal').modal('hide');
                $timeout(function () {
                    $state.go('main.user', {"user_id": $stateParams.user_id});
                    $state.reload();
                }, 1000);
                //$state.reload();

            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $.notify($rootScope.msgDescription, "error");
                // $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            //console.log(xhr);
            //console.log(err);
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });

    };

    $scope.dish_id = '';
    $('#deleteEvent').on('show.bs.modal', function (eve) {
        var button = $(eve.relatedTarget);
        $scope.dish_id = eve.relatedTarget.dataset.eventid;
    });

    $scope.deleteEventMethod = function () {
        //alert('called')
        //console.log($scope.dish_id);
        $('#deleteEvent').modal('hide');
        $rootScope.showLoading();
        //$scope.canceDeletelModel();
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                event_id: $scope.dish_id,
                _token: localStorage.satellizer_token
            };
            //console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/deleteEvent',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                //console.log(JSON.stringify(response));
                $rootScope.hideLoading();
                //$state.go('main.allEvents', {});
                $state.reload();
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    $scope.canceDeletelModel = function () {
        $scope.errorMsg = false;
    }

    $scope.editProfileName = function () {
        $scope.showEditName = false;

        var requestName = {
            _token: localStorage.satellizer_token,
            firstName: $('#newUserFirstName').val(),
            lastName: $('#newUserLastName').val(),
            proc: 'name'
        };

        //console.log(JSON.stringify(requestName));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/editEvent',
            data: {data: JSON.stringify(requestName)}
        }).success(function (response) {
            $.notify(response.content, "success");
            $state.reload();
        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.addLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.isLiked = function (id) {

        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isLiked',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            var liked = response.content;
            if (liked) {
                document.querySelector('.like_' + id + ' span.addLike').hidden = true;
                document.querySelector('.like_' + id + ' span.disLike').hidden = false;
            }

            if (!liked) {
                document.querySelector('.like_' + id + ' span.addLike').hidden = false;
                document.querySelector('.like_' + id + ' span.disLike').hidden = true;
            }

            document.querySelector('.like_' + id + ' span.addLike span.likeCount').innerText = response.count;
            document.querySelector('.like_' + id + ' span.disLike span.likeCount').innerText = response.count;

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.disLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/disLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.statusTextUpdate = function (text) {
        var request = {
            _token: localStorage.satellizer_token,
            text: text,
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/statusTextUpdate',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            //console.log(JSON.stringify(response));
            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $state.reload();

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.closeEdit = function () {
        $scope.showEditStatus = '';
        $scope.showEditName = '';
    }

    $scope.sendMessageToUser = function () {
        var request = {
            _token: localStorage.satellizer_token,
            to_user: $stateParams.user_id,
            message: $scope.messageToUserText
        };

        //console.log(request);

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendMessageToUser',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response));
            $scope.messageToUserText = '';
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.searchByTitle = function () {
        $scope.events = [];
        $scope.getContent();
    }

    $scope.reloadSearch = function () {
        $scope.userEventSearch = '';
        $scope.events = [];
        $scope.getContent();
    }

    $scope.reportBaduser = function () {
        $('#reportBaduser').modal('hide');
        promise = isValidToken.check();
        promise.then(function (token) {
            var request = {
                _token: localStorage.satellizer_token,
                bad_user_id: $stateParams.user_id,
                message: $scope.reportBaduserText
            };
            console.log(JSON.stringify(request));
            $http({
                method: 'POST',
                url: $rootScope.baseUrl + '/api/reportBaduser',
                data: {data: JSON.stringify(request)}
            }).success(function (response) {
                console.log(response);
                if (response.status = 1) {
                    $.notify(response, "success");
                }
            }).error(function (xhr, req, err) {
                if (req == 500) {
                    $rootScope.msgDescription = $filter('translate')('Error_msg10');
                    $("#alertModal").modal('show');
                }
            });
        }, function (response) {
            $state.go('main.login', {});
        });
    }


}]);

LOMCityControllers.controller('editEventCtrl', ["$rootScope", "$scope", "$timeout", "$state", '$http', '$stateParams', 'dhm', 'getDist', "$filter", "$sce", "isValidToken", 'dhmFilter', '$q', function ($rootScope, $scope, $timeout, $state, $http, $stateParams, dhm, getDist, $filter, $sce, isValidToken, dhmFilter, $q) {

    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.openEvent();
    }, function (response) {
        //$state.go('main.login', {});
        $rootScope.loggedIn = false;
        $scope.openEvent();
        $scope.getIdOfCat();
    });

    $scope.getEvent = false;
    $scope.noComments = false;
    $scope.errorMsg = false;
    $scope.stay_annonymous = false;
    $scope.comment = {};
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.eIsNow = false;
    $scope.isLoggedIn = $rootScope.loggedIn;
    $scope.pCategory = null;
    $scope.sCategory = null;

    //from createEvents
    $scope.newEvent = {};
    $scope.result1 = '';
    $scope.options1 = null;
    $scope.details1 = '';
    $scope.address = '';
    $scope.mediaType = '';
    $scope.mediaUrl = '';
    $scope.files = [];
    $scope.galleryFiles = null;
    $scope.fileUploading = false;
    $scope.maxFileSizeMB = 8e+7;
    $scope.required = false;

    $timeout(function () {
        $('#setTimeExample').timepicker({'step': 30});
        $('#setDiscountEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
        $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
        $('#setTimeExample').timepicker('setTime', new Date());
    }, 500);

    $scope.opened = false;
    $scope.date = new Date();
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'dd/MM/yyyy', 'dd-MM-yyyy', 'shortDate'];
    $scope.format = $scope.formats[4];

//calendar позволяет делать много датапикеров в одной форме
    $scope.calendar = {
        opened: {},
        dateFormat: $scope.formats[4],
        dateOptions: {},
        open: function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.calendar.opened[which] = true;
        }
    };

    $scope.getIdOfCat = function () {

        $timeout(function () {
            var scatid = $('#sub-cat option:selected')[0].id - 9;
            $scope.sCategory = $rootScope.getSubCateegoryes[scatid];
        }, 100);

    }

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    }

    $scope.openEvent = function () {
        var request = {
            id: $stateParams.event_id,
            _token: localStorage.satellizer_token,
            proc: 'edit'
        };

        $rootScope.showLoading();

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/editEvent',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(response);
            $rootScope.hideLoading();
            if (response.status == 1) {
                var even = response.content;
                if (even.gallery_files != null) {
                    even.gallery = JSON.parse(even.gallery_files);
                    angular.forEach(even.gallery, function (el) {
                        /*el.filename = $rootScope.baseUrl + '/uploads/' + el.filename;
                      el.thumb = $rootScope.baseUrl + '/uploads/' + el.thumb;*/
                        el.filename = $rootScope.mediaUrlS3 + el.filename;
                        el.thumb = $rootScope.mediaUrlS3 + el.thumb;

                        //console.log(el.thumb);
                    });
                } else {
                    even.gallery = [];
                }

                if (even.contentUrl == '') {
                    even.content = false;
                } else {
                    even.content = true;
                }

                $scope.op_event = even;
                console.log($scope.op_event);
                $scope.setCat(even['parentCategory'], even['category']);
                $scope.oldAddress = even['address'];
                $scope.address = $scope.oldAddress;
                $rootScope.pageTitle = even['title'];
                $scope.newEvent.title = even['title'];
                $scope.newEvent.description = even['description'];
                $scope.newEvent.reward = even['reward'];
                $scope.newEvent.discount = even['discount'];
                $scope.newEvent.email = even['email'];
                $scope.newEvent.phone = even['phone'];

                $scope.date = timeFormatToEdit(even.when, '#setTimeExample');

                if (even['end_event_time']) {

                    $timeout(function () {
                        timeFormatToEditNext(even['end_event_time'], '#endDate', '#setEndTime');
                    }, 1000);
                }

                if (even['end_discount_time']) {
                    $timeout(function () {
                        timeFormatToEditNext(even['end_discount_time'], '#endDiscountDate', '#setDiscountEndTime');
                    }, 1000);
                }

                if ($scope.op_event.category) {
                    $timeout(function () {
                        $scope.selectedOption = $scope.op_event.category;
                        $('#sub-cat').val($scope.op_event.category);
                    }, 100);
                }

                $scope.getIdOfCat();


            } else {
                if (response.status == 0 && response.content == "") {
                    window.history.back();
                    return;
                }
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    };

    function timeFormatToEdit(fullDate, timeId) {
        var fullDate = fullDate.split(" ");
        var isDate = fullDate[0].split("-");
        var isTime = fullDate[1];
        $(timeId).val(isTime);
        //console.log(fullDate);
        return new Date(isDate[0], isDate[1] - 1, isDate[2]);
    }

    function timeFormatToEditNext(fullD, dateId, timeId) {
        var fullDate = fullD.split(" ");
        var isDate = fullDate[0].split("-");
        $(timeId).val(fullDate[1]);
        //console.log("fullDate", isDate[2]+"-"+isDate[1]+"-"+isDate[0]);
        $(dateId).val(isDate[2] + "-" + isDate[1] + "-" + isDate[0]);
    }

    $scope.remove = function () {
        $scope.op_event.gallery.splice(this.$index, 1);
    }

    $scope.setCat = function (cat, subCat) {
        $timeout(function () {
            //alert($rootScope.chooseOptions[0].name);
            // $scope.selectedPOption = $rootScope.parentCategories[0].name;
            $scope.selectedPOption = cat;
            // $scope.selectedOption = $rootScope.chooseOptions[0].name;
            $scope.selectedOption = subCat;
            $('#sub-cat').val(subCat);
            $scope.updateSubCat($scope.selectedPOption);

            $scope.$apply();
        }, 100);
    }

    $scope.inputFiles = [];

    $scope.bigFilesName = [];

    // Second upload
    var secondUpload = new FileUploadWithPreview('mySecondImage', {showDeleteButtonOnImages: true})
    var secondUploadInfoButton = document.querySelector('.upload-info-button--second');

    // Image selected event listener
    window.addEventListener('fileUploadWithPreview:imageSelected', function (e) {
        $scope.bigFilesName = [];
        if (e.detail.uploadId === 'mySecondImage') {
            //console.log(e.detail.selectedFilesCount)
            //console.log(e.detail.cachedFileArray)
            var list = e.detail.cachedFileArray;
            list.forEach(function (item, index, object) {
                if (item.size > $scope.maxFileSizeMB) {
                    //$scope.bigFilesName.push({'name': item.name, 'size':$scope.formatBytes(item.size, 3)});
                }
            });

            $scope.inputFiles = e.detail.cachedFileArray;
        }
    })

    // Image deleted event listener
    window.addEventListener('fileUploadWithPreview:imageDeleted', function (e) {
        $scope.bigFilesName = [];
        if (e.detail.uploadId === 'mySecondImage') {
            //console.log(e.detail.selectedFilesCount);
            //console.log(e.detail.cachedFileArray);
            //console.log(typeof(e.detail.cachedFileArray));
            $scope.files.splice(e.detail.selectedFilesCount, 1);
            //console.log($scope.files);

            var list = e.detail.cachedFileArray;
            list.forEach(function (item, index, object) {
                if (item.size > $scope.maxFileSizeMB) {
                    //$scope.bigFilesName.push({'name': item.name, 'size':$scope.formatBytes(item.size, 3)});
                }
            });

            $scope.inputFiles = e.detail.cachedFileArray;
        }
    });

    $scope.uploadFile = function () {
        var files = $scope.inputFiles;

        files.forEach(function (item, index, object) {
            if (item.size > $scope.maxFileSizeMB) {
                //object.splice(index, 1);
            }
        });

        var img_count = 6 - $scope.op_event.gallery.length;

        files.splice(img_count);

        var fd = new FormData();

        angular.forEach(files, function (el, key) {
            //console.log(el);
            var tempFile = el.type;
            if (tempFile.indexOf("image") != -1) {
                fileType = 'image';
            } else if (tempFile.indexOf("video") != -1) {
                fileType = 'video';
            }
            fd.append('files[]', el);
        });

        $scope.fdN = fd;
        //console.log("SENDED", Object.values(fd));
        var uploadUrl = $rootScope.baseUrl + "/api/uploads3";
        //var uploadUrl = $rootScope.baseUrl + "/api/eventTest";
        //var uploadUrl = $rootScope.baseUrl + "/api/upload";

        const config = {
            onUploadProgress: function (progressEvent) {
                $scope.percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                var procent = $scope.percentCompleted + '%';
                $('.progress-bar').css('width', procent)
                $('.procent').text(procent);
                console.log(procent)
            }
        }

        axios.post(uploadUrl, fd, config)
            .then(function (res) {
                console.log(res);
                $scope.mediaUrl = res.data.content.mediaUrl;
                $scope.mediaType = res.data.content.mediaType;
                $scope.galleryFiles = res.data.content.gallery;
                promise = isValidToken.check();
                promise.then(function (token) {
                    $scope.updateEvent();
                }, function (response) {
                    $state.go('main.login', {});
                });
            })
            .catch(function (err) {
                $.notify("error: " + err, "error");
                console.log(err);
            });


        /*$http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
        }).success(function (res) {
            console.log("UPLOADED",JSON.stringify(res.data));

            $scope.mediaUrl = res.content.mediaUrl;
            $scope.mediaType = res.content.mediaType;
            $scope.galleryFiles = res.content.gallery;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.updateEvent();
            }, function (response) {
                $state.go('main.login', {});
            });
        }).error(function (xhr, req, err) {
            $.notify("error: "+req, "error");
        });*/


    };

    $scope.uploadFile_addEvent = function () {
        if ($scope.inputFiles.length > 0) {
            $scope.fileUploading = true;
            $scope.uploadFile();
        } else {
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.updateEvent();
            }, function (response) {
                $state.go('main.login', {});
            });
            $scope.fileUploading = true;
        }
    }
    $scope.updateSubCat = function (filter) {
        var catid = $('#parent-cat option:selected')[0].id - 1;
        $scope.pCategory = $rootScope.parentCategories[catid];


        $scope.selectedPOption = filter;
        //alert($scope.selectedPOption);
        for (var i = 0; i < $rootScope.chooseOptions.length; i++) {
            if ($rootScope.chooseOptions[i].parentCategory == filter) {
                //$scope.sCategory = $rootScope.chooseOptions[i++];
                $scope.selectedOption = $rootScope.chooseOptions[i].name;
                //$scope.getIdOfCat();
                break;
            }
        }

        //console.log("Here")
        //$scope.$apply();
        $timeout(function () {
            $('#setDiscountEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
            $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
            $scope.updateSubCatdata();
            //document.querySelectorAll('#sub-cat option[value="? string: ?"]')[0].remove();
        }, 500);
        // if($scope.selectedPOption == 'Discounts'){
        //
        // }
    }

    $scope.updateSubCatdata = function () {
        var scatid = $('#sub-cat option:selected')[0].id - 9;
        $scope.sCategory = $rootScope.chooseOptions[scatid];
        console.log('scatid', $scope.sCategory);
    }

    $scope.setEndTime = function () {
        $timeout(function () {
            $('#setEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
        }, 500);
    }

    $scope.setDiscountEndTime = function () {
        $timeout(function () {
            $('#setDiscountEndTime').timepicker({'timeFormat': 'H:i:s', 'step': 30});
        }, 500);
    }

    $scope.formatBytes = function (bytes, decimals) {
        if (bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals <= 0 ? 0 : decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    $scope.updateEvent = function () {

        var temp = $('#setTimeExample').val();
        var zz = temp.split(":");
        $scope.time = new Date();
        $scope.time.setHours(zz[0]);
        $scope.time.setMinutes(zz[1]);
        $scope.time.getMilliseconds(zz[2]);
        //console.log($scope.time);

        var whenDate = $scope.date;
        var whenTime = $scope.time;

        var dateString = whenDate.getFullYear() + '-' +
            ('00' + (whenDate.getMonth() + 1)).slice(-2) + '-' +
            ('00' + whenDate.getDate()).slice(-2) + ' ' +
            ('00' + whenTime.getHours()).slice(-2) + ':' +
            ('00' + whenTime.getMinutes()).slice(-2) + ':' +
            ('00' + whenTime.getSeconds()).slice(-2);
        // alert(dateString);

        var whenStr = whenDate.getFullYear() + "-" + whenDate.getMonth() + 1 + "-" + whenDate.getDate();
        var timeStr = whenTime.getHours() + ":" + whenTime.getMinutes() + ":" + whenTime.getSeconds();
        var when = whenStr + '~' + timeStr;

        if (localStorage.satellizer_token == undefined) {
            $state.go('main.login', {});
            return;
        }


// --------------------------------------------------------
        function dateStrings(dateItem, timeItem) {
            var dd = timeItem.split(":");
            var dtime = new Date();
            dtime.setHours(dd[0]);
            dtime.setMinutes(dd[1]);
            dtime.getMilliseconds(dd[2]);

            var dDateInput = dateItem.split('-');
            var dDateString = dDateInput[2] + "-" + dDateInput[1] + "-" + dDateInput[0];
            var dDate = new Date(dDateString);
            var dTime = dtime;

            var dString = dDate.getFullYear() + '-' +
                ('00' + (dDate.getMonth() + 1)).slice(-2) + '-' +
                ('00' + dDate.getDate()).slice(-2) + ' ' +
                ('00' + dTime.getHours()).slice(-2) + ':' +
                ('00' + dTime.getMinutes()).slice(-2) + ':' +
                ('00' + dTime.getSeconds()).slice(-2);

            return dString;
        }

        var endDate = $('#endDate').val();
        var endTemp = $('#setEndTime').val();
        var endDateString = null;
        if (endDate != null & endTemp != null) {
            var endDateString = dateStrings(endDate, endTemp);
        }

// --------------------------------------------------------

        var discountEndDate = $('#endDiscountDate').val();
        var disEndTemp = $('#setDiscountEndTime').val();
        var discountEndDateString = null;
        if (discountEndDate != null & disEndTemp != null) {
            var discountEndDateString = dateStrings(discountEndDate, disEndTemp);
        }


        if ($scope.newEvent.reward == undefined) {
            $scope.newEvent.reward = '';
        }

        var updateRequest = {
            id: $stateParams.event_id,
            title: $scope.newEvent.title,
            description: $scope.newEvent.description,
            reward: $scope.newEvent.reward,
            when: dateString,
        };

        //console.log('adress', $scope.address," ||| "+$scope.oldAddress);

        if ($scope.address !== $scope.oldAddress) {
            //console.log('ne ravni')
            var coord = $scope.address.geometry.location;
            var formatted_address = $scope.address.formatted_address;

            var loc = $.map(coord, function (value, index) {
                return [value];
            });

            updateRequest = {
                id: $stateParams.event_id,
                title: $scope.newEvent.title,
                description: $scope.newEvent.description,
                reward: $scope.newEvent.reward,
                when: dateString,
                endEventTime: endDateString,
                endDiscountTime: discountEndDateString,
                discount: $('#discount').val(),
                address: formatted_address,
                lat: coord.lat(),
                long: coord.lng(),
                location: $scope.address,
                _token: localStorage.satellizer_token,
                category: $scope.selectedOption,
                parentCategory: $scope.selectedPOption,
                mediaType: 'image',
                mediaUrl: $scope.mediaUrl,
                galleryFiles: $scope.galleryFiles,
                oldgallery: $scope.op_event.gallery,
                email: $scope.newEvent.email,
                phone: $scope.newEvent.phone,
                proc: 'save'
            };
        } else {
            updateRequest = {
                id: $stateParams.event_id,
                title: $scope.newEvent.title,
                description: $scope.newEvent.description,
                reward: $scope.newEvent.reward,
                when: dateString,
                endEventTime: endDateString,
                endDiscountTime: discountEndDateString,
                discount: $('#discount').val(),
                _token: localStorage.satellizer_token,
                category: $scope.selectedOption,
                parentCategory: $scope.selectedPOption,
                mediaType: 'image',
                mediaUrl: $scope.mediaUrl,
                galleryFiles: $scope.galleryFiles,
                oldgallery: $scope.op_event.gallery,
                email: $scope.newEvent.email,
                phone: $scope.newEvent.phone,
                proc: 'save'
            };
        }

        console.log('EDIT EVENT', JSON.stringify(updateRequest));

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/editEvent',
            data: {data: JSON.stringify(updateRequest)}
        }).success(function (response) {
            //console.log( "response");
            console.log(JSON.stringify(response.dd));
            $scope.fileUploading = false;
            if (response.status == 1) {
                $scope.newEvent = {};
                $scope.date = new Date();
                $scope.address = '';
                $scope.selectedOption = 'Stolen';
                $scope.result1 = '';
                $scope.mediaType = '';
                $scope.mediaUrl = '';
                $.notify($filter('translate')('nfy_event_updated'), "success");
                $timeout(function () {
                    $state.go('main.user', {"user_id": response.user_id});
                }, 1000);

            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
        }).error(function (xhr, req, err) {
            //console.log(xhr);
            //console.log(err);
            $scope.fileUploading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.timeToTime = function (goodClass, badClass) {
        //console.log("timeToTime");
        var goodTime = $('#' + goodClass).val();
        $('#' + badClass).val(goodTime);
    }

    $scope.getDateData = function (inputData) {
        //console.log("getDateData");
        $scope.endEvData = inputData;
    }

}]);

LOMCityControllers.controller('myFavorEventsCtrl', ["$rootScope", "$scope", "$state", '$http', 'dhm', 'getDist', "$filter", "$sce", "$cookies", '$stateParams', 'isValidToken', 'dhmFilter', 'getLocationDistance', '$timeout', '$interval', '$location', '$anchorScroll', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams, isValidToken, dhmFilter, getLocationDistance, $timeout, $interval, $location, $anchorScroll) {

    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getFavorLocations();
        $scope.getContent();
        $scope.initGetAroundEvents();


        var isCatOptReady = $interval(function () {
            if ($rootScope.optionsReady) {
                if ($state.current.name == "main.myFavEvents") {
                    $scope.selectedPOption = $rootScope.selectedPOption;
                    $scope.changePCat();
                    $scope.myFavLat = $scope.FavLocationsList[0].location_lat;
                    $scope.myFavLong = $scope.FavLocationsList[0].location_long;
                    $scope.myFavName = $scope.FavLocationsList[0].name;
                }
                $scope.getPreferredLocations();
                $interval.cancel(isCatOptReady);
            }
        }, 100);
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.results_from = 0;
    $scope.results_num = 20;
    $scope.load_events = false;
    $scope.events = [];
    $scope.isNow = false;
    $scope.showtime = false;
    $scope.SearchVal = false;
    $scope.FilterVal = false;
    $scope.locationSelected = false;
    $scope.loading = false;
    $scope.favLocLength = 0;
    $scope.isLocationEnabled = false;
    $scope.searchByLocation = false;
    $scope.selectedOption = "";
    $scope.selectedPOption = "";
    $scope.newChildCategories = [];
    var index = 0;


    $scope.showEvents = localStorage.getItem('showEvents');
    if ($scope.showEvents == 'list') {
        $scope.showListEvents = true;
        $scope.showMapEvents = false;
    } else if ($scope.showEvents == 'map') {
        $scope.showListEvents = false;
        $scope.showMapEvents = true;
    } else {
        $scope.showListEvents = true;
        $scope.showMapEvents = false;
    }


    $scope.catagorySelected = function (index) {
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $rootScope.chooseOptions[index].name;
            var pCat = $rootScope.chooseOptions[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;
        } else {
            $scope.selectedOption = "";
        }
        $scope.search();

    }
    $scope.catagoryFilteredSelected = function (index) {
        //alert("catagoryFilteredSelected "+index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $("#btn" + index).addClass('button-active');
        if (index != "all") {
            var cat = $scope.newChildCategories[index].name;
            var pCat = $scope.newChildCategories[index].parentCategory;
            var catVal = $filter('translate')(cat);
            $scope.selectedOption = cat;
            $scope.selectedPOption = pCat;
        } else {
            $scope.selectedOption = "";
        }
        $scope.search();

    }
    $scope.catagoryPSelected = function (index) {
        //console.log("catagoryPSelected " + index);
        $scope.results_from = 0;
        $(".catBtn").removeClass('button-active');
        $(".catPBtn").removeClass('button-dark');
        $("#btnp" + index).addClass('button-dark');
        $("#btnall").addClass('button-dark');
        if (index != "all") {
            var cat = $rootScope.parentCategories[index].name;
            var catVal = $filter('translate')(cat);
            $scope.selectedPOption = cat;
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
            for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                if (cat == $rootScope.chooseOptions[j].parentCategory) {
                    $scope.newChildCategories.push($rootScope.chooseOptions[j])
                } else {
                }
            }
        } else {
            $scope.selectedPOption = "";
            $scope.selectedOption = "";
            $scope.newChildCategories = [];
        }
        $scope.search();

        $timeout(function () {
            if (document.querySelector('i[class="fa fa-folder-open fa-lg"]').innerText == '') {
                document.querySelector('i[class="fa fa-folder-open fa-lg"]').innerHTML += $filter('translate')('all_sub_categories');
            }
        }, 10);
    }
    $scope.catagoryChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catBtn").removeClass('button-active');
            $("#btn" + index).addClass('button-active');
            if (index != "all") {
                var cat = $rootScope.chooseOptions[index].name;
                var catVal = $filter('translate')(cat);
                $scope.selectedOption = cat;
            } else {
                $scope.selectedOption = "";
            }
        }, 1000);
        //$scope.search();
    }
    $scope.changeCat = function () {
        //alert($rootScope.chooseOptions);
        if ($scope.newChildCategories.length == 0) {
            for (var i = $rootScope.chooseOptions.length - 1; i >= 0; i--) {
                if ($scope.selectedOption == $rootScope.chooseOptions[i].name) {
                    $scope.catagoryChangeSelected(i)
                }
            }
        } else {
            for (var l = $scope.newChildCategories.length - 1; l >= 0; l--) {
                if ($scope.selectedOption == $scope.newChildCategories[l].name) {
                    //alert("l"+l)
                    $scope.catagoryChangeSelected(l)
                }
            }
        }
    }
    $scope.catagoryPChangeSelected = function (index) {
        //alert(index);
        $timeout(function () {
            $(".catPBtn").removeClass('button-dark');
            $("#btnp" + index).addClass('button-dark');
            if (index != "all") {
                var cat = $rootScope.parentCategories[index].name;
                //var catVal = $filter('translate')(cat);
                $scope.selectedPOption = cat;
            } else {
                $scope.selectedPOption = "";
            }
        }, 1000);
    }
    $scope.changePCat = function () {
        //return;
        for (var i = $rootScope.parentCategories.length - 1; i >= 0; i--) {
            //console.log($scope.selectedPOption + "    :     " + $rootScope.chooseOptions[i].name)
            if ($scope.selectedPOption == $rootScope.parentCategories[i].name) {

                //$scope.catagoryPSelected(i);
                //$scope.catagoryPChangeSelected(i)
                $scope.results_from = 0;
                $(".catBtn").removeClass('button-active');
                $(".catPBtn").removeClass('button-dark');
                $("#btnp" + i).addClass('button-dark');
                $("#btnall").addClass('button-dark');
                if (i != "all") {
                    var cat = $rootScope.parentCategories[i].name;
                    var catVal = $filter('translate')(cat);
                    $scope.selectedPOption = cat;
                    //$scope.selectedOption = "";
                    $scope.newChildCategories = [];

                    for (var j = 0; j < $rootScope.chooseOptions.length; j++) {
                        if (cat == $rootScope.chooseOptions[j].parentCategory) {
                            $scope.newChildCategories.push($rootScope.chooseOptions[j])
                        } else {
                        }
                    }

                } else {
                    $scope.selectedPOption = "";
                    $scope.selectedOption = "";
                    $scope.newChildCategories = [];
                }
                $scope.catagoryPChangeSelected(i);

            }
        }
    }

    $scope.searchKeyPress = function (e) {
        e = e || window.event;
        if (e.keyCode == 13) {
            if ($scope.locationSelected) {
                $scope.search();
            }
        }
    }

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.switchView = function (event) {
        if (event.target.classList.contains('active')) {
        } else {
            $scope.showListEvents = !$scope.showListEvents;
            $scope.showMapEvents = !$scope.showMapEvents;
            if ($scope.showListEvents) {
                localStorage.setItem('showEvents', 'list');
            } else {
                localStorage.setItem('showEvents', 'map');
            }
            $scope.getContent($scope.SearchVal, $scope.FilterVal);
            $scope.initGetAroundEvents();
        }
    }

    $(window).scroll(function () {
        if (($(window).scrollTop() + $(window).height()) == ($(document).height())) {
            if (($state.includes('main.myFavEvents') || $state.includes('main.myFavEventsFiltered'))) {
                if ($scope.load_events) {
                    promise = isValidToken.check();
                    promise.then(function (token) {
                        if (!$scope.loading && $scope.load_events) {
                            $scope.loading = true;
                            $scope.load_events = false;
                            $scope.getContent();
                            //$scope.search();
                        }
                        //$scope.getContent(false,false);
                    }, function (response) {
                        $state.go('main.login', {});
                    });
                }
            }
        }
    });

    $scope.searchModel = {};
    $scope.checkLocFromStorage = false;

    $scope.searchModel.FilterVal = "Location";
    $scope.isLocation = true;


    $scope.search = function () {

        $scope.results_from = 0;
        $scope.load_events = false;
        var paramObj = {param1: "", param2: "", param3: "", param4: ""};
        if ($scope.FavLocationsList) {
            var index = $('#favLocation').val();

            console.log($scope.FavLocationsList[index]);

            $scope.myFavLat = $scope.FavLocationsList[index].location_lat;
            $scope.myFavLong = $scope.FavLocationsList[index].location_long;
            $scope.myFavName = $scope.FavLocationsList[index].name;

            paramObj = {
                param1: $scope.myFavName,
                param2: index,
                param3: $scope.selectedOption,
                param4: $scope.selectedPOption
            }
            /*                localStorage.setItem('SearchVal', JSON.stringify(locations));
                $state.go('main.myFavEventsFiltered', paramObj, { reload: true });
                $scope.searchByLocation = true;
                $scope.locationSelected = false;*/
            $scope.events = [];
            $scope.getContent();
            $scope.initGetAroundEvents();
        }
    }

    $scope.getGeolocation = function () {
        $scope.loading = true;
        $scope.topMap();
        if ($rootScope.myLat != 0 && $rootScope.myLong != 0 && $rootScope.myLat != undefined && $rootScope.myLong != undefined) {
            //alert('if');
            $scope.isLocationEnabled = true;
            promise = isValidToken.check();
            promise.then(function (token) {
                $scope.searchByLocation = false;
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });

        } else if (navigator.geolocation) {
            //alert('navigator');
            var geo_options = {
                timeout: 27000
            };
            navigator.geolocation.getCurrentPosition(myLoc, locationError);
        } else {
            //alert('else');
            $scope.isLocationEnabled = false;
            $rootScope.msgDescription = $filter('translate')('Error_msg9');
            $("#alertModal").modal('show');
            $rootScope.myLat = 0;
            $rootScope.myLong = 0;
            promise = isValidToken.check();
            promise.then(function (token) {
                //$scope.getContent($scope.SearchVal, $scope.FilterVal);
                $scope.getContent(false, false);
            }, function (response) {
                $state.go('main.login', {});
            });
        }
    }

    function myLoc(position) {
        $scope.isLocationEnabled = true;
        $rootScope.myLat = position.coords.latitude;
        $rootScope.myLong = position.coords.longitude;
        //alert($rootScope.myLat+''+$rootScope.myLong);
        promise = isValidToken.check();
        promise.then(function (token) {
            $scope.getContent(false, false);
        }, function (response) {
            $state.go('main.login', {});
        });
    }

    function locationError(error) {
        var errors = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        $scope.getContent(false, false);
        $scope.isLocationEnabled = false;
    }


    $scope.getNumberEvents = function () {
        $rootScope.numberOfIvents = document.querySelectorAll('[ng-repeat="event in events"]').length;
        //$rootScope.numberOfIvents = $scope.events.length;
    }

    if ($rootScope.backToPost) {
        if ($rootScope.numberOfIvents > 20) {
            $scope.results_num = $rootScope.numberOfIvents;
        }

        if ($rootScope.numberOfIvents > 100) {
            $scope.results_from = 100;
        }
        //alert($scope.results_num);
    }


    $scope.scrollTo = function (postId) {
        $timeout(function () {
            if ($rootScope.backToPost) {
                $location.hash(postId);
                $anchorScroll();
            }
            $rootScope.backToPost = null;
            $scope.results_num = 20;
        }, 100);
    };


    $scope.getContent = function () {
        $scope.loading = true;

        geocoder = new google.maps.Geocoder();
        if ($rootScope.range == undefined) { // will be deleted in the end
            $rootScope.range = 0;
        }
        $rootScope.showLoading();
        var request = {
            range: $rootScope.range,
            lat: $scope.myFavLat,
            long: $scope.myFavLong,
            /*            lat:($stateParams.param2 != undefined) ? $scope.FavLocationsList[$stateParams.param2].location_lat : $scope.myFavLat,
            long: ($stateParams.param2 != undefined) ? $scope.FavLocationsList[$stateParams.param2].location_long : $scope.myFavLong,*/
            SearchVal: '',
            FilterVal: '',
            _token: localStorage.satellizer_token,
            category: ($stateParams.param3 != undefined) ? $stateParams.param3 : $scope.selectedOption,
            parentCategory: ($stateParams.param4 != undefined) ? $stateParams.param4 : $scope.selectedPOption,
            results_from: $scope.results_from,
            results_num: $scope.results_num
        };

        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getEvents',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            $scope.loading = false;
            console.log($scope.events);
            $scope.flat = response.flat;
            $scope.flong = response.flong;

            if (document.querySelector('.parCat.active-cat')) {
                document.querySelector('.parCat.active-cat').classList.remove('active-cat');
            }

            if (document.querySelector('.chilCat.active-cat')) {
                document.querySelector('.chilCat.active-cat').classList.remove('active-cat');
            }

            if (request.parentCategory) {
                //document.querySelector('.'+request.parentCategory).style.backgroundColor = 'red';
                document.querySelector('.parCat.' + request.parentCategory).classList.add('active-cat');
            }

            if (request.category) {
                //document.querySelector('.'+request.category).style.backgroundColor = 'red';
                document.querySelector('.chilCat.' + request.category).classList.add('active-cat');
            }

            if (response.status == 1) {
                var events = response.content;

                var sorted = events.sort((a, b) => (b.created_at > a.created_at) ? 1 : -1);
                console.log(sorted);
                console.log(events);
                if (events.length > 0) {
                    if (events.length < 20) {
                        $scope.load_events = false;
                    } else {
                        $scope.results_from += 20;
                        $scope.load_events = true;
                        //$scope.resultsFromEvent = $scope.results_from;
                    }
                    var geocoder = new google.maps.Geocoder();
                    for (var i = 0; i < events.length; i++) {

                        //var time = dhmFilter(response.content[i]['created_at']);

                        if (events[i]['mediaThumb']) {

                            var mediaName = events[i]['mediaThumb'].split('/').pop();
                            events[i]['mediaThumb'] = $rootScope.mediaUrlS3 + mediaName;
                        }

                        if (events[i]['avatar'] == '') {

                            events[i]['profile_avatar'] = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6ACJqK9cgTk8gBj6JO72lhf2Ptf15aEW2cIvDkj0SNwbSfG0M';
                        } else {

                            events[i]['profile_avatar'] = events[i]['avatar'];

                            //var avatarName = events[i]['avatar'].split('/').pop();
                            //events[i]['profile_avatar'] = $rootScope.mediaUrlS3+'avatar/'+avatarName;
                            //events[i]['profile_avatar'] = events[i]['avatar'];
                        }

                        var created_at_full = events[i]['created_at'].split(' ');
                        var created_at_date = created_at_full[0].split('-');
                        var created_at_new_date = created_at_date[2] + '-' + created_at_date[1] + '-' + created_at_date[0];
                        events[i]['created_at'] = created_at_new_date + ' ' + created_at_full[1];

                        if ($scope.locSearchVal) {
                            //console.log(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked,$scope.locSearchVal.lat,$scope.locSearchVal.long);
                            events[i]['distance'] = getLocationDistance(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked, null, $scope.locSearchVal.lat, $scope.locSearchVal.long);
                        } else {
                            events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        }
                        //events[i]['distance'] = getDist(response.content[i]['location_lat'], response.content[i]['location_long'], $rootScope.km_checked);
                        if (events[i]['ad'] != undefined && events[i]['ad'] != "") {
                            var temp_start_date = events[i].ad.start_date;
                            var zz = temp_start_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.start_date = zzz3.getTime();

                            var temp_end_date = events[i].ad.end_date;
                            var zz = temp_end_date.split(" ");
                            var zz1 = zz[0].split("-");
                            var zz2 = zz[1].split(":");
                            var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                            events[i].ad.end_date = zzz3.getTime();
                        }

                        events[i]['type'] = 0;

                        if (events[i]['mediaTypeIs']) {
                            events[i]['type'] = events[i]['mediaTypeIs'].type;
                        }

                        $scope.events.push(events[i]);

                        //if($scope.showMapEvents){
                        // $scope.getMapEvents($scope.events);
                        //$scope.getAroundEvents(response.content[0]['location_lat'], response.content[0]['location_long']);
                        $rootScope.latForMap = response.content[0]['location_lat'];
                        $rootScope.longForMap = response.content[0]['location_long'];
                        $scope.$apply();
                        //}

                    }
                    ;
                    $scope.noEvents = false;
                    $scope.loading = false;
                    //console.log($scope.events.length);
                } else {
                    //console.log($scope.events.length);
                    if ($scope.events.length == 0) {
                        $scope.load_events = false;
                        $scope.events = events;
                        $scope.noEvents = true;
                    }
                }
            } else {
                $rootScope.msgDescription = $filter('translate')('Error_msg7');
                $("#alertModal").modal('show');
            }
            $scope.$broadcast('scroll.refreshComplete');
        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            $scope.loading = false;
            if (req == 500) {
                $rootScope.msgDescription = $filter('translate')('Error_msg10');
                $("#alertModal").modal('show');
            }
        });
    }

    $scope.getMapEvents = function (events) {
        var map = document.getElementById("allEventsMap");
        if (map) {
            $scope.Markers = events;
            $scope.MapOptions = {
                center: new google.maps.LatLng($rootScope.myLat, $rootScope.myLong),
                /* zoom: 15 - $rootScope.userInfo.notifyRadius,*/
                zoom: 16 - $rootScope.userInfo.notifyRadius,
                //zoom: 27 - $rootScope.userInfo.notifyRadius,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            $scope.Latlngbounds = new google.maps.LatLngBounds();
            $scope.Map = new google.maps.Map(document.getElementById("allEventsMap"), $scope.MapOptions);

            //Looping through the Array and adding Markers.
            for (var i = 0; i < $scope.Markers.length; i++) {
                var data = $scope.Markers[i];
                var myLatlng = new google.maps.LatLng(data.location_lat, data.location_long);

                //Initializing the Marker object.
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: $scope.Map,
                    icon: "img/map_icon.png",
                    title: data.title
                });

                //Adding InfoWindow to the Marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent('<div style = "max-width:300px;min-height:50px; text-align: center;"><a href="/#/main/event/' + data.id + '" target="_blank"><b>' + data.title + '</b></a><p>' + data.address + '</p><p><img src="' + data.mediaThumb + '" style="width:50%; height: 150px;"></p></div>');
                        $scope.InfoWindow.open($scope.Map, marker);
                    });
                })(marker, data);

                //Plotting the Marker on the Map.
                $scope.Latlngbounds.extend(marker.position);
            }

            //Adjusting the Map for best display.
            $scope.Map.setCenter($scope.Latlngbounds.getCenter());
            $scope.Map.fitBounds($scope.Latlngbounds);
        }

    }
    $scope.getPreferredLocations = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getSettings ',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            //console.log(JSON.stringify(response));
            $scope.preferredLocations = response.locations;

            $scope.favLocLength = 0;
            for (var i = $scope.preferredLocations.length - 1; i >= 0; i--) {
                if ($scope.preferredLocations[i].name != "") {
                    $scope.favLocLength++;
                }
            }

            $rootScope.range = response.settings[0].notifyRadius;
            $scope.getGeolocation();
            if (response.settings[0].notifyUnits == 'km') {
                $rootScope.km_checked = true;
                localStorage.setItem('km_checked', 'true');
            } else {
                $rootScope.km_checked = false;
                localStorage.setItem('km_checked', 'false');
            }
        }).error(function (xhr, req, err) {
            //console.log(JSON.stringify(req));
        });
    }

    $scope.changeLocationState = function () {
        if ($scope.favLocLength > 0) {
            $state.go('main.myFavEvents', {});
        } else {
            $state.go('main.settings', {});
        }
    }

    $scope.chnageInputAttribute = function () {
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
            $scope.locationSelected = false;
        } else {
            $scope.locationSelected = true;
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

    $scope.newInputAttribute = function (val) {
        $scope.searchModel.FilterVal = val;
        if ($scope.searchModel.FilterVal == 'Location') {
            $scope.isLocation = true;
            $scope.locationSelected = false;
        } else {
            $scope.locationSelected = true;
            $scope.isLocation = false;
            $scope.searchModel.SearchLocationVal = '';
        }
    }

    $scope.$on('g-places-autocomplete:select', function (event, param) {
        if ($scope.isLocation) {
            $scope.locationSelected = true;
            $scope.checkLocFromStorage = false;
        }
        ;
    });

    $scope.mapLoading = false;

    $scope.getAroundEvents = function (lat, lng) {
        $scope.mapLoading = true;
        //console.log(lat +" "+ lng);
        var reqt = {
            lat: lat,
            lng: lng,
            /*          lat: $scope.myFavLat,
          lng: $scope.myFavLong,*/
            category: $scope.selectedOption,
            parentCategory: $scope.selectedPOption,
            radiusKm: $rootScope.userInfo.notifyRadius,
            _token: localStorage.satellizer_token,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/aroundEvents',
            data: {data: JSON.stringify(reqt)}
        }).success(function (response) {
            $scope.aEvents = response.aroundEvents;
            // var markersData = $scope.aEvents;
            let respon = $scope.aEvents;

            let positions = ['0-0']
            let multipleEvents = []
            let oneEventPoints = []

            respon.map(item => {
                positions.map(pos => {
                    if (pos === item.location_lat + "-" + item.location_long)
                        item.unique = false
                    else item.unique = true
                })
                positions.push(item.location_lat + "-" + item.location_long)
            })

            respon.map(item => {
                if (item.unique) {
                    let count = 0
                    positions.map(pos => {
                        if (pos === item.location_lat + "-" + item.location_long)
                            count++
                    })
                    if (count > 1)
                        item.unique = false
                }
            })

            respon.map(item => {
                if (item.unique) {
                    oneEventPoints.push(item)
                } else {
                    let pushed = false
                    multipleEvents.map(mult => {
                        if (mult.lat + "-" + mult.long === item.location_lat + "-" + item.location_long) {
                            mult.count++
                            mult.events.push(item)
                            pushed = true
                        }
                    })
                    if (!pushed)
                        multipleEvents.push({
                            lat: item.location_lat,
                            long: item.location_long,
                            count: 1,
                            events: [item]
                        })
                }
            })


            //work many markers on map
            var map, infoWindow;
            var centerLatLng = new google.maps.LatLng(lat, lng);
            $scope.centerMarker = centerLatLng;
            var mapOptions = {
                center: centerLatLng,
                zoom: 16 - $rootScope.userInfo.notifyRadius
                //zoom: 27 - $rootScope.userInfo.notifyRadius,
            };
            map = new google.maps.Map(document.getElementById("map"), mapOptions);

            infoWindow = new google.maps.InfoWindow();

            google.maps.event.addListener(map, "click", function () {
                infoWindow.close();
            });

            oneEventPoints.forEach(function (item, i, arr) {
                var latLng = new google.maps.LatLng(item.location_lat, item.location_long);
                var iconUrl = "img/map_icon.png";
                if (lng === item.location_long) {
                    iconUrl = "img/map_icon_current.png";
                }
                var name = item.title;
                var address = item.address;
                var id = item.id;
                var parentCategory = $filter('translate')(item.parentCategory);
                var category = $filter('translate')(item.category);
                var mediaThumb;
                //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                if (!item.mediaThumb) {
                    mediaThumb = "img/blog-single-4.jpg";
                } else if (item.mediaThumb.indexOf('http')) {
                    mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                } else {
                    mediaThumb = item.mediaThumb;
                }
                // Добавляем маркер с информационным окном
                addMarker(latLng, name, address, mediaThumb, id, iconUrl, parentCategory, category);
            });

            multipleEvents.forEach(function (item, i, arr) {
                var latLng = new google.maps.LatLng(item.events[0].location_lat, item.events[0].location_long);
                var iconUrl = "img/map_icon_many.png";
                if (lng === item.events[0].location_long) {
                    //iconUrl = "img/map_icon_current_many.png";
                    iconUrl = "img/map_icon_current.png";
                }
                addMarkers(item.events, latLng, iconUrl);
            });


            google.maps.event.addDomListener(window, "load");

            // Функция добавления маркера с информационным окном


            function addMarker(latLng, name, address, mediaThumb, id, iconUrl, parentCategory, category) {
                // var iconUrl = "img/map_icon.png";

                var marker = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: name
                });

                // Отслеживаем клик по нашему маркеру
                google.maps.event.addListener(marker, "click", function () {
                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="infowindow">' +
                        '<h4> <a href="#/main/event/' + id + '" target="_blank">' + name + '</a></h4>' +
                        '<p>' + address + '</p>' +
                        '<p>' + parentCategory + ' / ' + category + '</p>' +
                        '<a href="#/main/event/' + id + '" target="_blank"><img src="' + mediaThumb + '">' + '</a></div>';
                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, marker);
                });
            }

            //
            // много маркеров на одном пине
            function addMarkers(array, latLng, iconUrl) {
                // var iconUrl = "img/map_icon_many.png";
                var address = array[0].address;
                var firstThumb = array[0].mediaThumb;
                var eventMarkerItems = array.length.toString() + " Events";

                var markers = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: iconUrl,
                    title: eventMarkerItems
                });

                google.maps.event.addListener(markers, "click", function () {
                    // contentString - это переменная в которой хранится содержимое информационного окна.
                    var contentString = '<div class="manyItemsWindow"><h4>' + eventMarkerItems + '</h4><p>' + address + '</p>';
                    contentString += '<div style="width: 80%;"><ul>';
                    array.forEach(function (item, i, arr) {
                        var mediaThumb;
                        //var imgUrlServer = "https://dg4x38lr4ktft.cloudfront.net/";
                        if (!item.mediaThumb) {
                            mediaThumb = "img/blog-single-4.jpg";
                        } else if (item.mediaThumb.indexOf('http')) {
                            mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                        } else {
                            mediaThumb = item.mediaThumb;
                        }

                        contentString +=
                            '<li>' +
                            '<i class="fas fa-burn"></i> ' +
                            '<a href="#/main/event/' + item.id + '" title="' + item.title + '">' + item.title.substr(0, 55) + '</a>' +
                            '<div class="manyItWinHyde"><img src="' + mediaThumb + '"></div>' +
                            '</li>';
                    });
                    contentString += '</ul></div>';
                    contentString += '<div style="width: 20%;float: right;position: absolute;right: -10px; top: 20px; margin: 10px;"></div>';
                    contentString += '</div>';

                    // Меняем содержимое информационного окна
                    infoWindow.setContent(contentString);
                    // Показываем информационное окно
                    infoWindow.open(map, markers);
                });

                $scope.mapLoading = false;

            }

        }).error(function (xhr, req, err) {
            //console.log(err);
        });


    }


    $scope.initGetAroundEvents = function () {
        $timeout(function () {
            //console.log($rootScope.latForMap,$rootScope.longForMap );
            $scope.getAroundEvents($scope.flat, $scope.flong);
        }, 1000);
    }


    $scope.addLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }

    $scope.isLiked = function (id) {

        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/isLiked',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            document.querySelector('.like_' + id + ' span.addLike').hidden = false;
            document.querySelector('.like_' + id + ' span.disLike').hidden = false;
            var liked = response.content;
            if (liked) {
                document.querySelector('.like_' + id + ' span.addLike').hidden = true;
            }

            if (!liked) {
                document.querySelector('.like_' + id + ' span.disLike').hidden = true;
            }

            document.querySelector('.like_' + id + ' span.likeCount').innerText = response.count;

        }).error(function (xhr, req, err) {
            //$.notify("error: "+req, "error");
        });
    }

    $scope.disLike = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            event_id: id,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/disLike',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {

            if (response.content == 'OK') {
                $.notify(response.content, "success");
            }

            if (response.content == 'NO') {
                $.notify(response.content, "error");
            }

            $scope.isLiked(id);

        }).error(function (xhr, req, err) {
            $.notify("error: " + req, "error");
        });
    }


    /* --------------------------------------------------------------------------------------------------------------- */


    $scope.getFavorLocations = function () {
        var request = {
            _token: localStorage.satellizer_token,
        };

        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getFavorLocations',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $rootScope.hideLoading();
            console.log(JSON.stringify(response));


            $scope.FavLocationsList = response.content;

        }).error(function (xhr, req, err) {
            $rootScope.hideLoading();
            if (req == 500) {
                $.notify("error: " + req, "error");
            }
        });
    }

    $scope.topMap = function () {
        var map, infoWindow;
        var lat = $rootScope.userPositionLat;
        var long = $rootScope.userPositionLong;

        var centerLatLng = new google.maps.LatLng(lat, long);

        var mapOptions = {
            center: centerLatLng,
            zoom: 14
        };

        map = new google.maps.Map(document.getElementById("top-map"), mapOptions);

        var iconUrl = "img/map_icon.png";

        var marker = new google.maps.Marker({
            position: centerLatLng,
            map: map,
            icon: iconUrl,
            title: name
        });


        if ($rootScope.userPositionLat == undefined && $rootScope.userPositionLong == undefined) {
            $.notify("You are not allowed to use your location.", "error");
            $('#top-map-hide').show();
            $('#top-map').hide();

            $.getJSON('https://json.geoiplookup.io/api?callback=?', function (data) {
                console.log(JSON.stringify(data, null, 2));
            });
            console.log('userPositionLat', lat, long);
        }
    }

}]);

LOMCityControllers.controller('notificationsCtrl', ["$rootScope", "$scope", "$state", '$http', 'dhm', 'getDist', "$filter", "$sce", "$cookies", '$stateParams', 'isValidToken', 'dhmFilter', 'getLocationDistance', '$timeout', '$interval', '$location', '$anchorScroll', function ($rootScope, $scope, $state, $http, dhm, getDist, $filter, $sce, $cookies, $stateParams, isValidToken, dhmFilter, getLocationDistance, $timeout, $interval, $location, $anchorScroll) {

    var promise = isValidToken.check();
    promise.then(function (token) {
        $scope.getNotifications();
    }, function (response) {
        $state.go('main.login', {});
    });

    $scope.getNotifications = function () {
        var request = {
            _token: localStorage.satellizer_token,
            limit: 300
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/getNotifications',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $scope.unreadCommentCounts = response.unreadCommentCount;
            $scope.unreadComments = response.commentCount;
            $scope.groupNotify = response.groupNotify;

            $scope.unreadComments.forEach(function (item) {
                item.mediaThumb = $rootScope.mediaUrlS3 + item.mediaThumb;
                if (item.comment) {
                    item.comment = item.comment.slice(0, 200);
                }
                //
            });
            $scope.unreadComments.sort((a, b) => (b.created_at > a.created_at) ? 1 : -1);
        }).error(function (xhr, req, err) {
        });
    }


    $scope.readNotify = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            object_id: id
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/readNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            // console.log(JSON.stringify(response));
            // $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    $scope.deleteOneNotify = function (id) {
        var request = {
            _token: localStorage.satellizer_token,
            object_id: id
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/deleteOneNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $state.reload();
            // console.log(JSON.stringify(response));
            // $.notify(response.content, "success");
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    $scope.readAllNotify = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/readAllNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            $state.reload();
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });

    }

    $scope.deleteNotify = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/deleteNotify',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            $state.reload();
        }).error(function (xhr, req, err) {
            $.notify(req, "success");
        });

    }

    $scope.deleteCheckedNotify = function () {
        var checked = $('.checkForDel:checkbox:checked');
        var checkedArr = [];
        checked.each(function () {
            var id = $(this).attr("id");
            checkedArr.push(id);
        });

        if (checkedArr == '') {
            $.notify('Select notifications to delete', "success");
        } else {
            checkedArr.forEach(function (element) {

                $scope.deleteOneNotify(element);
                console.log(element, ' deleted');
            });
            $.notify('OK', "success");
        }

    }

    $scope.sendPush = function () {
        var request = {
            _token: localStorage.satellizer_token
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/sendPush',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            //console.log(JSON.stringify(response.content));
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });
    }

    $scope.addDeviceToken = function () {
        var request = {
            _token: localStorage.satellizer_token,
            device_token: 'f7UTlBIWb-Q:APA91bEI21XqXjZPOcuLebqkOi0vwP59GEj0YhGA2aNyfo2RDorWBKTsmF08qSKsnWVrAMN-d-xlhv25mVNEFW33i6PiJvWDmH34MhNoLVpGSXxiFPzP3S5OajgfcmjiCQIALgGimSsb',
            device_os: 'android'
        };
        //console.log(JSON.stringify(request));
        $http({
            method: 'POST',
            url: $rootScope.baseUrl + '/api/addDeviceToken',
            data: {data: JSON.stringify(request)}
        }).success(function (response) {
            $.notify(response.content, "success");
            //console.log(JSON.stringify(response.content));
        }).error(function (xhr, req, err) {
            $.notify(req, "error");
        });
    }

    $('#delOneNotify').on('show.bs.modal', function (e) {
        var $modal = $(this),
            esseyId = e.relatedTarget.id;
        $scope.comment_del_id = esseyId.split('_').pop();

    })
}]);
