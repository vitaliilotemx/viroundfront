
var notifictionModelTime = 10000;
var LOMCityApp;
LOMCityApp = angular.module("LOMCityApp", ["ui.router", "ui.bootstrap", "ngRoute", "ngResource", "ngMaterial", "LOMCityControllers", "satellizer", "ng-iscroll", 'google.places', 'ui-rangeSlider', 'app.translate', 'ngCookies', 'ngMask', 'djds4rce.angular-socialshare', 'openfb', 'facebook']);

LOMCityApp.run(["$rootScope", "$state", "$timeout", "$filter", "$location", "$routeParams", "OpenFB", "$FB", "$http", '$anchorScroll', function ($rootScope, $state, $timeout, $filter, $location, $routeParams, OpenFB, $FB, $http, $anchorScroll) {
    //console.log(OpenFB);
    //OpenFB.init('984760774925340');
    $rootScope.myLat = 0;
    $rootScope.myLong = 0;

    $rootScope.shareUrl = "https://viround.com/web/#/main/event/";
    $rootScope.baseUrl = "https://viround.com/public";
    $rootScope.landingUrl = "https://viround.com/web/#/";


    //$rootScope.landingUrl = "http://localhost:53262/loremx-web/index.html#/";   // for testing

    $rootScope.chooseOptions = [];
    $rootScope.optionsReady = false;
    $rootScope.selectedPOption = "";

    $http({
        method: 'POST',
        url: 'https://viround.com/public/api/getCategories'
    }).success(function (response) {
        //console.log(response)
        $rootScope.chooseOptions = response.content;
        $rootScope.parentCategories = response.parentCategories;
        $rootScope.selectedPOption = response.defaultParentCategory;
        $rootScope.getSubCateegoryes = response.subCategories;
        //console.log(response);
        $rootScope.optionsReady = true;
        //$scope.$apply();
    }).error(function (error) {
        $rootScope.optionsReady = false;
    });


    $rootScope.$on('$viewContentLoaded', function (event) {
        //alert("Here");
        window.ga('send', 'pageview', { page: $location.url() });
    });

    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            //console.log(JSON.stringify(toState));
            if (toState.name == 'land') {
                $rootScope.pageTitle = "Home";
            } else {
                $rootScope.pageTitle = toState.views.mainView.title;
            }
            if (localStorage.satellizer_token == undefined || localStorage.satellizer_token == "undefined" || localStorage.satellizer_token == "" || localStorage.satellizer_token == null) {
                $rootScope.loggedIn = false;
                if (toState.name != 'land' && toState.name != 'main.home' && toState.name != 'main.login' && toState.name != 'main.signup' && toState.name != 'main.forgotPass' && toState.name != 'main.confirmation' && toState.name != 'main.support' && toState.name != 'main.event' && toState.name != 'main.download' && toState.name != 'main.forgotPass') {
                    $timeout(function () {
                        $state.go('main.login', {});
                        $rootScope.pageTitle = "Login";
                    }, 150);
                }
            } else {
                $rootScope.loggedIn = true;
                if (toState.name == 'land' || toState.name == 'main.home' || toState.name == 'main.login' || toState.name == 'main.signup' || toState.name == 'main.forgotPass') {
                    $timeout(function () {
                        $state.go('main.allEvents', {});
                        $rootScope.pageTitle = "All Events";
                    }, 150);
                }
            }
        });
    $rootScope.statusChangeCallback = function (response) {
        console.log('statusChangeCallback');
        console.log(response);
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            console.log("Why");
            $rootScope.getLoginFb(response);
        } else if (response.status === 'not_authorized') {
            $rootScope.fbLogin();
            console.log("Why not_authorized");
        } else {
            $rootScope.fbLogin();
            console.log("Why else");
        }
    }

    $rootScope.fbLogin = function(){
        FB.login(function(response) {
            if (response.status === 'connected') {
                $rootScope.getLoginFb(response);
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
            }
          },{scope: 'email'});
        //},{scope: 'publish_actions,email'});
    }

    window.fbAsyncInit = function () {
        FB.init({
          //appId: '984760774925340',
          //appId: '340304836577332',
          appId: '559967391050967',

            cookie: true,  // enable cookies to allow the server to access
            // the session
            xfbml: true,  // parse social plugins on this page
            version: 'v2.8' // use graph api version 2.8
        });
        FB.getLoginStatus(function (response) {
            //$rootScope.statusChangeCallback(response);
        });

    };

    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));


}]);
