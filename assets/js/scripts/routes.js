LOMCityApp.config(['$stateProvider', '$routeProvider', '$urlRouterProvider', '$authProvider', '$locationProvider','FacebookProvider', function ($stateProvider, $routeProvider, $urlRouterProvider, $authProvider, $locationProvider,FacebookProvider) {

  //FacebookProvider.init('433847516707016');
    FacebookProvider.init('559967391050967');

    $authProvider.loginUrl = 'https://viround.com/public/api/authenticate';
    $stateProvider.state('land', {
        url: '/:param1',
        cache: true,
        views: {
            'headerView': {
                templateUrl: 'template/test.html',
                controller: 'headerCtrl'
            },
            'footerView': {
                templateUrl: 'template/footer.html'
            },
            'contentView': {
                templateUrl: 'template/start.html',
                controller: 'homeCtrl',
                resolve: {
                    title: function () {
                        return "Home";
                    }
                }
            }
        }
    }).state('main', {
        url: '/main',
        views: {
            'headerView': {
                templateUrl: 'template/test.html',
                controller: 'headerCtrl'
            },
            'footerView': {
                templateUrl: 'template/footer.html'
            },
            'contentView': {
                templateUrl: 'template/main.html'
            }
        }
    }).state('main.home', {
        url: '/home',
        views: {
            'mainView': {
                templateUrl: 'template/start.html',
                controller: 'homeCtrl',
                title: 'Home'
            }
        }
    }).state('main.login', {
        url: '/login',
        views: {
            'mainView': {
                templateUrl: 'template/login.html',
                controller: 'loginCtrl',
                title: 'Login'
            }
        }
    }).state('main.signup', {
        url: '/signup',
        views: {
            'mainView': {
                templateUrl: 'template/sign-up.html',
                controller: 'signupCtrl',
                title: 'Sign Up'
            }
        }
    }).state('main.forgotPass', {
        url: '/resetPassword/:user_id/:user_auth',
        views: {
            'mainView': {
                templateUrl: 'template/forgot-password.html',
                controller: 'forgotPassCtrl',
                title: 'Forgot Password'
            }
        }
    }).state('main.confirmation', {
        url: '/confirmation',
        views: {
            'mainView': {
                templateUrl: 'template/confirmation.html',
                title: 'Confirmation'
            }
        }
    }).state('main.allEventsFiltered', {
        url: '/allEvents/:param1/:param2/:param3/:param4',
        cache: false,
        views: {
            'mainView': {
                templateUrl: 'template/all-events.html',
                controller: 'allEventsCtrl',
                title: 'All Events'
            }
        }
    }).state('main.allEvents', {
        url: '/allEvents',
        cache: false,
        views: {
            'mainView': {
                templateUrl: 'template/all-events.html',
                controller: 'allEventsCtrl',
                title: 'All Events'
            }
        }
    }).state('main.myEvents', {
        url: '/myEvents',
        cache: true,
        views: {
            'mainView': {
                templateUrl: 'template/myEvents.html',
                controller: 'myEventsCtrl',
                title: 'My Events',
                cache: true
            }
        }
    }).state('main.event', {
        url: '/event/:event_id',
       // url: '/event',
        views: {
            'mainView': {
                templateUrl: 'template/event.html',
                controller: 'eventCtrl',
                title: 'Event'
            }
        }
    }).state('main.editEvent', {
        url: '/editEvent/:event_id',
        views: {
            'mainView': {
                templateUrl: 'template/editEvent.html',
                controller: 'editEventCtrl',
                title: 'Edit Event'
            }
        }
    }).state('main.myFavEventsFiltered', {
        url: '/myFavEvents/:param1/:param2/:param3/:param4',
        cache: true,
        views: {
            'mainView': {
                templateUrl: 'template/my-fav-events.html',
                /*controller: 'myFavEventsCtrl',*/
                controller: 'myFavorEventsCtrl',
                title: 'My Favorite Events'
            }
        }
    }).state('main.myFavEvents', {
        url: '/myFavEvents',
        cache: true,
        views: {
            'mainView': {
                templateUrl: 'template/my-fav-events.html',
                /*controller: 'myFavEventsCtrl',*/
                controller: 'myFavorEventsCtrl',
                title: 'My Favorite Events'
            }
        }
    }).state('main.conversations', {
        url: '/Conversations',
        views: {
            'mainView': {
                templateUrl: 'template/conversations.html',
                controller: 'conversationsCtrl',
                title: 'Conversations'
            }
        }
    }).state('main.conversation', {
        url: '/conversation/:event_id/:from_user',
        views: {
            'mainView': {
                templateUrl: 'template/conversation.html',
                controller: 'conversationCtrl',
                title: 'Conversation'
            }
        }
    }).state('main.messages', {
        url: '/messages',
        views: {
            'mainView': {
                templateUrl: 'template/messages.html',
                controller: 'messagesCtrl',
                title: 'Messages'
            }
        }
    }).state('main.createEvents', {
        url: '/createEvents',
        views: {
            'mainView': {
                templateUrl: 'template/create-events.html',
                controller: 'createEventsCtrl',
                title: 'Create Events'
            }
        }
    }).state('main.settings', {
        url: '/settings',
        views: {
            'mainView': {
                templateUrl: 'template/settings.html',
                controller: 'settingsCtrl',
                title: 'Settings'
            }
        }
    }).state('main.download', {
        url: '/download',
        views: {
            'mainView': {
                templateUrl: 'template/download.html',
                controller: 'downloadCtrl',
                title: 'Download'
            }
        }
    }).state('main.manageAds', {
        url: '/manageAds',
        views: {
            'mainView': {
                templateUrl: 'template/manage-ads.html',
                controller: 'manageAdsCtrl',
                title: 'Manage Ads'
            }
        }
    }).state('main.createAds', {
        url: '/createAds',
        views: {
            'mainView': {
                templateUrl: 'template/create-ads.html',
                controller: 'createAdsCtrl',
                title: 'Create Ads'
            }
        }
    }).state('main.support', {
        url: '/support',
        views: {
            'mainView': {
                templateUrl: 'template/support.html',
                title: 'Support'
            }
        }
    }).state('main.profile', {
        url: '/profile',
        views: {
            'mainView': {
                templateUrl: 'template/profile.html',
                controller: 'profileCtrl',
                title: 'Profile'
            }
        }
    }).state('main.user', {
        url: '/user/:user_id',
        cache: true,
       // url: '/event',
        views: {
            'mainView': {
                templateUrl: 'template/user.html',
                controller: 'userCtrl',
                title: 'User Profile'
            }
        }
    }).state('main.notifications', {
        url: '/notifications',
        views: {
            'mainView': {
                templateUrl: 'template/notifications.html',
                controller: 'notificationsCtrl',
                title: 'Notifications'
            }
        }
    });
    if (localStorage.satellizer_token == undefined || localStorage.satellizer_token == "undefined" || localStorage.satellizer_token == "" || localStorage.satellizer_token == null) {
        $urlRouterProvider.otherwise('/');
    } else {
        $urlRouterProvider.otherwise('/main/allEvents');
    }
    //$locationProvider.hashPrefix('!');
} ]);
