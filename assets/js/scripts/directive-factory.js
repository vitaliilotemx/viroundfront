
LOMCityApp.directive('toggleClass', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.bind('click', function () {
                element.toggleClass(attrs.toggleClass);
            });
        }
    };
});

LOMCityApp.directive('actualSrc', function () {
    return {
        link: function postLink(scope, element, attrs) {
            attrs.$observe('actualSrc', function (newVal, oldVal) {
                if (newVal != undefined) {
                    var img = new Image();
                    img.src = attrs.actualSrc;
                    angular.element(img).bind('load', function () {
                        element.attr("src", attrs.actualSrc);
                    });
                }
            });

        }
    }
});

LOMCityApp.directive('fbLike', function ($rootScope) {
    return function (scope, iElement, iAttrs) {
        if (FB && scope.$last) {
            FB.XFBML.parse(iElement[0]);
        }
    };
});

// LOMCityApp.directive('locatMap', function ($timeout, $stateParams) {
//     return {
//         link: function (scope, elem, attrs) {
//             var mapOptions,
//             latitude = parseFloat(attrs.lat),
//             longitude = parseFloat(attrs.long),
//             map, marker;
//             //console.log(attrs.lat + '  ' + attrs.long);
//             LatLong = new google.maps.LatLng(latitude, longitude);
//             mapOptions = {
//                 zoom: 14,
//                 zoomControl: false,
//                 center: LatLong
//             };
//             $timeout(function () {
//                 map = new google.maps.Map(elem[0], mapOptions);
//                 marker = new google.maps.Marker({
//                     position: LatLong,
//                     map: map,
//                     icon:"img/map_icon.png"
//                 });
//             }, 150);
//         }
//     };
// });


LOMCityApp.directive('locatMap', function ($timeout, $stateParams) {
    return {
        link: function (scope, elem, attrs) {
            var mapOptions,
            latitude = parseFloat(attrs.lat),
            longitude = parseFloat(attrs.long),
            aEvents = attrs.aEvents,
            title = attrs.title,
            map, marker;
            //console.log(attrs.lat + '  ' + attrs.long);
            LatLong = new google.maps.LatLng(latitude, longitude);
            mapOptions = {
                zoom: 14,
                zoomControl: false,
                center: LatLong
            };

            $timeout(function () {
                map = new google.maps.Map(elem[0], mapOptions);
                marker = new google.maps.Marker({
                    position: LatLong,
                    map: map,
                    icon:"img/map_icon.png"
                });

                // var infoWindow = new google.maps.InfoWindow({
                //   content: title
                // });
                //
                // marker.addListener('click', function(){
                //   infoWindow.open(map, marker);
                // });

            }, 150);
        }
    };
});

LOMCityApp.directive('ngScroll', function () {
    return {
		replace: false,
		restrict: 'A',
        link: function(scope, element, attr){
            scope.$watch(attr.ngScroll, function(value){
				new iScroll(document.querySelector('#wrapper'), {
	   	      	  snap: true,
	   	      	  momentum: true,
	   	      	  hScrollbar: true
	   	    });
        });
       }
    };
});

LOMCityApp.factory('getDist', function ($rootScope) {
    return function (lat, lon, km_checked, item) {
        var lat1 = lat;
        var long1 = lon;
        var p1 = new google.maps.LatLng(lat1, long1);
        var p2 = new google.maps.LatLng($rootScope.myLat, $rootScope.myLong);
        if (km_checked) {
            var dist = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(1);
        } else {
            var dist = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) * 0.000621371192).toFixed(1);
        }
        return dist;
    }
});

LOMCityApp.factory('getLocationDistance', function ($rootScope) {
    return function (lat, lon, km_checked, item,locationLat,locationLong) {
        var lat1 = lat;
        var long1 = lon;
        var p1 = new google.maps.LatLng(lat1, long1);
        var p2 = new google.maps.LatLng(locationLat, locationLong);
        if (km_checked) {
            var dist = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(1);
        } else {
            var dist = (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) * 0.000621371192).toFixed(1);
        }
        //console.log(dist);
        return dist;
    }
})

LOMCityApp.factory('dhm', function () {
    return function (data) {
        /*start time zone*/
        var current_date = new Date();
        //var gmt_offset = current_date.getTimezoneOffset() / 60;
        var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;
        /*end time zone*/
        var a = Date.parse(Date());
        //var b = Date.parse(data);

        var tempDate = data;
        tempDate = tempDate.toString();
        var zz = tempDate.split(" ");
        var zz1 = zz[0].split("-");
        var zz2 = zz[1].split(":");
        var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
        var b = zzz3.getTime();

        var t = a - b;
        t = t + gmt_offset;
        var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor((t - d * cd) / ch),
        m = Math.round((t - d * cd - h * ch) / 60000),
        pad = function (n) { return n < 10 ? '0' + n : n; };
        var timeZoneDateTime = b + gmt_offset;
        var H;
        /*if (h == 0) {
        H = h;
        } else {
        H = h + gmt_offset;
        }*/
        H = h;
        if (m === 60) {
            H++;
            m = 0;
        }
        if (H === 24) {
            d++;
            H = 0;
        }
        text = '';
        /*if (d != 0) {
        text += d + ' day ';
        }
        if (H != 0) {
        text += pad(H) + ' h ';
        }
        if (pad(m) != 0) {
        text += pad(m) + ' min ago';
        }*/

        if (d > 0) {
            if (d == 1) {
                text += 'Yesterday';
            } else {
                if (d <= 7) {
                    text += d + $filter('translate')('dAgo');
                } else {
                    var dt = new Date(timeZoneDateTime);
                    text += dt.getDate() + '-' + (dt.getMonth() + 1) + '-' + dt.getFullYear();
                }
            }
        }else if(d <0){
            if (d == -1) {
                text += 'Tomorrow';
            } else {
                var dt = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    text += dt.getDate() + '-' + (dt.getMonth() + 1) + '-' + dt.getFullYear();
            }
        } else if (!(H < 1)) {
            if (H == 1) {
                text += H + $filter('translate')('hAgo');
            } else {
                text += H + $filter('translate')('hAgo');
            }
        } else if (m != 0) {
            if (m == 1) {
                text += m + $filter('translate')('minAgo');
            } else {
                text += m + $filter('translate')('minAgo');
            }
        } else {
            text += $filter('translate')('Just_now');
        }
        return text;
    }
});
LOMCityApp.factory('dhmFilter', function ($filter,$rootScope) {
    return function (data) {
        /*start time zone*/
        var current_date = new Date();
        //var gmt_offset = current_date.getTimezoneOffset() / 60;
        var gmt_offset = current_date.getTimezoneOffset() * 60 * 1000;

        /*end time zone*/
        var a = Date.parse(Date());
        //var b = Date.parse(data);

        var tempDate = data;
        tempDate = tempDate.toString();
        var zz = tempDate.split(" ");
        var zz1 = zz[0].split("-");
        var zz2 = zz[1].split(":");
        var zzz3 = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
        var b = zzz3.getTime();

        var t = a - b;
        t = t + gmt_offset;
        var cd = 24 * 60 * 60 * 1000,
        ch = 60 * 60 * 1000,
        d = Math.floor(t / cd),
        h = Math.floor((t - d * cd) / ch),
        m = Math.round((t - d * cd - h * ch) / 60000),
        pad = function (n) { return n < 10 ? '0' + n : n; };
        var H;
        /*
        if (h == 0) {
        H = h;
        } else {
        H = h + gmt_offset;
        }
        */
        H = h;

        if (m === 60) {
            H++;
            m = 0;
        }
        if (H === 24) {
            d++;
            H = 0;
        }
        text = '';
        /*if (d != 0) {
        text += d + ' day ';
        }
        if (H != 0) {
        text += pad(H) + ' h ';
        }
        if (pad(m) != 0) {
        text += pad(m) + ' min ago';
        }*/
        var future = false;
        var isNow = false;
        var showtime = false;
        //console.log('day '+d);
        if (d > 0) {
            if (d == 1) {
                text += 'Yesterday';
                isNow = false;
                showtime = true;
            } else {
                if (d <= 7) {
                    text += d + $filter('translate')('dAgo');
                    isNow = false;
                    showtime = true;
                } else {
                    var dt = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    //text += dt.getDate() + '-' + (dt.getMonth() + 1) + '-' + dt.getFullYear();
                    //alert(dt.toLocaleTimeString());
                    //alert(dt.getMonth()+1);

                    var _date = $filter('date')(dt.getTime(), $filter('translate')('dateFormat'));
                    var _mn = (dt.getMonth());
                    var _month = $rootScope.monthArray.split(",");
                    var pattern = new RegExp(_month[_mn].trim(), "ig");

                    var _lanM = $filter('translate')('monthArray').split(",");

                    //console.log(_date+"  :  "+pattern+"  :  "+_lanM[_mn]);

                    var _dateF  = _date.replace(pattern,_lanM[_mn]);
                    text += _dateF;
                    //text += dt.toLocaleDateString()+" "+dt.toLocaleTimeString();
                    isNow = false;
                    showtime = false;
                }
            }
        }else if(d <0){
            //alert(d+"  "+H);
            if (d == -1 && H ==0) {
                text += 'Tomorrow';
                isNow = false;
                showtime = true;
            } else {
                var dt = new Date(zz1[0], (zz1[1] - 1), zz1[2], zz2[0], zz2[1], zz2[2]);
                    //text += dt.getDate() + '-' + (dt.getMonth() + 1) + '-' + dt.getFullYear();

                    var _date = $filter('date')(dt.getTime(), $filter('translate')('dateFormat'));
                    var _mn = (dt.getMonth());
                    var _month = $rootScope.monthArray.split(",");
                    var pattern = new RegExp(_month[_mn].trim(), "ig");

                    var _lanM = $filter('translate')('monthArray').split(",");
                    console.log(_lanM);
                    var _dateF  = _date.replace(pattern,_lanM[_mn]);
                    //text = dt.toLocaleDateString()+" "+dt.toLocaleTimeString();
                    isNow = false;
                    showtime = false;
            }
        } else if (!(H < 1)) {
            //console.log(H);
            if (H == 1) {
                text += H + $filter('translate')('hAgo');
                isNow = true;
                showtime = false;
            } else {
                text += H + $filter('translate')('hAgo');
                isNow = true;
                showtime = false;
            }
        } else if (m != 0) {
            if (m == 1) {
                text += m + " "+ $filter('translate')('minAgo');
                isNow = true;
                showtime = false;
            } else {
                text += m + " "+$filter('translate')('minAgo');
                isNow = true;
                showtime = false;
            }
        } else {
            text += $filter('translate')('Just_now');
            isNow = true;
            showtime = false;
        }
        var result = [];
        result.push(text,isNow,showtime);
        return result;
    }
})



LOMCityApp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            // element.bind('change', function(){
            //     scope.$apply(function(){
            //         modelSetter(scope, element[0].files[0]);
            //     });
            // });

            element.bind('change', function () {
                var values = [];
                angular.forEach(element[0].files, function (item) {
                    var value = {
                       // File Name
                        name: item.name,
                        //File Size
                        size: item.size,
                        //File URL to view
                        url: URL.createObjectURL(item),
                        // File Input Value
                        file: item
                    };
                    values.push(value);
                });

                scope.$apply(function () {
                    modelSetter(scope, values);
                });
            });
        }
    };
}]);

LOMCityApp.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (type, file, fileName, uploadUrl) {
        //console.log(type + ' ' + file + ' ' + fileName + ' ' + uploadUrl +'zzz');
        var fd = new FormData();
        fd.append('type', type);
        fd.append('file', file);
        fd.append('fileName', fileName);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
        .success(function (res) {
            //alert(JSON.stringify(res));
            //console.log(JSON.stringify(res));
            $scope.mediaType = type;
            $scope.mediaUrl = res.content;
        })
        .error(function (res) {
            //alert('1' + res);
            //console.log(JSON.stringify(res));
        });
    }
} ]);

LOMCityApp.directive("scroll", function ($window) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function () {
            var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
            var body = document.body, html = document.documentElement;
            var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
            windowBottom = windowHeight + window.pageYOffset;
            if (windowBottom >= docHeight) {
                alert('bottom reached');
            }
        });
    };
});



LOMCityApp.factory('isValidToken', ['$q','SatellizerShared','$auth', function ($q,shared,$auth) {
    var isValidToken = {};

    isValidToken.check = function () {
        var deferred = new $q.defer();
        if (shared.isAuthenticated()) {
            deferred.resolve(localStorage.satellizer_token);
        } else {
            //alert(localStorage.getItem('loginDetail'));
            var loginDetail = localStorage.getItem('loginDetail');
            if(loginDetail != null)
            {
                loginDetail = JSON.parse(loginDetail);
                var request = {
                    email: loginDetail.email,
                    password: loginDetail.password
                };
                //console.log(JSON.stringify(request));
                $auth.login(request).catch(function(response) {
                    deferred.reject(response);
                }).then(function(response) {
                    if(response != undefined && response.status == 200){
                        deferred.resolve('new token: '+ localStorage.satellizer_token);
                    }
                });
            }else{
                deferred.reject("Token Expired");
            }
        }
        return deferred.promise;
    };
    return isValidToken;
} ]);

LOMCityApp.filter('unsafe', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
});


LOMCityApp.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
  });


// LOMCityApp.directive('myEvents', function () {
//     return {
//   		replace: false,
//   		restrict: 'E',
//       templateUrl: "template/myEvents.html"
//       link: function(scope, element, attr){
//       }
//     };
// });
